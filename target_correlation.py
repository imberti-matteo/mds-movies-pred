import pandas as pd
import numpy as np
import string as str
import operator
import networkx as nx
import matplotlib.pyplot as plt


data = pd.read_csv("2017_07_14_v01dataset.csv",sep=",")

targets = data.loc[:]['TARGET'] #ottengo tutti i valori della colonna TARGET

# QUANTE VOLTE APPARE UNA LABEL? QUANTE VOLTE UNA LABEL APPARE DA SOLA?
targets_dictionary = {}
lonely_dictionary = {}
unique_targets = []
for current_target in targets:
    current_target_lower = current_target.lower()
    splitted_target = current_target_lower.split(',')
    for m in splitted_target:
        if m in targets_dictionary:
            targets_dictionary[m] += 1
            if len(splitted_target) == 1:
                lonely_dictionary[m] += 1
        else:
            targets_dictionary[m] = 1
            unique_targets.append(m)
            lonely_dictionary[m] = 0
            if len(splitted_target) == 1:
                lonely_dictionary[m] += 1
print("\n|| NUMERO DI VOLTE IN CUI UNA LABEL APPARE ||\n")
print(sorted(targets_dictionary.items(), key=operator.itemgetter(1), reverse=True))
print("\n")

y=np.arange(len(unique_targets))
plt.plot(1)
plt.subplot(221)
plt.barh(y,list(targets_dictionary.values()))
plt.yticks(y, list(targets_dictionary.keys()),  fontsize=5)
plt.title("NUMERO DI VOLTE IN CUI UNA LABEL APPARE", fontsize=7)

# QUANTE VOLTE APPARE UNA LABEL IN PERCENTUALE?
total_count = len(targets)
targets_percentual_dictionary = {key:round((value/total_count)*100,2) for key,value in targets_dictionary.items()}
print("\n|| PERCENTUALE DI VOLTE IN CUI UNA LABEL APPARE ||\n")
print(sorted(targets_percentual_dictionary.items(), key=operator.itemgetter(1), reverse=True))
print("\n")

y=np.arange(len(unique_targets))
plt.subplot(222)
plt.barh(y,list(targets_percentual_dictionary.values()))
plt.yticks(y, list(targets_percentual_dictionary.keys()),  fontsize=5)
plt.title("PERCENTUALE DI VOLTE IN CUI UNA LABEL APPARE", fontsize=7)

# QUANTE VOLTE DUE LABEL APPAIONO INSIEME?
cross_relation = {}
for x in unique_targets:
    for y in unique_targets:
        union_string = x + ' UNION ' + y
        reversed_union_string = y + ' UNION ' + x
        if (reversed_union_string not in cross_relation) and (x != y):
            cross_relation[union_string] = 0
for current_target in targets:
    current_target_lower = current_target.lower()
    splitted_target = current_target_lower.split(',')
    for x in splitted_target:
        for y in splitted_target:
            union_string = x + ' UNION ' + y
            reversed_union_string = y + ' UNION ' + x
            if (union_string in cross_relation):
                cross_relation[union_string] += 1
            elif (reversed_union_string in cross_relation):
                cross_relation[reversed_union_string] += 1
cross_relation = {key:int(value/2) for key,value in cross_relation.items()}
print("\n|| NUMERO DI VOLTE IN CUI DUE LABEL APPAIONO INSIEME ||\n")
print(sorted(cross_relation.items(), key=operator.itemgetter(1), reverse=True))
print("\n")

# QUANTE VOLTE APPARE SOLA UNA LABEL?
print("\n|| NUMERO DI VOLTE IN CUI UNA LABEL APPARE DA SOLA ||\n")
print(sorted(lonely_dictionary.items(), key=operator.itemgetter(1), reverse=True))
print("\n")

y=np.arange(len(lonely_dictionary))
plt.subplot(223)
plt.barh(y,list(lonely_dictionary.values()))
plt.yticks(y, list(lonely_dictionary.keys()), fontsize=5)
plt.title("NUMERO DI VOLTE IN CUI UNA LABEL APPARE DA SOLA", fontsize=7)

# QUANTE VOLTE IN PERCENTUALE UNA LABEL APPARE DA SOLA?
lonely_percentual_dictionary = lonely_dictionary.copy()
for current_target in lonely_percentual_dictionary.keys():
    lonely_percentual_dictionary[current_target] = round((lonely_percentual_dictionary[current_target] / targets_dictionary[current_target])*100,2)
print("\n|| PERCENTUALE DI VOLTE IN CUI UNA LABEL APPARE DA SOLA ||\n")
print(sorted(lonely_percentual_dictionary.items(), key=operator.itemgetter(1), reverse=True))
print("\n")

y=np.arange(len(lonely_percentual_dictionary))
plt.subplot(224)
plt.barh(y,list(lonely_percentual_dictionary.values()))
plt.yticks(y, list(lonely_percentual_dictionary.keys()), fontsize=5)
plt.title("PERCENTUALE DI VOLTE IN CUI UNA LABEL APPARE DA SOLA", fontsize=7)
plt.show()

# QUANTE VOLTE IN PERCENTUALE UNA LABEL APPARE IN PRESENZA DI UN'ALTRA?
print("\n|| PERCENTUALE DI VOLTE IN CUI UNA LABEL APPARE IN PRESENZA DI UN'ALTRA ||\n[Da leggere come 'X' ---> 'Y', Z: Se è presente la label 'X', allora nel Z% delle volte è presente anche la label 'Y']\n")
composition = {}
ratio_dictionary = {}
for m in unique_targets:
    for n in unique_targets:
        if m != n:
            union_string = m + ' UNION ' + n
            reversed_union_string = n + ' UNION ' + m
            if (union_string in cross_relation):
                ratio = cross_relation[union_string] / targets_dictionary[n]
                composition_string = "'" + n + "' ---> '" + m + "'"
                ratio_dictionary[composition_string] = round(ratio*100,2)
                composition[composition_string] = round(ratio*100,2)
            elif (reversed_union_string in cross_relation):
                ratio = cross_relation[reversed_union_string] / targets_dictionary[n]
                composition_string = "'" + n + "' ---> '" + m + "'"
                ratio_dictionary[composition_string] = round(ratio * 100, 2)
                composition[composition_string] = round(ratio * 100,2)
print(sorted(ratio_dictionary.items(), key=operator.itemgetter(1), reverse=True))
print("\n")

G = nx.Graph()
G.add_nodes_from(unique_targets)
graph_threshold = 0
# QUALI SONO LE LABEL IN PERCENTUALE CHE COMPAIONO CON UN'ALTRA LABEL?
print("\n|| PERCENTUALE DI VOLTE IN CUI UNA LABEL APPARE IN PRESENZA DI UN'ALTRA || (elencazione per label) \n")
for m in unique_targets:
    current_composition = {}
    print("°°°",m.upper(),"°°°")
    for n in unique_targets:
        composition_string = "'" + m + "' ---> '" + n + "'"
        if (composition_string in composition) and (composition[composition_string]>0):
            current_composition[n]= composition[composition_string]
            if(composition[composition_string]>graph_threshold):
                G.add_edge(m,n)
    current_composition['niente']=lonely_percentual_dictionary[m]
    print(sorted(current_composition.items(), key=operator.itemgetter(1), reverse=True))
    print()
print()
plt.show()
plt.plot(3)
plt.title("GRAFO DELLE RELAZIONI TRA LABEL", fontsize=15)
nx.draw_spring(G, with_labels=True, font_size=6, font_weight='bold')
plt.show()

# QUANTE VOLTE IN PERCENTUALE DUE LABEL APPAIONO INSIEME?
print("\n|| PERCENTUALE DI VOLTE IN CUI DUE LABEL APPAIONO INSIEME ||\n[Da leggere come 'X' UNION 'Y', Z: La label 'X' e la label 'Y' sono presenti insieme il Z% delle volte]\n")
done_list = []
bidirectional_ratio_dictionary = {}
for m in unique_targets:
    for n in unique_targets:
        if m != n:
            union_string = m + ' UNION ' + n
            reversed_union_string = n + ' UNION ' + m
            if (union_string in cross_relation) and (union_string not in done_list):
                bidirectional_ratio = cross_relation[union_string] / (targets_dictionary[n] + targets_dictionary[m] - cross_relation[union_string])
                bidirectional_composition_string = "'" + n + "'" + " UNION " + "'" + m + "'"
                bidirectional_ratio_dictionary[bidirectional_composition_string] = round(bidirectional_ratio*100,2)
                done_list.append(union_string)
            elif (reversed_union_string in cross_relation) and (reversed_union_string not in done_list):
                bidirectional_ratio = cross_relation[reversed_union_string] / (targets_dictionary[n] + targets_dictionary[m] - cross_relation[reversed_union_string])
                bidirectional_composition_string = "'" + n + "'" + " UNION " + "'" + m + "'"
                bidirectional_ratio_dictionary[bidirectional_composition_string] = round(bidirectional_ratio * 100, 2)
                done_list.append(reversed_union_string)
print(sorted(bidirectional_ratio_dictionary.items(), key=operator.itemgetter(1), reverse=True))
print("\n")
