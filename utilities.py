import re
from nltk.corpus import wordnet as wn
from nltk import word_tokenize, pos_tag, ne_chunk
from nltk.corpus import stopwords
from word2number import w2n
from nltk.tree import Tree
import string

STOPWORDS = stopwords.words('english')
regex = re.compile('[%s]' % re.escape(string.punctuation))

def get_proper_names(text):
    chunked = ne_chunk(pos_tag(word_tokenize(text)))
    prev = None
    continuous_chunk = []
    current_chunk = []
    for i in chunked:
            if type(i) == Tree:
                    current_chunk.append(" ".join([token for token, pos in i.leaves()]))
            elif current_chunk:
                    named_entity = " ".join(current_chunk)
                    if named_entity not in continuous_chunk:
                            continuous_chunk.append(named_entity)
                            current_chunk = []
            else:
                    continue
    return continuous_chunk

def get_lemma(word):
    lemma = wn.morphy(word)
    if lemma is None:
        return word
    else:
        return lemma

def preprocessing_text(text):
    return text

def clean_text(text):
    text = text.replace(".", " ")
    text = regex.sub(' ', text)
    proper_names = word_tokenize(' '.join(get_proper_names(text)).lower())
    tokenized_text = word_tokenize(text.lower())
    for i in range(len(tokenized_text)):
        try:
            tokenized_text[i] = str(w2n.word_to_num(tokenized_text[i]))
        except ValueError:
            pass
    cleaned_text = [t for t in tokenized_text if t not in STOPWORDS and re.match('[a-zA-Z\-][a-zA-Z\-]{2,}', t) and t not in proper_names]
    lemmatized_text = [get_lemma(token) for token in cleaned_text]
    return lemmatized_text
