import gensim
import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from nltk import ne_chunk, pos_tag, word_tokenize
from nltk.tree import Tree
from sklearn.naive_bayes import GaussianNB
from imblearn.over_sampling import RandomOverSampler
from imblearn.under_sampling import RandomUnderSampler
from imblearn.combine import SMOTEENN
from imblearn.over_sampling import SMOTE, ADASYN
from imblearn.under_sampling import NearMiss
from imblearn.under_sampling import TomekLinks
from imblearn.under_sampling import EditedNearestNeighbours
from imblearn.under_sampling import RepeatedEditedNearestNeighbours
from imblearn.under_sampling import OneSidedSelection
from imblearn.under_sampling import InstanceHardnessThreshold
from imblearn.ensemble import BalancedBaggingClassifier
from sklearn import metrics
from collections import Counter

def get_continuous_chunks(text):
    chunked = ne_chunk(pos_tag(word_tokenize(text)))
    prev = None
    continuous_chunk = []
    current_chunk = []
    for i in chunked:
            if type(i) == Tree:
                    current_chunk.append(" ".join([token for token, pos in i.leaves()]))
            elif current_chunk:
                    named_entity = " ".join(current_chunk)
                    if named_entity not in continuous_chunk:
                            continuous_chunk.append(named_entity)
                            current_chunk = []
            else:
                    continue
    return continuous_chunk

def restore_movie2vec(m2v_string):
    m2v_string = m2v_string.replace('\r','').replace('\n','').replace('[ ','').replace('[','').replace(']','').replace('  ',' ')
    m2v_string_splitted = m2v_string.split(' ')
    m2v = []
    for i in range(len(m2v_string_splitted)):
        try:
            m2v.append(float(m2v_string_splitted[i]))
        except ValueError:
            pass
    return m2v

model = gensim.models.KeyedVectors.load_word2vec_format('../google-word2vec/google-w2v.bin', binary=True)

df = pd.read_csv("./../new_extended_cleaned_dataset.csv")
df_train, df_test = train_test_split(df,test_size=0.2,random_state=7)
m2v_df = pd.read_csv("./../movie2vec.csv")
train_m2v_df = m2v_df.loc[:,'MOVIE2VEC']
train_m2v = []
for k in range(len(train_m2v_df)):
    m2v = restore_movie2vec(train_m2v_df[k])
    train_m2v.append(m2v)

labels = ['ADRENALINICO/MOZZAFIATO', 'AGGHIACCIANTE-TERRIFICANTE', 'AGRODOLCE', 'ARRAPANTE', 'CEREBRALE', 'CHE FA PENSARE/INSPIRING', 'CHE FA SOGNARE', 'COMMOVENTE/STRAPPALACRIME', 'ESALTANTE/TRASCINANTE/EPICO', 'FA MORIRE DAL RIDERE', 'FEEL-GOOD', 'INQUIETANTE/ANGOSCIANTE', 'NOSTALGICO/MELANCONICO', 'PROVOCA RABBIA/INDIGNAZIONE', 'RASSICURANTE', 'ROMANTICO', 'SCACCIAPENSIERI - EVASIVO', 'SFAVILLANTE/STYLISH', 'SORPRENDENTE', 'SPIAZZANTE', 'ADULTI (40-OLTRE)', 'BAMBINI ANCHE SOLI (ETÀ PRE-SCOLARE)', 'BAMBINI ANCHE SOLI (ETÀ SCOLARE)', 'FAMILY (BAMBINI + ADULTI)', 'FEMMINILE', 'GIOVANI ADULTI (20-40)', 'MASCHILE', 'TEENS (13-19)']

train_text = df_train.loc[:,'TEXT'].str.lower().tolist()
test_text = df_test.loc[:,'TEXT'].str.lower().tolist()
df_test_id_list = df_test.loc[:,'ID'].tolist()
test_text_original = df_test.loc[:,'TEXT'].tolist()

tfidf = TfidfVectorizer(min_df=0.01, max_df=0.25, stop_words='english')
tfidf_input = tfidf.fit(train_text)
tfidf_test = tfidf.transform(test_text)
feature_names = tfidf.get_feature_names()

tfidf_test = tfidf_test.toarray()

test_m2v = []
m2v_account = 15
range_len = len(tfidf_test)
for movie in range(range_len):
    w2v_keywords = []
    slave_input= tfidf_test[movie]
    slave_feature_names = feature_names[:]
    ne_set = get_continuous_chunks(test_text_original[movie])
    i = 0
    while i < m2v_account:
        max_score = max(slave_input)
        if max_score == 0:
            break
        try:
            ind = int(np.where(slave_input == max_score)[0])
        except TypeError:
            ind = int(np.where(slave_input == max_score)[0][0])
        if(slave_feature_names[ind] not in [x.lower() for x in ne_set]):
            try:
                w2v_keywords.append(model.get_vector(slave_feature_names[ind]))
                i += 1
            except KeyError:
                pass
        slave_input = np.delete(slave_input,ind)
        slave_feature_names.pop(ind)
    plot2vec = np.average(w2v_keywords,axis=0)
    test_m2v.append(plot2vec)
    print("Movie2Vec -", df_test_id_list[movie], "||", int((movie*100)/range_len),"%")

#bbc = BalancedBaggingClassifier(base_estimator=GaussianNB(),ratio='auto',replacement=False,random_state=0)
oss = EditedNearestNeighbours(random_state=0)

out_file = open("./test/test_M2V_NB_ENN_NEW_EXTENDED.txt","w")

for label in labels:
    train_label = df_train.loc[:, label].tolist()
    test_label = df_test.loc[:, label].tolist()
    try:
        #bbc.fit(train_m2v,train_label)
        train_input_resampled, train_label_resampled = oss.fit_sample(train_m2v,train_label)
    except ValueError:
        print("ValueError per label: ",label)
        train_input_resampled = train_m2v
        train_label_resampled = train_label
    #print(sorted(Counter(train_label_resampled).items()))
    clf = GaussianNB().fit(train_input_resampled, train_label_resampled)
    predictions = clf.predict(test_m2v)
    names = ['0','1']
    print("EVALUATION FOR LABEL: \n",label,"\n")
    print(metrics.classification_report(test_label, predictions,target_names=names))
    out_file.write("EVALUATION FOR LABEL: ")
    out_file.write(label)
    out_file.write("\n")
    out_file.write(metrics.classification_report(test_label, predictions,target_names=names))
    out_file.write("\n\n")
out_file.close()







