EVALUATION FOR LABEL: ADRENALINICO/MOZZAFIATO
             precision    recall  f1-score   support

          0       0.93      0.74      0.82       321
          1       0.54      0.84      0.65       117

avg / total       0.82      0.76      0.78       438


EVALUATION FOR LABEL: AGGHIACCIANTE-TERRIFICANTE
             precision    recall  f1-score   support

          0       0.98      0.76      0.86       406
          1       0.20      0.78      0.32        32

avg / total       0.92      0.76      0.82       438


EVALUATION FOR LABEL: AGRODOLCE
             precision    recall  f1-score   support

          0       0.95      0.70      0.80       386
          1       0.24      0.71      0.36        52

avg / total       0.86      0.70      0.75       438


EVALUATION FOR LABEL: ARRAPANTE
             precision    recall  f1-score   support

          0       0.99      0.71      0.82       429
          1       0.04      0.56      0.07         9

avg / total       0.97      0.71      0.81       438


EVALUATION FOR LABEL: CEREBRALE
             precision    recall  f1-score   support

          0       0.98      0.57      0.72       422
          1       0.06      0.75      0.11        16

avg / total       0.95      0.57      0.70       438


EVALUATION FOR LABEL: CHE FA PENSARE/INSPIRING
             precision    recall  f1-score   support

          0       0.91      0.64      0.75       373
          1       0.24      0.65      0.35        65

avg / total       0.81      0.64      0.69       438


EVALUATION FOR LABEL: CHE FA SOGNARE
             precision    recall  f1-score   support

          0       0.98      0.75      0.85       417
          1       0.13      0.76      0.23        21

avg / total       0.94      0.75      0.82       438


EVALUATION FOR LABEL: COMMOVENTE/STRAPPALACRIME
             precision    recall  f1-score   support

          0       0.98      0.61      0.75       414
          1       0.10      0.75      0.18        24

avg / total       0.93      0.62      0.72       438


EVALUATION FOR LABEL: ESALTANTE/TRASCINANTE/EPICO
             precision    recall  f1-score   support

          0       0.97      0.71      0.82       392
          1       0.24      0.78      0.37        46

avg / total       0.89      0.72      0.77       438


EVALUATION FOR LABEL: FA MORIRE DAL RIDERE
             precision    recall  f1-score   support

          0       0.97      0.69      0.80       399
          1       0.19      0.77      0.31        39

avg / total       0.90      0.69      0.76       438


EVALUATION FOR LABEL: FEEL-GOOD
             precision    recall  f1-score   support

          0       0.93      0.67      0.78       390
          1       0.18      0.60      0.28        48

avg / total       0.85      0.66      0.72       438


EVALUATION FOR LABEL: INQUIETANTE/ANGOSCIANTE
             precision    recall  f1-score   support

          0       0.92      0.65      0.76       371
          1       0.26      0.69      0.38        67

avg / total       0.82      0.66      0.70       438


EVALUATION FOR LABEL: NOSTALGICO/MELANCONICO
             precision    recall  f1-score   support

          0       0.96      0.60      0.74       409
          1       0.11      0.69      0.19        29

avg / total       0.91      0.61      0.70       438


EVALUATION FOR LABEL: PROVOCA RABBIA/INDIGNAZIONE
             precision    recall  f1-score   support

          0       1.00      1.00      1.00       438

avg / total       1.00      1.00      1.00       438


EVALUATION FOR LABEL: RASSICURANTE
             precision    recall  f1-score   support

          0       1.00      0.73      0.85       433
          1       0.03      0.80      0.06         5

avg / total       0.99      0.74      0.84       438


EVALUATION FOR LABEL: ROMANTICO
             precision    recall  f1-score   support

          0       0.98      0.72      0.83       416
          1       0.13      0.77      0.22        22

avg / total       0.94      0.72      0.80       438


EVALUATION FOR LABEL: SCACCIAPENSIERI - EVASIVO
             precision    recall  f1-score   support

          0       0.89      0.66      0.76       357
          1       0.30      0.64      0.41        81

avg / total       0.78      0.66      0.70       438


EVALUATION FOR LABEL: SFAVILLANTE/STYLISH
             precision    recall  f1-score   support

          0       0.98      0.58      0.73       427
          1       0.03      0.45      0.05        11

avg / total       0.95      0.58      0.71       438


EVALUATION FOR LABEL: SORPRENDENTE
             precision    recall  f1-score   support

          0       0.93      0.60      0.73       408
          1       0.07      0.40      0.12        30

avg / total       0.87      0.58      0.69       438


EVALUATION FOR LABEL: SPIAZZANTE
             precision    recall  f1-score   support

          0       0.93      0.56      0.70       394
          1       0.14      0.64      0.23        44

avg / total       0.85      0.57      0.66       438


EVALUATION FOR LABEL: ADULTI (40-OLTRE)
             precision    recall  f1-score   support

          0       0.55      0.71      0.62       146
          1       0.83      0.71      0.76       292

avg / total       0.73      0.71      0.71       438


EVALUATION FOR LABEL: BAMBINI ANCHE SOLI (ET� PRE-SCOLARE)
             precision    recall  f1-score   support

          0       1.00      0.74      0.85       435
          1       0.03      1.00      0.05         3

avg / total       0.99      0.74      0.84       438


EVALUATION FOR LABEL: BAMBINI ANCHE SOLI (ET� SCOLARE)
             precision    recall  f1-score   support

          0       1.00      0.82      0.90       417
          1       0.21      0.95      0.34        21

avg / total       0.96      0.82      0.87       438


EVALUATION FOR LABEL: FAMILY (BAMBINI + ADULTI)
             precision    recall  f1-score   support

          0       0.96      0.76      0.85       391
          1       0.28      0.77      0.41        47

avg / total       0.89      0.76      0.81       438


EVALUATION FOR LABEL: FEMMINILE
             precision    recall  f1-score   support

          0       0.97      0.74      0.84       396
          1       0.25      0.79      0.38        42

avg / total       0.90      0.75      0.80       438


EVALUATION FOR LABEL: GIOVANI ADULTI (20-40)
             precision    recall  f1-score   support

          0       0.33      0.56      0.42        71
          1       0.90      0.78      0.84       367

avg / total       0.81      0.74      0.77       438


EVALUATION FOR LABEL: MASCHILE
             precision    recall  f1-score   support

          0       0.89      0.71      0.79       309
          1       0.53      0.78      0.63       129

avg / total       0.78      0.73      0.74       438


EVALUATION FOR LABEL: TEENS (13-19)
             precision    recall  f1-score   support

          0       0.91      0.65      0.76       357
          1       0.32      0.73      0.45        81

avg / total       0.80      0.66      0.70       438


