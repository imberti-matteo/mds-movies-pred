EVALUATION FOR LABEL: ADRENALINICO/MOZZAFIATO
             precision    recall  f1-score   support

          0       0.83      0.93      0.88       321
          1       0.73      0.48      0.58       117

avg / total       0.80      0.81      0.80       438


EVALUATION FOR LABEL: AGGHIACCIANTE-TERRIFICANTE
             precision    recall  f1-score   support

          0       0.93      1.00      0.96       406
          1       0.67      0.06      0.11        32

avg / total       0.91      0.93      0.90       438


EVALUATION FOR LABEL: AGRODOLCE
             precision    recall  f1-score   support

          0       0.88      1.00      0.94       386
          1       0.50      0.02      0.04        52

avg / total       0.84      0.88      0.83       438


EVALUATION FOR LABEL: ARRAPANTE
             precision    recall  f1-score   support

          0       0.98      1.00      0.99       429
          1       0.00      0.00      0.00         9

avg / total       0.96      0.98      0.97       438


EVALUATION FOR LABEL: CEREBRALE
             precision    recall  f1-score   support

          0       0.96      1.00      0.98       422
          1       0.00      0.00      0.00        16

avg / total       0.93      0.96      0.94       438


EVALUATION FOR LABEL: CHE FA PENSARE/INSPIRING
             precision    recall  f1-score   support

          0       0.87      0.96      0.91       373
          1       0.45      0.20      0.28        65

avg / total       0.81      0.84      0.82       438


EVALUATION FOR LABEL: CHE FA SOGNARE
             precision    recall  f1-score   support

          0       0.96      1.00      0.98       417
          1       1.00      0.14      0.25        21

avg / total       0.96      0.96      0.94       438


EVALUATION FOR LABEL: COMMOVENTE/STRAPPALACRIME
             precision    recall  f1-score   support

          0       0.95      1.00      0.97       414
          1       0.00      0.00      0.00        24

avg / total       0.89      0.94      0.92       438


EVALUATION FOR LABEL: ESALTANTE/TRASCINANTE/EPICO
             precision    recall  f1-score   support

          0       0.93      0.99      0.96       392
          1       0.76      0.35      0.48        46

avg / total       0.91      0.92      0.91       438


EVALUATION FOR LABEL: FA MORIRE DAL RIDERE
             precision    recall  f1-score   support

          0       0.93      1.00      0.96       399
          1       0.90      0.23      0.37        39

avg / total       0.93      0.93      0.91       438


EVALUATION FOR LABEL: FEEL-GOOD
             precision    recall  f1-score   support

          0       0.89      0.98      0.94       390
          1       0.25      0.04      0.07        48

avg / total       0.82      0.88      0.84       438


EVALUATION FOR LABEL: INQUIETANTE/ANGOSCIANTE
             precision    recall  f1-score   support

          0       0.86      0.95      0.90       371
          1       0.32      0.12      0.17        67

avg / total       0.77      0.83      0.79       438


EVALUATION FOR LABEL: NOSTALGICO/MELANCONICO
             precision    recall  f1-score   support

          0       0.93      1.00      0.97       409
          1       0.00      0.00      0.00        29

avg / total       0.87      0.93      0.90       438


EVALUATION FOR LABEL: PROVOCA RABBIA/INDIGNAZIONE
             precision    recall  f1-score   support

          0       1.00      1.00      1.00       438

avg / total       1.00      1.00      1.00       438


EVALUATION FOR LABEL: RASSICURANTE
             precision    recall  f1-score   support

          0       0.99      1.00      0.99       433
          1       0.00      0.00      0.00         5

avg / total       0.98      0.99      0.98       438


EVALUATION FOR LABEL: ROMANTICO
             precision    recall  f1-score   support

          0       0.95      1.00      0.97       416
          1       0.00      0.00      0.00        22

avg / total       0.90      0.95      0.93       438


EVALUATION FOR LABEL: SCACCIAPENSIERI - EVASIVO
             precision    recall  f1-score   support

          0       0.84      0.99      0.91       357
          1       0.83      0.19      0.30        81

avg / total       0.84      0.84      0.80       438


EVALUATION FOR LABEL: SFAVILLANTE/STYLISH
             precision    recall  f1-score   support

          0       0.97      1.00      0.99       427
          1       0.00      0.00      0.00        11

avg / total       0.95      0.97      0.96       438


EVALUATION FOR LABEL: SORPRENDENTE
             precision    recall  f1-score   support

          0       0.93      1.00      0.96       408
          1       0.00      0.00      0.00        30

avg / total       0.87      0.93      0.90       438


EVALUATION FOR LABEL: SPIAZZANTE
             precision    recall  f1-score   support

          0       0.90      0.99      0.94       394
          1       0.25      0.02      0.04        44

avg / total       0.84      0.89      0.85       438


EVALUATION FOR LABEL: ADULTI (40-OLTRE)
             precision    recall  f1-score   support

          0       0.64      0.48      0.55       146
          1       0.77      0.87      0.81       292

avg / total       0.73      0.74      0.73       438


EVALUATION FOR LABEL: BAMBINI ANCHE SOLI (ET� PRE-SCOLARE)
             precision    recall  f1-score   support

          0       0.99      1.00      1.00       435
          1       0.00      0.00      0.00         3

avg / total       0.99      0.99      0.99       438


EVALUATION FOR LABEL: BAMBINI ANCHE SOLI (ET� SCOLARE)
             precision    recall  f1-score   support

          0       0.96      1.00      0.98       417
          1       0.80      0.19      0.31        21

avg / total       0.95      0.96      0.95       438


EVALUATION FOR LABEL: FAMILY (BAMBINI + ADULTI)
             precision    recall  f1-score   support

          0       0.92      1.00      0.95       391
          1       0.92      0.23      0.37        47

avg / total       0.92      0.92      0.89       438


EVALUATION FOR LABEL: FEMMINILE
             precision    recall  f1-score   support

          0       0.91      1.00      0.96       396
          1       1.00      0.12      0.21        42

avg / total       0.92      0.92      0.88       438


EVALUATION FOR LABEL: GIOVANI ADULTI (20-40)
             precision    recall  f1-score   support

          0       0.80      0.17      0.28        71
          1       0.86      0.99      0.92       367

avg / total       0.85      0.86      0.82       438


EVALUATION FOR LABEL: MASCHILE
             precision    recall  f1-score   support

          0       0.79      0.92      0.85       309
          1       0.69      0.42      0.52       129

avg / total       0.76      0.77      0.75       438


EVALUATION FOR LABEL: TEENS (13-19)
             precision    recall  f1-score   support

          0       0.85      0.99      0.91       357
          1       0.83      0.23      0.37        81

avg / total       0.85      0.85      0.81       438


