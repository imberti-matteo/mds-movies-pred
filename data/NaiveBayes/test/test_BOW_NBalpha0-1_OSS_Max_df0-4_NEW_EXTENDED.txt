EVALUATION FOR LABEL: ADRENALINICO/MOZZAFIATO
             precision    recall  f1-score   support

          0       0.89      0.95      0.92       607
          1       0.80      0.62      0.70       181

avg / total       0.87      0.88      0.87       788


EVALUATION FOR LABEL: AGGHIACCIANTE-TERRIFICANTE
             precision    recall  f1-score   support

          0       0.97      0.98      0.97       747
          1       0.50      0.39      0.44        41

avg / total       0.94      0.95      0.94       788


EVALUATION FOR LABEL: AGRODOLCE
             precision    recall  f1-score   support

          0       0.92      0.82      0.87       686
          1       0.29      0.49      0.37       102

avg / total       0.84      0.78      0.80       788


EVALUATION FOR LABEL: ARRAPANTE
             precision    recall  f1-score   support

          0       0.98      1.00      0.99       774
          1       0.50      0.07      0.12        14

avg / total       0.97      0.98      0.98       788


EVALUATION FOR LABEL: CEREBRALE
             precision    recall  f1-score   support

          0       0.98      0.99      0.98       771
          1       0.00      0.00      0.00        17

avg / total       0.96      0.97      0.96       788


EVALUATION FOR LABEL: CHE FA PENSARE/INSPIRING
             precision    recall  f1-score   support

          0       0.89      0.83      0.86       657
          1       0.36      0.49      0.42       131

avg / total       0.80      0.77      0.79       788


EVALUATION FOR LABEL: CHE FA SOGNARE
             precision    recall  f1-score   support

          0       0.97      0.99      0.98       757
          1       0.58      0.23      0.33        31

avg / total       0.95      0.96      0.96       788


EVALUATION FOR LABEL: COMMOVENTE/STRAPPALACRIME
             precision    recall  f1-score   support

          0       0.93      0.95      0.94       727
          1       0.26      0.20      0.22        61

avg / total       0.88      0.89      0.89       788


EVALUATION FOR LABEL: ESALTANTE/TRASCINANTE/EPICO
             precision    recall  f1-score   support

          0       0.95      0.96      0.96       724
          1       0.50      0.42      0.46        64

avg / total       0.91      0.92      0.92       788


EVALUATION FOR LABEL: FA MORIRE DAL RIDERE
             precision    recall  f1-score   support

          0       0.91      0.93      0.92       698
          1       0.37      0.31      0.34        90

avg / total       0.85      0.86      0.86       788


EVALUATION FOR LABEL: FEEL-GOOD
             precision    recall  f1-score   support

          0       0.90      0.90      0.90       695
          1       0.25      0.24      0.24        93

avg / total       0.82      0.82      0.82       788


EVALUATION FOR LABEL: INQUIETANTE/ANGOSCIANTE
             precision    recall  f1-score   support

          0       0.89      0.94      0.92       676
          1       0.49      0.33      0.40       112

avg / total       0.84      0.86      0.84       788


EVALUATION FOR LABEL: NOSTALGICO/MELANCONICO
             precision    recall  f1-score   support

          0       0.96      0.95      0.95       746
          1       0.24      0.31      0.27        42

avg / total       0.92      0.91      0.92       788


EVALUATION FOR LABEL: PROVOCA RABBIA/INDIGNAZIONE
             precision    recall  f1-score   support

          0       1.00      1.00      1.00       788

avg / total       1.00      1.00      1.00       788


EVALUATION FOR LABEL: RASSICURANTE
             precision    recall  f1-score   support

          0       0.98      0.99      0.99       776
          1       0.00      0.00      0.00        12

avg / total       0.97      0.98      0.97       788


EVALUATION FOR LABEL: ROMANTICO
             precision    recall  f1-score   support

          0       0.94      0.97      0.96       729
          1       0.41      0.25      0.31        59

avg / total       0.90      0.92      0.91       788


EVALUATION FOR LABEL: SCACCIAPENSIERI - EVASIVO
             precision    recall  f1-score   support

          0       0.83      0.83      0.83       608
          1       0.42      0.42      0.42       180

avg / total       0.74      0.73      0.74       788


EVALUATION FOR LABEL: SFAVILLANTE/STYLISH
             precision    recall  f1-score   support

          0       0.98      1.00      0.99       772
          1       0.00      0.00      0.00        16

avg / total       0.96      0.98      0.97       788


EVALUATION FOR LABEL: SORPRENDENTE
             precision    recall  f1-score   support

          0       0.96      0.99      0.98       757
          1       0.00      0.00      0.00        31

avg / total       0.92      0.95      0.94       788


EVALUATION FOR LABEL: SPIAZZANTE
             precision    recall  f1-score   support

          0       0.93      0.99      0.96       734
          1       0.30      0.06      0.09        54

avg / total       0.89      0.93      0.90       788


EVALUATION FOR LABEL: ADULTI (40-OLTRE)
             precision    recall  f1-score   support

          0       0.67      0.47      0.55       275
          1       0.75      0.88      0.81       513

avg / total       0.72      0.73      0.72       788


EVALUATION FOR LABEL: BAMBINI ANCHE SOLI (ET� PRE-SCOLARE)
             precision    recall  f1-score   support

          0       0.99      0.99      0.99       775
          1       0.56      0.38      0.45        13

avg / total       0.98      0.98      0.98       788


EVALUATION FOR LABEL: BAMBINI ANCHE SOLI (ET� SCOLARE)
             precision    recall  f1-score   support

          0       0.98      0.99      0.98       755
          1       0.56      0.42      0.48        33

avg / total       0.96      0.96      0.96       788


EVALUATION FOR LABEL: FAMILY (BAMBINI + ADULTI)
             precision    recall  f1-score   support

          0       0.93      0.97      0.95       696
          1       0.67      0.43      0.53        92

avg / total       0.90      0.91      0.90       788


EVALUATION FOR LABEL: FEMMINILE
             precision    recall  f1-score   support

          0       0.92      0.92      0.92       681
          1       0.50      0.50      0.50       107

avg / total       0.86      0.87      0.86       788


EVALUATION FOR LABEL: GIOVANI ADULTI (20-40)
             precision    recall  f1-score   support

          0       0.45      0.33      0.38       149
          1       0.85      0.90      0.88       639

avg / total       0.78      0.80      0.78       788


EVALUATION FOR LABEL: MASCHILE
             precision    recall  f1-score   support

          0       0.85      0.91      0.88       581
          1       0.70      0.56      0.62       207

avg / total       0.81      0.82      0.81       788


EVALUATION FOR LABEL: TEENS (13-19)
             precision    recall  f1-score   support

          0       0.90      0.96      0.93       679
          1       0.57      0.32      0.41       109

avg / total       0.85      0.87      0.86       788


