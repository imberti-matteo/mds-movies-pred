EVALUATION FOR LABEL: ADRENALINICO/MOZZAFIATO
             precision    recall  f1-score   support

          0       0.84      0.79      0.81       321
          1       0.50      0.58      0.54       117

avg / total       0.75      0.74      0.74       438


EVALUATION FOR LABEL: AGGHIACCIANTE-TERRIFICANTE
             precision    recall  f1-score   support

          0       0.98      0.89      0.93       406
          1       0.33      0.72      0.46        32

avg / total       0.93      0.87      0.89       438


EVALUATION FOR LABEL: AGRODOLCE
             precision    recall  f1-score   support

          0       0.96      0.54      0.69       386
          1       0.20      0.85      0.32        52

avg / total       0.87      0.58      0.65       438


EVALUATION FOR LABEL: ARRAPANTE
             precision    recall  f1-score   support

          0       0.98      0.53      0.69       429
          1       0.02      0.56      0.05         9

avg / total       0.96      0.53      0.67       438


EVALUATION FOR LABEL: CEREBRALE
             precision    recall  f1-score   support

          0       0.97      0.86      0.91       422
          1       0.08      0.31      0.12        16

avg / total       0.94      0.84      0.88       438


EVALUATION FOR LABEL: CHE FA PENSARE/INSPIRING
             precision    recall  f1-score   support

          0       0.91      0.43      0.59       373
          1       0.19      0.74      0.30        65

avg / total       0.80      0.48      0.54       438


EVALUATION FOR LABEL: CHE FA SOGNARE
             precision    recall  f1-score   support

          0       0.96      0.79      0.87       417
          1       0.07      0.33      0.12        21

avg / total       0.92      0.77      0.83       438


EVALUATION FOR LABEL: COMMOVENTE/STRAPPALACRIME
             precision    recall  f1-score   support

          0       1.00      0.49      0.66       414
          1       0.10      0.96      0.18        24

avg / total       0.95      0.52      0.63       438


EVALUATION FOR LABEL: ESALTANTE/TRASCINANTE/EPICO
             precision    recall  f1-score   support

          0       0.97      0.81      0.88       392
          1       0.32      0.76      0.45        46

avg / total       0.90      0.81      0.84       438


EVALUATION FOR LABEL: FA MORIRE DAL RIDERE
             precision    recall  f1-score   support

          0       0.94      0.46      0.62       399
          1       0.11      0.69      0.19        39

avg / total       0.86      0.48      0.58       438


EVALUATION FOR LABEL: FEEL-GOOD
             precision    recall  f1-score   support

          0       0.94      0.46      0.62       390
          1       0.15      0.77      0.25        48

avg / total       0.86      0.50      0.58       438


EVALUATION FOR LABEL: INQUIETANTE/ANGOSCIANTE
             precision    recall  f1-score   support

          0       0.90      0.79      0.84       371
          1       0.30      0.49      0.37        67

avg / total       0.80      0.74      0.77       438


EVALUATION FOR LABEL: NOSTALGICO/MELANCONICO
             precision    recall  f1-score   support

          0       0.97      0.46      0.63       409
          1       0.10      0.83      0.18        29

avg / total       0.92      0.49      0.60       438


EVALUATION FOR LABEL: PROVOCA RABBIA/INDIGNAZIONE
             precision    recall  f1-score   support

          0       1.00      1.00      1.00       438

avg / total       1.00      1.00      1.00       438


EVALUATION FOR LABEL: RASSICURANTE
             precision    recall  f1-score   support

          0       0.99      0.58      0.73       433
          1       0.02      0.60      0.03         5

avg / total       0.98      0.58      0.73       438


EVALUATION FOR LABEL: ROMANTICO
             precision    recall  f1-score   support

          0       0.99      0.75      0.86       416
          1       0.16      0.91      0.28        22

avg / total       0.95      0.76      0.83       438


EVALUATION FOR LABEL: SCACCIAPENSIERI - EVASIVO
             precision    recall  f1-score   support

          0       0.92      0.47      0.62       357
          1       0.26      0.81      0.39        81

avg / total       0.80      0.53      0.58       438


EVALUATION FOR LABEL: SFAVILLANTE/STYLISH
             precision    recall  f1-score   support

          0       0.98      0.55      0.70       427
          1       0.03      0.55      0.06        11

avg / total       0.96      0.55      0.69       438


EVALUATION FOR LABEL: SORPRENDENTE
             precision    recall  f1-score   support

          0       0.94      0.75      0.83       408
          1       0.10      0.37      0.15        30

avg / total       0.88      0.72      0.79       438


EVALUATION FOR LABEL: SPIAZZANTE
             precision    recall  f1-score   support

          0       0.92      0.78      0.85       394
          1       0.18      0.43      0.25        44

avg / total       0.85      0.74      0.79       438


EVALUATION FOR LABEL: ADULTI (40-OLTRE)
             precision    recall  f1-score   support

          0       0.47      0.40      0.43       146
          1       0.72      0.77      0.75       292

avg / total       0.64      0.65      0.64       438


EVALUATION FOR LABEL: BAMBINI ANCHE SOLI (ET� PRE-SCOLARE)
             precision    recall  f1-score   support

          0       1.00      0.80      0.89       435
          1       0.02      0.67      0.04         3

avg / total       0.99      0.80      0.88       438


EVALUATION FOR LABEL: BAMBINI ANCHE SOLI (ET� SCOLARE)
             precision    recall  f1-score   support

          0       0.99      0.58      0.73       417
          1       0.09      0.86      0.17        21

avg / total       0.94      0.59      0.70       438


EVALUATION FOR LABEL: FAMILY (BAMBINI + ADULTI)
             precision    recall  f1-score   support

          0       0.95      0.83      0.89       391
          1       0.32      0.66      0.43        47

avg / total       0.89      0.82      0.84       438


EVALUATION FOR LABEL: FEMMINILE
             precision    recall  f1-score   support

          0       0.97      0.56      0.71       396
          1       0.17      0.83      0.28        42

avg / total       0.89      0.58      0.67       438


EVALUATION FOR LABEL: GIOVANI ADULTI (20-40)
             precision    recall  f1-score   support

          0       0.22      0.80      0.35        71
          1       0.92      0.45      0.60       367

avg / total       0.81      0.51      0.56       438


EVALUATION FOR LABEL: MASCHILE
             precision    recall  f1-score   support

          0       0.81      0.77      0.79       309
          1       0.50      0.57      0.53       129

avg / total       0.72      0.71      0.71       438


EVALUATION FOR LABEL: TEENS (13-19)
             precision    recall  f1-score   support

          0       0.86      0.75      0.80       357
          1       0.28      0.44      0.35        81

avg / total       0.75      0.69      0.71       438


