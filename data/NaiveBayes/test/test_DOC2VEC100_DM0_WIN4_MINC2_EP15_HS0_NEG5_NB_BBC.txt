EVALUATION FOR LABEL: ADRENALINICO/MOZZAFIATO
             precision    recall  f1-score   support

          0       0.93      0.77      0.84       321
          1       0.57      0.85      0.68       117

avg / total       0.84      0.79      0.80       438


EVALUATION FOR LABEL: AGGHIACCIANTE-TERRIFICANTE
             precision    recall  f1-score   support

          0       0.98      0.90      0.94       406
          1       0.39      0.78      0.52        32

avg / total       0.94      0.89      0.91       438


EVALUATION FOR LABEL: AGRODOLCE
             precision    recall  f1-score   support

          0       0.94      0.77      0.85       386
          1       0.27      0.63      0.38        52

avg / total       0.86      0.76      0.79       438


EVALUATION FOR LABEL: ARRAPANTE
             precision    recall  f1-score   support

          0       0.98      0.90      0.94       429
          1       0.04      0.22      0.07         9

avg / total       0.96      0.89      0.92       438


EVALUATION FOR LABEL: CEREBRALE
             precision    recall  f1-score   support

          0       0.98      0.80      0.88       422
          1       0.11      0.62      0.18        16

avg / total       0.95      0.79      0.86       438


EVALUATION FOR LABEL: CHE FA PENSARE/INSPIRING
             precision    recall  f1-score   support

          0       0.93      0.75      0.83       373
          1       0.31      0.66      0.42        65

avg / total       0.84      0.73      0.77       438


EVALUATION FOR LABEL: CHE FA SOGNARE
             precision    recall  f1-score   support

          0       0.97      0.90      0.94       417
          1       0.20      0.48      0.28        21

avg / total       0.93      0.88      0.90       438


EVALUATION FOR LABEL: COMMOVENTE/STRAPPALACRIME
             precision    recall  f1-score   support

          0       0.97      0.80      0.88       414
          1       0.14      0.58      0.23        24

avg / total       0.93      0.79      0.84       438


EVALUATION FOR LABEL: ESALTANTE/TRASCINANTE/EPICO
             precision    recall  f1-score   support

          0       0.96      0.82      0.89       392
          1       0.32      0.70      0.44        46

avg / total       0.89      0.81      0.84       438


EVALUATION FOR LABEL: FA MORIRE DAL RIDERE
             precision    recall  f1-score   support

          0       0.96      0.78      0.86       399
          1       0.22      0.64      0.33        39

avg / total       0.89      0.77      0.81       438


EVALUATION FOR LABEL: FEEL-GOOD
             precision    recall  f1-score   support

          0       0.97      0.74      0.84       390
          1       0.27      0.79      0.40        48

avg / total       0.89      0.74      0.79       438


EVALUATION FOR LABEL: INQUIETANTE/ANGOSCIANTE
             precision    recall  f1-score   support

          0       0.94      0.79      0.86       371
          1       0.39      0.73      0.51        67

avg / total       0.86      0.78      0.81       438


EVALUATION FOR LABEL: NOSTALGICO/MELANCONICO
             precision    recall  f1-score   support

          0       0.96      0.78      0.86       409
          1       0.16      0.59      0.25        29

avg / total       0.91      0.77      0.82       438


EVALUATION FOR LABEL: PROVOCA RABBIA/INDIGNAZIONE
             precision    recall  f1-score   support

          0       1.00      1.00      1.00       438

avg / total       1.00      1.00      1.00       438


EVALUATION FOR LABEL: RASSICURANTE
             precision    recall  f1-score   support

          0       0.99      0.91      0.95       433
          1       0.05      0.40      0.08         5

avg / total       0.98      0.90      0.94       438


EVALUATION FOR LABEL: ROMANTICO
             precision    recall  f1-score   support

          0       0.98      0.85      0.91       416
          1       0.20      0.73      0.31        22

avg / total       0.94      0.84      0.88       438


EVALUATION FOR LABEL: SCACCIAPENSIERI - EVASIVO
             precision    recall  f1-score   support

          0       0.94      0.74      0.83       357
          1       0.41      0.79      0.54        81

avg / total       0.84      0.75      0.77       438


EVALUATION FOR LABEL: SFAVILLANTE/STYLISH
             precision    recall  f1-score   support

          0       0.98      0.88      0.93       427
          1       0.07      0.36      0.12        11

avg / total       0.96      0.87      0.91       438


EVALUATION FOR LABEL: SORPRENDENTE
             precision    recall  f1-score   support

          0       0.94      0.77      0.85       408
          1       0.10      0.33      0.15        30

avg / total       0.88      0.74      0.80       438


EVALUATION FOR LABEL: SPIAZZANTE
             precision    recall  f1-score   support

          0       0.95      0.75      0.84       394
          1       0.23      0.68      0.34        44

avg / total       0.88      0.74      0.79       438


EVALUATION FOR LABEL: ADULTI (40-OLTRE)
             precision    recall  f1-score   support

          0       0.58      0.75      0.65       146
          1       0.85      0.73      0.79       292

avg / total       0.76      0.74      0.74       438


EVALUATION FOR LABEL: BAMBINI ANCHE SOLI (ET� PRE-SCOLARE)
             precision    recall  f1-score   support

          0       1.00      0.99      0.99       435
          1       0.20      0.33      0.25         3

avg / total       0.99      0.99      0.99       438


EVALUATION FOR LABEL: BAMBINI ANCHE SOLI (ET� SCOLARE)
             precision    recall  f1-score   support

          0       0.99      0.89      0.94       417
          1       0.29      0.90      0.44        21

avg / total       0.96      0.89      0.91       438


EVALUATION FOR LABEL: FAMILY (BAMBINI + ADULTI)
             precision    recall  f1-score   support

          0       0.98      0.84      0.90       391
          1       0.39      0.85      0.53        47

avg / total       0.92      0.84      0.86       438


EVALUATION FOR LABEL: FEMMINILE
             precision    recall  f1-score   support

          0       0.97      0.83      0.89       396
          1       0.33      0.79      0.46        42

avg / total       0.91      0.82      0.85       438


EVALUATION FOR LABEL: GIOVANI ADULTI (20-40)
             precision    recall  f1-score   support

          0       0.34      0.66      0.45        71
          1       0.92      0.75      0.83       367

avg / total       0.83      0.74      0.77       438


EVALUATION FOR LABEL: MASCHILE
             precision    recall  f1-score   support

          0       0.87      0.72      0.79       309
          1       0.52      0.74      0.61       129

avg / total       0.77      0.72      0.73       438


EVALUATION FOR LABEL: TEENS (13-19)
             precision    recall  f1-score   support

          0       0.94      0.77      0.84       357
          1       0.43      0.77      0.55        81

avg / total       0.84      0.77      0.79       438


