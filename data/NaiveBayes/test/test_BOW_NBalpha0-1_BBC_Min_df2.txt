EVALUATION FOR LABEL: ADRENALINICO/MOZZAFIATO
             precision    recall  f1-score   support

          0       0.91      0.82      0.86       321
          1       0.60      0.77      0.68       117

avg / total       0.83      0.80      0.81       438


EVALUATION FOR LABEL: AGGHIACCIANTE-TERRIFICANTE
             precision    recall  f1-score   support

          0       0.97      0.93      0.95       406
          1       0.41      0.66      0.51        32

avg / total       0.93      0.91      0.92       438


EVALUATION FOR LABEL: AGRODOLCE
             precision    recall  f1-score   support

          0       0.92      0.86      0.89       386
          1       0.29      0.44      0.35        52

avg / total       0.85      0.81      0.82       438


EVALUATION FOR LABEL: ARRAPANTE
             precision    recall  f1-score   support

          0       0.98      0.90      0.94       429
          1       0.05      0.22      0.08         9

avg / total       0.96      0.89      0.92       438


EVALUATION FOR LABEL: CEREBRALE
             precision    recall  f1-score   support

          0       0.97      0.95      0.96       422
          1       0.17      0.25      0.20        16

avg / total       0.94      0.93      0.93       438


EVALUATION FOR LABEL: CHE FA PENSARE/INSPIRING
             precision    recall  f1-score   support

          0       0.90      0.79      0.84       373
          1       0.30      0.51      0.38        65

avg / total       0.81      0.75      0.77       438


EVALUATION FOR LABEL: CHE FA SOGNARE
             precision    recall  f1-score   support

          0       0.97      0.93      0.95       417
          1       0.28      0.52      0.36        21

avg / total       0.94      0.91      0.92       438


EVALUATION FOR LABEL: COMMOVENTE/STRAPPALACRIME
             precision    recall  f1-score   support

          0       0.97      0.86      0.91       414
          1       0.17      0.50      0.25        24

avg / total       0.92      0.84      0.87       438


EVALUATION FOR LABEL: ESALTANTE/TRASCINANTE/EPICO
             precision    recall  f1-score   support

          0       0.96      0.83      0.89       392
          1       0.33      0.72      0.45        46

avg / total       0.89      0.82      0.84       438


EVALUATION FOR LABEL: FA MORIRE DAL RIDERE
             precision    recall  f1-score   support

          0       0.94      0.89      0.92       399
          1       0.28      0.44      0.34        39

avg / total       0.88      0.85      0.87       438


EVALUATION FOR LABEL: FEEL-GOOD
             precision    recall  f1-score   support

          0       0.92      0.85      0.88       390
          1       0.23      0.38      0.29        48

avg / total       0.84      0.79      0.81       438


EVALUATION FOR LABEL: INQUIETANTE/ANGOSCIANTE
             precision    recall  f1-score   support

          0       0.91      0.86      0.88       371
          1       0.40      0.51      0.44        67

avg / total       0.83      0.81      0.82       438


EVALUATION FOR LABEL: NOSTALGICO/MELANCONICO
             precision    recall  f1-score   support

          0       0.96      0.86      0.91       409
          1       0.20      0.48      0.28        29

avg / total       0.91      0.84      0.87       438


EVALUATION FOR LABEL: PROVOCA RABBIA/INDIGNAZIONE
             precision    recall  f1-score   support

          0       1.00      1.00      1.00       438

avg / total       1.00      1.00      1.00       438


EVALUATION FOR LABEL: RASSICURANTE
             precision    recall  f1-score   support

          0       0.99      0.94      0.96       433
          1       0.04      0.20      0.06         5

avg / total       0.98      0.93      0.95       438


EVALUATION FOR LABEL: ROMANTICO
             precision    recall  f1-score   support

          0       0.97      0.88      0.92       416
          1       0.17      0.50      0.26        22

avg / total       0.93      0.86      0.89       438


EVALUATION FOR LABEL: SCACCIAPENSIERI - EVASIVO
             precision    recall  f1-score   support

          0       0.87      0.85      0.86       357
          1       0.41      0.46      0.43        81

avg / total       0.79      0.78      0.78       438


EVALUATION FOR LABEL: SFAVILLANTE/STYLISH
             precision    recall  f1-score   support

          0       0.98      0.91      0.94       427
          1       0.05      0.18      0.08        11

avg / total       0.95      0.89      0.92       438


EVALUATION FOR LABEL: SORPRENDENTE
             precision    recall  f1-score   support

          0       0.94      0.93      0.93       408
          1       0.12      0.13      0.13        30

avg / total       0.88      0.87      0.88       438


EVALUATION FOR LABEL: SPIAZZANTE
             precision    recall  f1-score   support

          0       0.92      0.91      0.91       394
          1       0.23      0.25      0.24        44

avg / total       0.85      0.84      0.84       438


EVALUATION FOR LABEL: ADULTI (40-OLTRE)
             precision    recall  f1-score   support

          0       0.60      0.62      0.61       146
          1       0.80      0.79      0.80       292

avg / total       0.74      0.73      0.73       438


EVALUATION FOR LABEL: BAMBINI ANCHE SOLI (ET� PRE-SCOLARE)
             precision    recall  f1-score   support

          0       1.00      0.93      0.96       435
          1       0.03      0.33      0.06         3

avg / total       0.99      0.93      0.96       438


EVALUATION FOR LABEL: BAMBINI ANCHE SOLI (ET� SCOLARE)
             precision    recall  f1-score   support

          0       0.99      0.93      0.96       417
          1       0.39      0.86      0.54        21

avg / total       0.96      0.93      0.94       438


EVALUATION FOR LABEL: FAMILY (BAMBINI + ADULTI)
             precision    recall  f1-score   support

          0       0.96      0.93      0.94       391
          1       0.52      0.66      0.58        47

avg / total       0.91      0.90      0.90       438


EVALUATION FOR LABEL: FEMMINILE
             precision    recall  f1-score   support

          0       0.96      0.86      0.91       396
          1       0.33      0.67      0.44        42

avg / total       0.90      0.84      0.86       438


EVALUATION FOR LABEL: GIOVANI ADULTI (20-40)
             precision    recall  f1-score   support

          0       0.46      0.46      0.46        71
          1       0.90      0.90      0.90       367

avg / total       0.83      0.83      0.83       438


EVALUATION FOR LABEL: MASCHILE
             precision    recall  f1-score   support

          0       0.88      0.76      0.81       309
          1       0.56      0.74      0.64       129

avg / total       0.78      0.75      0.76       438


EVALUATION FOR LABEL: TEENS (13-19)
             precision    recall  f1-score   support

          0       0.90      0.84      0.87       357
          1       0.46      0.60      0.52        81

avg / total       0.82      0.79      0.80       438


