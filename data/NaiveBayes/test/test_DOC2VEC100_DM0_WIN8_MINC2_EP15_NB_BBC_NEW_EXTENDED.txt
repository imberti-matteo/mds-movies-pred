EVALUATION FOR LABEL: ADRENALINICO/MOZZAFIATO
             precision    recall  f1-score   support

          0       0.94      0.80      0.86       607
          1       0.55      0.84      0.67       181

avg / total       0.85      0.81      0.82       788


EVALUATION FOR LABEL: AGGHIACCIANTE-TERRIFICANTE
             precision    recall  f1-score   support

          0       0.99      0.89      0.94       747
          1       0.30      0.83      0.44        41

avg / total       0.95      0.89      0.91       788


EVALUATION FOR LABEL: AGRODOLCE
             precision    recall  f1-score   support

          0       0.94      0.70      0.80       686
          1       0.25      0.69      0.37       102

avg / total       0.85      0.70      0.74       788


EVALUATION FOR LABEL: ARRAPANTE
             precision    recall  f1-score   support

          0       0.99      0.83      0.91       774
          1       0.07      0.71      0.13        14

avg / total       0.98      0.83      0.89       788


EVALUATION FOR LABEL: CEREBRALE
             precision    recall  f1-score   support

          0       0.99      0.80      0.88       771
          1       0.05      0.47      0.09        17

avg / total       0.97      0.80      0.87       788


EVALUATION FOR LABEL: CHE FA PENSARE/INSPIRING
             precision    recall  f1-score   support

          0       0.92      0.66      0.77       657
          1       0.29      0.69      0.41       131

avg / total       0.81      0.67      0.71       788


EVALUATION FOR LABEL: CHE FA SOGNARE
             precision    recall  f1-score   support

          0       0.98      0.90      0.94       757
          1       0.19      0.55      0.28        31

avg / total       0.95      0.89      0.91       788


EVALUATION FOR LABEL: COMMOVENTE/STRAPPALACRIME
             precision    recall  f1-score   support

          0       0.96      0.74      0.83       727
          1       0.16      0.61      0.26        61

avg / total       0.90      0.73      0.79       788


EVALUATION FOR LABEL: ESALTANTE/TRASCINANTE/EPICO
             precision    recall  f1-score   support

          0       0.97      0.83      0.90       724
          1       0.26      0.67      0.38        64

avg / total       0.91      0.82      0.85       788


EVALUATION FOR LABEL: FA MORIRE DAL RIDERE
             precision    recall  f1-score   support

          0       0.95      0.72      0.82       698
          1       0.25      0.72      0.37        90

avg / total       0.87      0.72      0.77       788


EVALUATION FOR LABEL: FEEL-GOOD
             precision    recall  f1-score   support

          0       0.96      0.71      0.82       695
          1       0.27      0.81      0.40        93

avg / total       0.88      0.72      0.77       788


EVALUATION FOR LABEL: INQUIETANTE/ANGOSCIANTE
             precision    recall  f1-score   support

          0       0.95      0.81      0.88       676
          1       0.40      0.77      0.52       112

avg / total       0.88      0.80      0.83       788


EVALUATION FOR LABEL: NOSTALGICO/MELANCONICO
             precision    recall  f1-score   support

          0       0.98      0.70      0.82       746
          1       0.12      0.74      0.21        42

avg / total       0.93      0.71      0.79       788


EVALUATION FOR LABEL: PROVOCA RABBIA/INDIGNAZIONE
             precision    recall  f1-score   support

          0       1.00      1.00      1.00       788

avg / total       1.00      1.00      1.00       788


EVALUATION FOR LABEL: RASSICURANTE
             precision    recall  f1-score   support

          0       0.99      0.82      0.90       776
          1       0.02      0.25      0.04        12

avg / total       0.97      0.81      0.88       788


EVALUATION FOR LABEL: ROMANTICO
             precision    recall  f1-score   support

          0       0.98      0.81      0.88       729
          1       0.24      0.75      0.36        59

avg / total       0.92      0.80      0.84       788


EVALUATION FOR LABEL: SCACCIAPENSIERI - EVASIVO
             precision    recall  f1-score   support

          0       0.90      0.72      0.80       608
          1       0.43      0.73      0.54       180

avg / total       0.79      0.72      0.74       788


EVALUATION FOR LABEL: SFAVILLANTE/STYLISH
             precision    recall  f1-score   support

          0       0.99      0.82      0.90       772
          1       0.08      0.75      0.15        16

avg / total       0.98      0.82      0.89       788


EVALUATION FOR LABEL: SORPRENDENTE
             precision    recall  f1-score   support

          0       0.96      0.74      0.84       757
          1       0.05      0.32      0.09        31

avg / total       0.93      0.73      0.81       788


EVALUATION FOR LABEL: SPIAZZANTE
             precision    recall  f1-score   support

          0       0.96      0.72      0.83       734
          1       0.14      0.59      0.22        54

avg / total       0.90      0.71      0.78       788


EVALUATION FOR LABEL: ADULTI (40-OLTRE)
             precision    recall  f1-score   support

          0       0.59      0.69      0.64       275
          1       0.82      0.74      0.78       513

avg / total       0.74      0.73      0.73       788


EVALUATION FOR LABEL: BAMBINI ANCHE SOLI (ET� PRE-SCOLARE)
             precision    recall  f1-score   support

          0       0.99      0.92      0.96       775
          1       0.13      0.69      0.22        13

avg / total       0.98      0.92      0.94       788


EVALUATION FOR LABEL: BAMBINI ANCHE SOLI (ET� SCOLARE)
             precision    recall  f1-score   support

          0       0.99      0.90      0.94       755
          1       0.25      0.79      0.38        33

avg / total       0.96      0.89      0.92       788


EVALUATION FOR LABEL: FAMILY (BAMBINI + ADULTI)
             precision    recall  f1-score   support

          0       0.96      0.87      0.91       696
          1       0.41      0.71      0.52        92

avg / total       0.89      0.85      0.86       788


EVALUATION FOR LABEL: FEMMINILE
             precision    recall  f1-score   support

          0       0.97      0.78      0.87       681
          1       0.38      0.84      0.52       107

avg / total       0.89      0.79      0.82       788


EVALUATION FOR LABEL: GIOVANI ADULTI (20-40)
             precision    recall  f1-score   support

          0       0.36      0.63      0.46       149
          1       0.90      0.74      0.81       639

avg / total       0.80      0.72      0.75       788


EVALUATION FOR LABEL: MASCHILE
             precision    recall  f1-score   support

          0       0.88      0.75      0.81       581
          1       0.50      0.72      0.59       207

avg / total       0.78      0.74      0.75       788


EVALUATION FOR LABEL: TEENS (13-19)
             precision    recall  f1-score   support

          0       0.95      0.74      0.83       679
          1       0.32      0.76      0.45       109

avg / total       0.86      0.74      0.78       788


