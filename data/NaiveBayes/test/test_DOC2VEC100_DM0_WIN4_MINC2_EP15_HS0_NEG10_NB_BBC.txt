EVALUATION FOR LABEL: ADRENALINICO/MOZZAFIATO
             precision    recall  f1-score   support

          0       0.91      0.78      0.84       321
          1       0.57      0.79      0.66       117

avg / total       0.82      0.79      0.79       438


EVALUATION FOR LABEL: AGGHIACCIANTE-TERRIFICANTE
             precision    recall  f1-score   support

          0       0.98      0.91      0.94       406
          1       0.39      0.75      0.51        32

avg / total       0.94      0.89      0.91       438


EVALUATION FOR LABEL: AGRODOLCE
             precision    recall  f1-score   support

          0       0.95      0.77      0.85       386
          1       0.29      0.71      0.42        52

avg / total       0.87      0.76      0.80       438


EVALUATION FOR LABEL: ARRAPANTE
             precision    recall  f1-score   support

          0       0.99      0.87      0.92       429
          1       0.07      0.44      0.12         9

avg / total       0.97      0.86      0.91       438


EVALUATION FOR LABEL: CEREBRALE
             precision    recall  f1-score   support

          0       0.97      0.82      0.89       422
          1       0.09      0.44      0.14        16

avg / total       0.94      0.81      0.86       438


EVALUATION FOR LABEL: CHE FA PENSARE/INSPIRING
             precision    recall  f1-score   support

          0       0.93      0.75      0.83       373
          1       0.33      0.69      0.45        65

avg / total       0.84      0.74      0.78       438


EVALUATION FOR LABEL: CHE FA SOGNARE
             precision    recall  f1-score   support

          0       0.98      0.87      0.92       417
          1       0.20      0.67      0.31        21

avg / total       0.94      0.86      0.89       438


EVALUATION FOR LABEL: COMMOVENTE/STRAPPALACRIME
             precision    recall  f1-score   support

          0       0.98      0.81      0.89       414
          1       0.17      0.67      0.27        24

avg / total       0.93      0.80      0.85       438


EVALUATION FOR LABEL: ESALTANTE/TRASCINANTE/EPICO
             precision    recall  f1-score   support

          0       0.97      0.78      0.86       392
          1       0.28      0.76      0.41        46

avg / total       0.89      0.77      0.81       438


EVALUATION FOR LABEL: FA MORIRE DAL RIDERE
             precision    recall  f1-score   support

          0       0.97      0.80      0.88       399
          1       0.27      0.74      0.39        39

avg / total       0.91      0.80      0.83       438


EVALUATION FOR LABEL: FEEL-GOOD
             precision    recall  f1-score   support

          0       0.95      0.74      0.83       390
          1       0.25      0.71      0.37        48

avg / total       0.88      0.73      0.78       438


EVALUATION FOR LABEL: INQUIETANTE/ANGOSCIANTE
             precision    recall  f1-score   support

          0       0.94      0.79      0.86       371
          1       0.38      0.70      0.49        67

avg / total       0.85      0.78      0.80       438


EVALUATION FOR LABEL: NOSTALGICO/MELANCONICO
             precision    recall  f1-score   support

          0       0.98      0.78      0.87       409
          1       0.19      0.72      0.30        29

avg / total       0.92      0.78      0.83       438


EVALUATION FOR LABEL: PROVOCA RABBIA/INDIGNAZIONE
             precision    recall  f1-score   support

          0       1.00      1.00      1.00       438

avg / total       1.00      1.00      1.00       438


EVALUATION FOR LABEL: RASSICURANTE
             precision    recall  f1-score   support

          0       0.99      0.90      0.94       433
          1       0.02      0.20      0.04         5

avg / total       0.98      0.89      0.93       438


EVALUATION FOR LABEL: ROMANTICO
             precision    recall  f1-score   support

          0       0.98      0.85      0.91       416
          1       0.20      0.73      0.32        22

avg / total       0.94      0.84      0.88       438


EVALUATION FOR LABEL: SCACCIAPENSIERI - EVASIVO
             precision    recall  f1-score   support

          0       0.95      0.74      0.83       357
          1       0.41      0.81      0.55        81

avg / total       0.85      0.75      0.78       438


EVALUATION FOR LABEL: SFAVILLANTE/STYLISH
             precision    recall  f1-score   support

          0       0.98      0.92      0.95       427
          1       0.06      0.18      0.09        11

avg / total       0.95      0.90      0.93       438


EVALUATION FOR LABEL: SORPRENDENTE
             precision    recall  f1-score   support

          0       0.93      0.81      0.87       408
          1       0.06      0.17      0.09        30

avg / total       0.87      0.77      0.81       438


EVALUATION FOR LABEL: SPIAZZANTE
             precision    recall  f1-score   support

          0       0.94      0.78      0.85       394
          1       0.22      0.57      0.32        44

avg / total       0.87      0.76      0.80       438


EVALUATION FOR LABEL: ADULTI (40-OLTRE)
             precision    recall  f1-score   support

          0       0.60      0.79      0.68       146
          1       0.88      0.73      0.80       292

avg / total       0.78      0.75      0.76       438


EVALUATION FOR LABEL: BAMBINI ANCHE SOLI (ET� PRE-SCOLARE)
             precision    recall  f1-score   support

          0       0.99      0.99      0.99       435
          1       0.00      0.00      0.00         3

avg / total       0.99      0.98      0.98       438


EVALUATION FOR LABEL: BAMBINI ANCHE SOLI (ET� SCOLARE)
             precision    recall  f1-score   support

          0       1.00      0.89      0.94       417
          1       0.30      0.95      0.45        21

avg / total       0.96      0.89      0.92       438


EVALUATION FOR LABEL: FAMILY (BAMBINI + ADULTI)
             precision    recall  f1-score   support

          0       0.98      0.85      0.91       391
          1       0.40      0.83      0.54        47

avg / total       0.91      0.85      0.87       438


EVALUATION FOR LABEL: FEMMINILE
             precision    recall  f1-score   support

          0       0.97      0.83      0.89       396
          1       0.32      0.79      0.46        42

avg / total       0.91      0.82      0.85       438


EVALUATION FOR LABEL: GIOVANI ADULTI (20-40)
             precision    recall  f1-score   support

          0       0.35      0.68      0.46        71
          1       0.92      0.76      0.83       367

avg / total       0.83      0.75      0.77       438


EVALUATION FOR LABEL: MASCHILE
             precision    recall  f1-score   support

          0       0.88      0.72      0.79       309
          1       0.53      0.78      0.63       129

avg / total       0.78      0.73      0.74       438


EVALUATION FOR LABEL: TEENS (13-19)
             precision    recall  f1-score   support

          0       0.94      0.79      0.86       357
          1       0.46      0.78      0.58        81

avg / total       0.85      0.79      0.81       438


