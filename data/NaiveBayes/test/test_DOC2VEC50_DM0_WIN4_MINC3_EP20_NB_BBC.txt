EVALUATION FOR LABEL: ADRENALINICO/MOZZAFIATO
             precision    recall  f1-score   support

          0       0.92      0.77      0.84       321
          1       0.56      0.80      0.66       117

avg / total       0.82      0.78      0.79       438


EVALUATION FOR LABEL: AGGHIACCIANTE-TERRIFICANTE
             precision    recall  f1-score   support

          0       0.97      0.89      0.93       406
          1       0.34      0.69      0.45        32

avg / total       0.93      0.88      0.90       438


EVALUATION FOR LABEL: AGRODOLCE
             precision    recall  f1-score   support

          0       0.95      0.76      0.84       386
          1       0.28      0.69      0.40        52

avg / total       0.87      0.75      0.79       438


EVALUATION FOR LABEL: ARRAPANTE
             precision    recall  f1-score   support

          0       0.99      0.87      0.93       429
          1       0.08      0.56      0.14         9

avg / total       0.97      0.86      0.91       438


EVALUATION FOR LABEL: CEREBRALE
             precision    recall  f1-score   support

          0       0.98      0.77      0.86       422
          1       0.08      0.56      0.15        16

avg / total       0.95      0.76      0.84       438


EVALUATION FOR LABEL: CHE FA PENSARE/INSPIRING
             precision    recall  f1-score   support

          0       0.92      0.73      0.81       373
          1       0.29      0.63      0.40        65

avg / total       0.83      0.72      0.75       438


EVALUATION FOR LABEL: CHE FA SOGNARE
             precision    recall  f1-score   support

          0       0.98      0.87      0.92       417
          1       0.18      0.57      0.28        21

avg / total       0.94      0.86      0.89       438


EVALUATION FOR LABEL: COMMOVENTE/STRAPPALACRIME
             precision    recall  f1-score   support

          0       0.97      0.79      0.87       414
          1       0.15      0.62      0.24        24

avg / total       0.93      0.78      0.84       438


EVALUATION FOR LABEL: ESALTANTE/TRASCINANTE/EPICO
             precision    recall  f1-score   support

          0       0.96      0.83      0.89       392
          1       0.32      0.70      0.44        46

avg / total       0.89      0.82      0.84       438


EVALUATION FOR LABEL: FA MORIRE DAL RIDERE
             precision    recall  f1-score   support

          0       0.96      0.76      0.85       399
          1       0.22      0.69      0.34        39

avg / total       0.90      0.76      0.81       438


EVALUATION FOR LABEL: FEEL-GOOD
             precision    recall  f1-score   support

          0       0.96      0.71      0.82       390
          1       0.24      0.75      0.37        48

avg / total       0.88      0.72      0.77       438


EVALUATION FOR LABEL: INQUIETANTE/ANGOSCIANTE
             precision    recall  f1-score   support

          0       0.93      0.80      0.86       371
          1       0.38      0.67      0.48        67

avg / total       0.85      0.78      0.80       438


EVALUATION FOR LABEL: NOSTALGICO/MELANCONICO
             precision    recall  f1-score   support

          0       0.97      0.74      0.84       409
          1       0.15      0.66      0.25        29

avg / total       0.91      0.74      0.80       438


EVALUATION FOR LABEL: PROVOCA RABBIA/INDIGNAZIONE
             precision    recall  f1-score   support

          0       1.00      1.00      1.00       438

avg / total       1.00      1.00      1.00       438


EVALUATION FOR LABEL: RASSICURANTE
             precision    recall  f1-score   support

          0       1.00      0.88      0.93       433
          1       0.07      0.80      0.13         5

avg / total       0.99      0.87      0.92       438


EVALUATION FOR LABEL: ROMANTICO
             precision    recall  f1-score   support

          0       0.99      0.83      0.90       416
          1       0.20      0.82      0.32        22

avg / total       0.95      0.83      0.87       438


EVALUATION FOR LABEL: SCACCIAPENSIERI - EVASIVO
             precision    recall  f1-score   support

          0       0.93      0.71      0.81       357
          1       0.38      0.78      0.51        81

avg / total       0.83      0.72      0.75       438


EVALUATION FOR LABEL: SFAVILLANTE/STYLISH
             precision    recall  f1-score   support

          0       0.98      0.83      0.90       427
          1       0.07      0.45      0.11        11

avg / total       0.96      0.82      0.88       438


EVALUATION FOR LABEL: SORPRENDENTE
             precision    recall  f1-score   support

          0       0.95      0.77      0.85       408
          1       0.11      0.40      0.18        30

avg / total       0.89      0.75      0.80       438


EVALUATION FOR LABEL: SPIAZZANTE
             precision    recall  f1-score   support

          0       0.95      0.75      0.83       394
          1       0.21      0.61      0.32        44

avg / total       0.87      0.73      0.78       438


EVALUATION FOR LABEL: ADULTI (40-OLTRE)
             precision    recall  f1-score   support

          0       0.55      0.74      0.63       146
          1       0.84      0.70      0.76       292

avg / total       0.75      0.71      0.72       438


EVALUATION FOR LABEL: BAMBINI ANCHE SOLI (ET� PRE-SCOLARE)
             precision    recall  f1-score   support

          0       1.00      0.95      0.97       435
          1       0.08      0.67      0.15         3

avg / total       0.99      0.95      0.97       438


EVALUATION FOR LABEL: BAMBINI ANCHE SOLI (ET� SCOLARE)
             precision    recall  f1-score   support

          0       0.99      0.89      0.94       417
          1       0.30      0.90      0.45        21

avg / total       0.96      0.89      0.92       438


EVALUATION FOR LABEL: FAMILY (BAMBINI + ADULTI)
             precision    recall  f1-score   support

          0       0.98      0.84      0.91       391
          1       0.39      0.85      0.54        47

avg / total       0.92      0.84      0.87       438


EVALUATION FOR LABEL: FEMMINILE
             precision    recall  f1-score   support

          0       0.97      0.81      0.88       396
          1       0.30      0.76      0.43        42

avg / total       0.91      0.81      0.84       438


EVALUATION FOR LABEL: GIOVANI ADULTI (20-40)
             precision    recall  f1-score   support

          0       0.34      0.65      0.45        71
          1       0.92      0.76      0.83       367

avg / total       0.82      0.74      0.77       438


EVALUATION FOR LABEL: MASCHILE
             precision    recall  f1-score   support

          0       0.88      0.71      0.79       309
          1       0.53      0.78      0.63       129

avg / total       0.78      0.73      0.74       438


EVALUATION FOR LABEL: TEENS (13-19)
             precision    recall  f1-score   support

          0       0.93      0.75      0.83       357
          1       0.40      0.75      0.52        81

avg / total       0.83      0.75      0.77       438


