EVALUATION FOR LABEL: ADRENALINICO/MOZZAFIATO
             precision    recall  f1-score   support

          0       0.94      0.78      0.85       607
          1       0.53      0.83      0.65       181

avg / total       0.85      0.79      0.81       788


EVALUATION FOR LABEL: AGGHIACCIANTE-TERRIFICANTE
             precision    recall  f1-score   support

          0       0.99      0.85      0.92       747
          1       0.24      0.83      0.37        41

avg / total       0.95      0.85      0.89       788


EVALUATION FOR LABEL: AGRODOLCE
             precision    recall  f1-score   support

          0       0.93      0.69      0.79       686
          1       0.24      0.66      0.35       102

avg / total       0.84      0.69      0.74       788


EVALUATION FOR LABEL: ARRAPANTE
             precision    recall  f1-score   support

          0       0.99      0.83      0.90       774
          1       0.07      0.71      0.13        14

avg / total       0.98      0.82      0.89       788


EVALUATION FOR LABEL: CEREBRALE
             precision    recall  f1-score   support

          0       0.99      0.75      0.85       771
          1       0.04      0.53      0.08        17

avg / total       0.97      0.74      0.83       788


EVALUATION FOR LABEL: CHE FA PENSARE/INSPIRING
             precision    recall  f1-score   support

          0       0.91      0.68      0.78       657
          1       0.29      0.66      0.40       131

avg / total       0.81      0.68      0.71       788


EVALUATION FOR LABEL: CHE FA SOGNARE
             precision    recall  f1-score   support

          0       0.98      0.83      0.90       757
          1       0.12      0.58      0.20        31

avg / total       0.95      0.82      0.87       788


EVALUATION FOR LABEL: COMMOVENTE/STRAPPALACRIME
             precision    recall  f1-score   support

          0       0.95      0.76      0.84       727
          1       0.16      0.54      0.24        61

avg / total       0.89      0.74      0.80       788


EVALUATION FOR LABEL: ESALTANTE/TRASCINANTE/EPICO
             precision    recall  f1-score   support

          0       0.97      0.81      0.88       724
          1       0.24      0.70      0.36        64

avg / total       0.91      0.80      0.84       788


EVALUATION FOR LABEL: FA MORIRE DAL RIDERE
             precision    recall  f1-score   support

          0       0.94      0.73      0.82       698
          1       0.24      0.66      0.35        90

avg / total       0.86      0.72      0.77       788


EVALUATION FOR LABEL: FEEL-GOOD
             precision    recall  f1-score   support

          0       0.93      0.68      0.79       695
          1       0.21      0.62      0.31        93

avg / total       0.85      0.67      0.73       788


EVALUATION FOR LABEL: INQUIETANTE/ANGOSCIANTE
             precision    recall  f1-score   support

          0       0.95      0.78      0.86       676
          1       0.36      0.73      0.48       112

avg / total       0.86      0.77      0.80       788


EVALUATION FOR LABEL: NOSTALGICO/MELANCONICO
             precision    recall  f1-score   support

          0       0.97      0.71      0.82       746
          1       0.11      0.64      0.19        42

avg / total       0.93      0.71      0.79       788


EVALUATION FOR LABEL: PROVOCA RABBIA/INDIGNAZIONE
             precision    recall  f1-score   support

          0       1.00      1.00      1.00       788

avg / total       1.00      1.00      1.00       788


EVALUATION FOR LABEL: RASSICURANTE
             precision    recall  f1-score   support

          0       0.99      0.79      0.88       776
          1       0.02      0.25      0.03        12

avg / total       0.97      0.78      0.86       788


EVALUATION FOR LABEL: ROMANTICO
             precision    recall  f1-score   support

          0       0.98      0.77      0.86       729
          1       0.21      0.76      0.33        59

avg / total       0.92      0.77      0.82       788


EVALUATION FOR LABEL: SCACCIAPENSIERI - EVASIVO
             precision    recall  f1-score   support

          0       0.88      0.68      0.77       608
          1       0.39      0.69      0.50       180

avg / total       0.77      0.68      0.71       788


EVALUATION FOR LABEL: SFAVILLANTE/STYLISH
             precision    recall  f1-score   support

          0       0.99      0.79      0.88       772
          1       0.05      0.56      0.10        16

avg / total       0.97      0.79      0.86       788


EVALUATION FOR LABEL: SORPRENDENTE
             precision    recall  f1-score   support

          0       0.97      0.79      0.87       757
          1       0.07      0.39      0.12        31

avg / total       0.93      0.78      0.84       788


EVALUATION FOR LABEL: SPIAZZANTE
             precision    recall  f1-score   support

          0       0.96      0.74      0.83       734
          1       0.15      0.63      0.24        54

avg / total       0.91      0.73      0.79       788


EVALUATION FOR LABEL: ADULTI (40-OLTRE)
             precision    recall  f1-score   support

          0       0.57      0.66      0.61       275
          1       0.80      0.73      0.77       513

avg / total       0.72      0.71      0.71       788


EVALUATION FOR LABEL: BAMBINI ANCHE SOLI (ET� PRE-SCOLARE)
             precision    recall  f1-score   support

          0       1.00      0.87      0.93       775
          1       0.10      0.85      0.17        13

avg / total       0.98      0.87      0.92       788


EVALUATION FOR LABEL: BAMBINI ANCHE SOLI (ET� SCOLARE)
             precision    recall  f1-score   support

          0       0.99      0.88      0.93       755
          1       0.21      0.73      0.32        33

avg / total       0.95      0.87      0.90       788


EVALUATION FOR LABEL: FAMILY (BAMBINI + ADULTI)
             precision    recall  f1-score   support

          0       0.96      0.83      0.89       696
          1       0.37      0.76      0.50        92

avg / total       0.89      0.82      0.85       788


EVALUATION FOR LABEL: FEMMINILE
             precision    recall  f1-score   support

          0       0.97      0.76      0.85       681
          1       0.35      0.84      0.50       107

avg / total       0.88      0.77      0.80       788


EVALUATION FOR LABEL: GIOVANI ADULTI (20-40)
             precision    recall  f1-score   support

          0       0.36      0.66      0.47       149
          1       0.90      0.73      0.81       639

avg / total       0.80      0.72      0.74       788


EVALUATION FOR LABEL: MASCHILE
             precision    recall  f1-score   support

          0       0.88      0.74      0.80       581
          1       0.50      0.73      0.59       207

avg / total       0.78      0.73      0.75       788


EVALUATION FOR LABEL: TEENS (13-19)
             precision    recall  f1-score   support

          0       0.94      0.74      0.83       679
          1       0.30      0.69      0.42       109

avg / total       0.85      0.73      0.77       788


