EVALUATION FOR LABEL: ADRENALINICO/MOZZAFIATO
             precision    recall  f1-score   support

          0       0.92      0.76      0.83       321
          1       0.56      0.82      0.66       117

avg / total       0.82      0.78      0.79       438


EVALUATION FOR LABEL: AGGHIACCIANTE-TERRIFICANTE
             precision    recall  f1-score   support

          0       0.98      0.89      0.93       406
          1       0.35      0.75      0.48        32

avg / total       0.93      0.88      0.90       438


EVALUATION FOR LABEL: AGRODOLCE
             precision    recall  f1-score   support

          0       0.95      0.71      0.81       386
          1       0.25      0.71      0.37        52

avg / total       0.86      0.71      0.76       438


EVALUATION FOR LABEL: ARRAPANTE
             precision    recall  f1-score   support

          0       0.98      0.73      0.84       429
          1       0.03      0.44      0.06         9

avg / total       0.96      0.73      0.83       438


EVALUATION FOR LABEL: CEREBRALE
             precision    recall  f1-score   support

          0       0.98      0.72      0.83       422
          1       0.07      0.56      0.12        16

avg / total       0.94      0.71      0.80       438


EVALUATION FOR LABEL: CHE FA PENSARE/INSPIRING
             precision    recall  f1-score   support

          0       0.90      0.69      0.78       373
          1       0.25      0.58      0.35        65

avg / total       0.81      0.67      0.72       438


EVALUATION FOR LABEL: CHE FA SOGNARE
             precision    recall  f1-score   support

          0       0.98      0.87      0.92       417
          1       0.19      0.62      0.29        21

avg / total       0.94      0.86      0.89       438


EVALUATION FOR LABEL: COMMOVENTE/STRAPPALACRIME
             precision    recall  f1-score   support

          0       0.98      0.73      0.84       414
          1       0.15      0.79      0.25        24

avg / total       0.94      0.74      0.81       438


EVALUATION FOR LABEL: ESALTANTE/TRASCINANTE/EPICO
             precision    recall  f1-score   support

          0       0.96      0.79      0.87       392
          1       0.30      0.74      0.42        46

avg / total       0.89      0.79      0.82       438


EVALUATION FOR LABEL: FA MORIRE DAL RIDERE
             precision    recall  f1-score   support

          0       0.96      0.68      0.80       399
          1       0.19      0.74      0.30        39

avg / total       0.90      0.69      0.75       438


EVALUATION FOR LABEL: FEEL-GOOD
             precision    recall  f1-score   support

          0       0.95      0.66      0.78       390
          1       0.20      0.69      0.31        48

avg / total       0.86      0.66      0.73       438


EVALUATION FOR LABEL: INQUIETANTE/ANGOSCIANTE
             precision    recall  f1-score   support

          0       0.93      0.77      0.85       371
          1       0.36      0.70      0.47        67

avg / total       0.85      0.76      0.79       438


EVALUATION FOR LABEL: NOSTALGICO/MELANCONICO
             precision    recall  f1-score   support

          0       0.96      0.70      0.81       409
          1       0.12      0.59      0.20        29

avg / total       0.90      0.69      0.77       438


EVALUATION FOR LABEL: PROVOCA RABBIA/INDIGNAZIONE
             precision    recall  f1-score   support

          0       1.00      1.00      1.00       438

avg / total       1.00      1.00      1.00       438


EVALUATION FOR LABEL: RASSICURANTE
             precision    recall  f1-score   support

          0       0.99      0.79      0.88       433
          1       0.01      0.20      0.02         5

avg / total       0.98      0.78      0.87       438


EVALUATION FOR LABEL: ROMANTICO
             precision    recall  f1-score   support

          0       0.98      0.81      0.89       416
          1       0.17      0.73      0.27        22

avg / total       0.94      0.80      0.86       438


EVALUATION FOR LABEL: SCACCIAPENSIERI - EVASIVO
             precision    recall  f1-score   support

          0       0.92      0.69      0.79       357
          1       0.35      0.75      0.48        81

avg / total       0.82      0.70      0.73       438


EVALUATION FOR LABEL: SFAVILLANTE/STYLISH
             precision    recall  f1-score   support

          0       0.98      0.76      0.86       427
          1       0.05      0.45      0.09        11

avg / total       0.96      0.76      0.84       438


EVALUATION FOR LABEL: SORPRENDENTE
             precision    recall  f1-score   support

          0       0.93      0.65      0.77       408
          1       0.07      0.33      0.11        30

avg / total       0.87      0.63      0.72       438


EVALUATION FOR LABEL: SPIAZZANTE
             precision    recall  f1-score   support

          0       0.94      0.75      0.84       394
          1       0.21      0.59      0.31        44

avg / total       0.87      0.74      0.78       438


EVALUATION FOR LABEL: ADULTI (40-OLTRE)
             precision    recall  f1-score   support

          0       0.56      0.68      0.62       146
          1       0.82      0.73      0.77       292

avg / total       0.73      0.71      0.72       438


EVALUATION FOR LABEL: BAMBINI ANCHE SOLI (ET� PRE-SCOLARE)
             precision    recall  f1-score   support

          0       1.00      0.93      0.96       435
          1       0.06      0.67      0.11         3

avg / total       0.99      0.93      0.96       438


EVALUATION FOR LABEL: BAMBINI ANCHE SOLI (ET� SCOLARE)
             precision    recall  f1-score   support

          0       0.99      0.88      0.94       417
          1       0.28      0.90      0.43        21

avg / total       0.96      0.88      0.91       438


EVALUATION FOR LABEL: FAMILY (BAMBINI + ADULTI)
             precision    recall  f1-score   support

          0       0.97      0.85      0.90       391
          1       0.38      0.77      0.50        47

avg / total       0.90      0.84      0.86       438


EVALUATION FOR LABEL: FEMMINILE
             precision    recall  f1-score   support

          0       0.97      0.76      0.85       396
          1       0.26      0.79      0.39        42

avg / total       0.90      0.76      0.81       438


EVALUATION FOR LABEL: GIOVANI ADULTI (20-40)
             precision    recall  f1-score   support

          0       0.31      0.72      0.43        71
          1       0.93      0.68      0.79       367

avg / total       0.83      0.69      0.73       438


EVALUATION FOR LABEL: MASCHILE
             precision    recall  f1-score   support

          0       0.88      0.72      0.79       309
          1       0.53      0.75      0.62       129

avg / total       0.77      0.73      0.74       438


EVALUATION FOR LABEL: TEENS (13-19)
             precision    recall  f1-score   support

          0       0.92      0.75      0.83       357
          1       0.39      0.72      0.51        81

avg / total       0.82      0.74      0.77       438


