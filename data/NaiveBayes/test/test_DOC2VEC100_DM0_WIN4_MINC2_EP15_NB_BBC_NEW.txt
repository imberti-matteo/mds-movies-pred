EVALUATION FOR LABEL: ADRENALINICO/MOZZAFIATO
             precision    recall  f1-score   support

          0       0.95      0.81      0.88       438
          1       0.59      0.87      0.71       138

avg / total       0.87      0.83      0.84       576


EVALUATION FOR LABEL: AGGHIACCIANTE-TERRIFICANTE
             precision    recall  f1-score   support

          0       0.99      0.91      0.95       526
          1       0.49      0.88      0.63        50

avg / total       0.94      0.91      0.92       576


EVALUATION FOR LABEL: AGRODOLCE
             precision    recall  f1-score   support

          0       0.96      0.72      0.82       518
          1       0.22      0.72      0.34        58

avg / total       0.88      0.72      0.77       576


EVALUATION FOR LABEL: ARRAPANTE
             precision    recall  f1-score   support

          0       0.99      0.84      0.91       570
          1       0.03      0.50      0.06         6

avg / total       0.98      0.83      0.90       576


EVALUATION FOR LABEL: CEREBRALE
             precision    recall  f1-score   support

          0       0.99      0.84      0.91       557
          1       0.12      0.63      0.20        19

avg / total       0.96      0.83      0.88       576


EVALUATION FOR LABEL: CHE FA PENSARE/INSPIRING
             precision    recall  f1-score   support

          0       0.93      0.73      0.81       479
          1       0.34      0.71      0.46        97

avg / total       0.83      0.72      0.76       576


EVALUATION FOR LABEL: CHE FA SOGNARE
             precision    recall  f1-score   support

          0       0.99      0.90      0.94       559
          1       0.20      0.82      0.32        17

avg / total       0.97      0.90      0.93       576


EVALUATION FOR LABEL: COMMOVENTE/STRAPPALACRIME
             precision    recall  f1-score   support

          0       0.97      0.78      0.87       540
          1       0.18      0.69      0.28        36

avg / total       0.92      0.78      0.83       576


EVALUATION FOR LABEL: ESALTANTE/TRASCINANTE/EPICO
             precision    recall  f1-score   support

          0       0.97      0.82      0.89       521
          1       0.30      0.75      0.43        55

avg / total       0.90      0.81      0.84       576


EVALUATION FOR LABEL: FA MORIRE DAL RIDERE
             precision    recall  f1-score   support

          0       0.94      0.78      0.85       501
          1       0.32      0.69      0.44        75

avg / total       0.86      0.77      0.80       576


EVALUATION FOR LABEL: FEEL-GOOD
             precision    recall  f1-score   support

          0       0.96      0.73      0.83       507
          1       0.28      0.80      0.42        69

avg / total       0.88      0.73      0.78       576


EVALUATION FOR LABEL: INQUIETANTE/ANGOSCIANTE
             precision    recall  f1-score   support

          0       0.95      0.81      0.88       481
          1       0.46      0.80      0.58        95

avg / total       0.87      0.81      0.83       576


EVALUATION FOR LABEL: NOSTALGICO/MELANCONICO
             precision    recall  f1-score   support

          0       0.97      0.79      0.87       545
          1       0.13      0.55      0.21        31

avg / total       0.92      0.77      0.83       576


EVALUATION FOR LABEL: PROVOCA RABBIA/INDIGNAZIONE
             precision    recall  f1-score   support

          0       1.00      1.00      1.00       576

avg / total       1.00      1.00      1.00       576


EVALUATION FOR LABEL: RASSICURANTE
             precision    recall  f1-score   support

          0       0.99      0.84      0.91       566
          1       0.04      0.40      0.08        10

avg / total       0.97      0.83      0.89       576


EVALUATION FOR LABEL: ROMANTICO
             precision    recall  f1-score   support

          0       0.98      0.82      0.90       536
          1       0.26      0.82      0.40        40

avg / total       0.93      0.82      0.86       576


EVALUATION FOR LABEL: SCACCIAPENSIERI - EVASIVO
             precision    recall  f1-score   support

          0       0.94      0.73      0.82       461
          1       0.42      0.80      0.55       115

avg / total       0.83      0.74      0.77       576


EVALUATION FOR LABEL: SFAVILLANTE/STYLISH
             precision    recall  f1-score   support

          0       0.99      0.81      0.89       564
          1       0.06      0.58      0.11        12

avg / total       0.97      0.81      0.88       576


EVALUATION FOR LABEL: SORPRENDENTE
             precision    recall  f1-score   support

          0       0.97      0.74      0.84       555
          1       0.06      0.43      0.10        21

avg / total       0.94      0.72      0.81       576


EVALUATION FOR LABEL: SPIAZZANTE
             precision    recall  f1-score   support

          0       0.96      0.77      0.86       524
          1       0.22      0.63      0.33        52

avg / total       0.89      0.76      0.81       576


EVALUATION FOR LABEL: ADULTI (40-OLTRE)
             precision    recall  f1-score   support

          0       0.62      0.74      0.68       211
          1       0.83      0.74      0.78       365

avg / total       0.75      0.74      0.74       576


EVALUATION FOR LABEL: BAMBINI ANCHE SOLI (ET� PRE-SCOLARE)
             precision    recall  f1-score   support

          0       1.00      0.91      0.95       569
          1       0.11      0.86      0.19         7

avg / total       0.99      0.91      0.94       576


EVALUATION FOR LABEL: BAMBINI ANCHE SOLI (ET� SCOLARE)
             precision    recall  f1-score   support

          0       0.99      0.90      0.94       536
          1       0.38      0.88      0.53        40

avg / total       0.95      0.89      0.91       576


EVALUATION FOR LABEL: FAMILY (BAMBINI + ADULTI)
             precision    recall  f1-score   support

          0       0.96      0.86      0.91       505
          1       0.44      0.77      0.56        71

avg / total       0.90      0.85      0.87       576


EVALUATION FOR LABEL: FEMMINILE
             precision    recall  f1-score   support

          0       0.99      0.81      0.89       510
          1       0.38      0.91      0.54        66

avg / total       0.92      0.82      0.85       576


EVALUATION FOR LABEL: GIOVANI ADULTI (20-40)
             precision    recall  f1-score   support

          0       0.36      0.66      0.47       100
          1       0.91      0.75      0.83       476

avg / total       0.82      0.74      0.76       576


EVALUATION FOR LABEL: MASCHILE
             precision    recall  f1-score   support

          0       0.92      0.80      0.86       417
          1       0.61      0.81      0.70       159

avg / total       0.83      0.80      0.81       576


EVALUATION FOR LABEL: TEENS (13-19)
             precision    recall  f1-score   support

          0       0.90      0.71      0.80       476
          1       0.32      0.64      0.42       100

avg / total       0.80      0.70      0.73       576


