import pandas as pd
from gensim.models import Doc2Vec
import pickle as pk
from gensim.models.doc2vec import TaggedDocument
import string
import numpy as np
from nltk import word_tokenize
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.naive_bayes import GaussianNB
from imblearn.over_sampling import RandomOverSampler
from imblearn.under_sampling import RandomUnderSampler
from imblearn.combine import SMOTEENN
from imblearn.under_sampling import NearMiss
from imblearn.under_sampling import TomekLinks
from imblearn.under_sampling import EditedNearestNeighbours
from imblearn.under_sampling import RepeatedEditedNearestNeighbours
from imblearn.under_sampling import OneSidedSelection
from imblearn.under_sampling import InstanceHardnessThreshold
from imblearn.ensemble import BalancedBaggingClassifier
from sklearn import metrics
from sklearn.model_selection import GridSearchCV
from collections import Counter



df = pd.read_csv("./../new_extended_cleaned_dataset.csv")
df['TEXT'] = df['TEXT'].str.lower()
df['TEXT'] = df['TEXT'].apply(word_tokenize)

doc = []

for sequence ,movie_id in zip(df['TEXT'],df['ID'] ):
    s = TaggedDocument(words= sequence, tags= [movie_id])
    doc.append(s)

# for text, tag, trama in zip(df['synopsis'],df['imdb_id'],df['TRAMA_EN']):
#     t = str(text).lower().split()
#     if t == ["it"] or t == ['nan'] or len(t) == 0: # TODO fix in dataset it with whitespace
#         t = trama.lower().split()
#     s = TaggedDocument(words = t, tags = [tag])
#     doc.append(s)

model = Doc2Vec(doc, vector_size=100, window=4, min_count=2, workers=8, epochs=15, dbow_words = 1, dm=0) #dbow_words =1 => skip gram w2v
# model = Doc2Vec(doc ,  vector_size=300, window=8, min_count=0, workers=8, epochs=20)
model.train(doc, total_examples=model.corpus_count,epochs=model.epochs)

df_to_split = pd.read_csv("./../new_extended_cleaned_dataset.csv")
df_train, df_test = train_test_split(df_to_split,test_size=0.2,random_state=7)

labels = ['ADRENALINICO/MOZZAFIATO', 'AGGHIACCIANTE-TERRIFICANTE', 'AGRODOLCE', 'ARRAPANTE', 'CEREBRALE', 'CHE FA PENSARE/INSPIRING', 'CHE FA SOGNARE', 'COMMOVENTE/STRAPPALACRIME', 'ESALTANTE/TRASCINANTE/EPICO', 'FA MORIRE DAL RIDERE', 'FEEL-GOOD', 'INQUIETANTE/ANGOSCIANTE', 'NOSTALGICO/MELANCONICO', 'PROVOCA RABBIA/INDIGNAZIONE', 'RASSICURANTE', 'ROMANTICO', 'SCACCIAPENSIERI - EVASIVO', 'SFAVILLANTE/STYLISH', 'SORPRENDENTE', 'SPIAZZANTE', 'ADULTI (40-OLTRE)', 'BAMBINI ANCHE SOLI (ETÀ PRE-SCOLARE)', 'BAMBINI ANCHE SOLI (ETÀ SCOLARE)', 'FAMILY (BAMBINI + ADULTI)', 'FEMMINILE', 'GIOVANI ADULTI (20-40)', 'MASCHILE', 'TEENS (13-19)']

train_text = []
for id in df_train.loc[:,'ID'].tolist():
    train_text.append(model.docvecs[id])
test_text = []
for id in df_test.loc[:,'ID'].tolist():
    test_text.append(model.docvecs[id])

bbc = BalancedBaggingClassifier(base_estimator=GaussianNB(),ratio='auto',replacement=False,random_state=0)

out_file = open("./test/test_DOC2VEC100_DM0_WIN4_MINC2_EP15_NB_BBC_NEW_EXTENDED.txt","w")

for label in labels:
    train_label = df_train.loc[:, label].tolist()
    test_label = df_test.loc[:, label].tolist()
    try:
        bbc.fit(train_text,train_label)
        #train_input_resampled, train_label_resampled = rus.fit_sample(train_input,train_label)
    except ValueError:
        print("ValueError per label: ",label)
        #train_input_resampled = train_input
        #train_label_resampled = train_label
    #print(sorted(Counter(train_label_resampled).items()))
    #clf = MultinomialNB(alpha=0.1).fit(train_input_resampled, train_label_resampled)
    predictions = bbc.predict(test_text)
    names = ['0','1']
    print("EVALUATION FOR LABEL: \n",label,"\n")
    print(metrics.classification_report(test_label, predictions,target_names=names))
    out_file.write("EVALUATION FOR LABEL: ")
    out_file.write(label)
    out_file.write("\n")
    out_file.write(metrics.classification_report(test_label, predictions,target_names=names))
    out_file.write("\n\n")
out_file.close()



