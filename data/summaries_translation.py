import sinossi as sin
import pandas as pd
import random as ran
from googletrans import Translator

df = pd.read_csv("./new_extended_cleaned_dataset.csv")
translator = Translator()

for i in range(len(df)):
    lang = translator.detect(df.loc[i,'TEXT'])
    if lang.lang == 'it':
        print(df.loc[i,'ID']," ---> LUNGHEZZA: ",len(df.loc[i,'TEXT']),"\n")
        df.loc[i,'TEXT']= translator.translate(df.loc[i,'TEXT'], src='it',dest='en').text
df.to_csv('new_extended_cleaned_dataset.csv',index=False)