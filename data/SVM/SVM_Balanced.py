from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import SGDClassifier
from imblearn.over_sampling import RandomOverSampler
from imblearn.under_sampling import RandomUnderSampler
from imblearn.combine import SMOTEENN #molto meglio ENN
from imblearn.over_sampling import SMOTE, ADASYN #non convincono
from imblearn.under_sampling import NearMiss #fa schifo
from imblearn.under_sampling import TomekLinks #uguale a OSS
from imblearn.under_sampling import EditedNearestNeighbours #carino
from imblearn.under_sampling import RepeatedEditedNearestNeighbours #fa schifo
from imblearn.under_sampling import OneSidedSelection #carino
from imblearn.under_sampling import InstanceHardnessThreshold #non convince
from imblearn.ensemble import BalancedBaggingClassifier
from sklearn import metrics
from sklearn.model_selection import GridSearchCV
from collections import Counter

import pandas as pd
import numpy as np

df = pd.read_csv("./../new_extended_cleaned_dataset.csv")
df_train, df_test = train_test_split(df,test_size=0.2,random_state=7)

labels = ['ADRENALINICO/MOZZAFIATO', 'AGGHIACCIANTE-TERRIFICANTE', 'AGRODOLCE', 'ARRAPANTE', 'CEREBRALE', 'CHE FA PENSARE/INSPIRING', 'CHE FA SOGNARE', 'COMMOVENTE/STRAPPALACRIME', 'ESALTANTE/TRASCINANTE/EPICO', 'FA MORIRE DAL RIDERE', 'FEEL-GOOD', 'INQUIETANTE/ANGOSCIANTE', 'NOSTALGICO/MELANCONICO', 'PROVOCA RABBIA/INDIGNAZIONE', 'RASSICURANTE', 'ROMANTICO', 'SCACCIAPENSIERI - EVASIVO', 'SFAVILLANTE/STYLISH', 'SORPRENDENTE', 'SPIAZZANTE', 'ADULTI (40-OLTRE)', 'BAMBINI ANCHE SOLI (ETÀ PRE-SCOLARE)', 'BAMBINI ANCHE SOLI (ETÀ SCOLARE)', 'FAMILY (BAMBINI + ADULTI)', 'FEMMINILE', 'GIOVANI ADULTI (20-40)', 'MASCHILE', 'TEENS (13-19)']

train_text = df_train.loc[:,'TEXT'].str.lower().tolist()
test_text = df_test.loc[:,'TEXT'].str.lower().tolist()

vectorizer = CountVectorizer(min_df=0.01,max_df=0.4)
vectorizer.fit(train_text)
train_input = vectorizer.transform(train_text)
#rus = RandomUnderSampler(random_state=0)
#oss = OneSidedSelection(random_state=0)
bbc = BalancedBaggingClassifier(base_estimator=SGDClassifier(alpha=0.5, average=False, class_weight='balanced',
       eta0=0.0, fit_intercept=True, l1_ratio=0,
       learning_rate='optimal', loss='hinge', max_iter=50,
       n_jobs=1, penalty='l2', random_state=0,
       shuffle=True, tol=None, verbose=0),ratio='auto',replacement=False,random_state=0)
#ros = RandomOverSampler(random_state=0)

out_file = open("./test/test_BOW_SGDalpha0-5penaltyL2weightBALANCEDmax_iter50_BBC_Min_df0-01_Max_df0-4_NEW_EXTENDED.txt","w")

for label in labels:
    train_label = df_train.loc[:, label].values
    test_label = df_test.loc[:, label].values
    try:
        bbc.fit(train_input,train_label)
        #train_input_resampled, train_label_resampled = oss.fit_sample(train_input,train_label)
    except ValueError:
        print("ValueError per label: ",label)
        #train_input_resampled = train_input
        #train_label_resampled = train_label
    #print(sorted(Counter(train_label_resampled).items()))
    # clf = SGDClassifier(alpha=0.0001, average=False, class_weight=None, epsilon=0.1,
    #    eta0=0.0, fit_intercept=True, l1_ratio=0.15,
    #    learning_rate='optimal', loss='hinge', max_iter=None, n_iter=None,
    #    n_jobs=1, penalty='l2', power_t=0.5, random_state=None,
    #    shuffle=True, tol=None, verbose=0, warm_start=False).fit(train_input_resampled, train_label_resampled)
    test_input = vectorizer.transform(test_text)
    predictions = bbc.predict(test_input)
    names = ['0','1']
    print("EVALUATION FOR LABEL: \n",label,"\n")
    print(metrics.classification_report(test_label, predictions,target_names=names))
    out_file.write("EVALUATION FOR LABEL: ")
    out_file.write(label)
    out_file.write("\n")
    out_file.write(metrics.classification_report(test_label, predictions,target_names=names))
    out_file.write("\n\n")
out_file.close()