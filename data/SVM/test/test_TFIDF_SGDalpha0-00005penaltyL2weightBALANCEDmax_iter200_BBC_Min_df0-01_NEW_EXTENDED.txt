EVALUATION FOR LABEL: ADRENALINICO/MOZZAFIATO
             precision    recall  f1-score   support

          0       0.93      0.87      0.90       607
          1       0.64      0.79      0.71       181

avg / total       0.87      0.85      0.85       788


EVALUATION FOR LABEL: AGGHIACCIANTE-TERRIFICANTE
             precision    recall  f1-score   support

          0       0.98      0.92      0.95       747
          1       0.32      0.68      0.44        41

avg / total       0.95      0.91      0.92       788


EVALUATION FOR LABEL: AGRODOLCE
             precision    recall  f1-score   support

          0       0.91      0.79      0.84       686
          1       0.24      0.46      0.32       102

avg / total       0.82      0.74      0.78       788


EVALUATION FOR LABEL: ARRAPANTE
             precision    recall  f1-score   support

          0       0.99      0.91      0.95       774
          1       0.06      0.36      0.11        14

avg / total       0.97      0.90      0.93       788


EVALUATION FOR LABEL: CEREBRALE
             precision    recall  f1-score   support

          0       0.98      0.88      0.93       771
          1       0.05      0.29      0.09        17

avg / total       0.96      0.86      0.91       788


EVALUATION FOR LABEL: CHE FA PENSARE/INSPIRING
             precision    recall  f1-score   support

          0       0.89      0.80      0.84       657
          1       0.33      0.50      0.40       131

avg / total       0.80      0.75      0.77       788


EVALUATION FOR LABEL: CHE FA SOGNARE
             precision    recall  f1-score   support

          0       0.98      0.91      0.94       757
          1       0.17      0.45      0.25        31

avg / total       0.94      0.89      0.91       788


EVALUATION FOR LABEL: COMMOVENTE/STRAPPALACRIME
             precision    recall  f1-score   support

          0       0.95      0.81      0.87       727
          1       0.16      0.44      0.24        61

avg / total       0.88      0.78      0.82       788


EVALUATION FOR LABEL: ESALTANTE/TRASCINANTE/EPICO
             precision    recall  f1-score   support

          0       0.97      0.87      0.92       724
          1       0.33      0.70      0.45        64

avg / total       0.92      0.86      0.88       788


EVALUATION FOR LABEL: FA MORIRE DAL RIDERE
             precision    recall  f1-score   support

          0       0.93      0.84      0.88       698
          1       0.29      0.51      0.37        90

avg / total       0.86      0.80      0.83       788


EVALUATION FOR LABEL: FEEL-GOOD
             precision    recall  f1-score   support

          0       0.91      0.80      0.85       695
          1       0.23      0.44      0.30        93

avg / total       0.83      0.76      0.79       788


EVALUATION FOR LABEL: INQUIETANTE/ANGOSCIANTE
             precision    recall  f1-score   support

          0       0.92      0.86      0.89       676
          1       0.39      0.55      0.46       112

avg / total       0.84      0.81      0.83       788


EVALUATION FOR LABEL: NOSTALGICO/MELANCONICO
             precision    recall  f1-score   support

          0       0.96      0.81      0.88       746
          1       0.10      0.36      0.15        42

avg / total       0.91      0.79      0.84       788


EVALUATION FOR LABEL: PROVOCA RABBIA/INDIGNAZIONE
             precision    recall  f1-score   support

          0       1.00      1.00      1.00       788

avg / total       1.00      1.00      1.00       788


EVALUATION FOR LABEL: RASSICURANTE
             precision    recall  f1-score   support

          0       0.98      0.94      0.96       776
          1       0.00      0.00      0.00        12

avg / total       0.97      0.93      0.95       788


EVALUATION FOR LABEL: ROMANTICO
             precision    recall  f1-score   support

          0       0.97      0.85      0.91       729
          1       0.27      0.69      0.39        59

avg / total       0.92      0.84      0.87       788


EVALUATION FOR LABEL: SCACCIAPENSIERI - EVASIVO
             precision    recall  f1-score   support

          0       0.86      0.79      0.83       608
          1       0.45      0.57      0.50       180

avg / total       0.77      0.74      0.75       788


EVALUATION FOR LABEL: SFAVILLANTE/STYLISH
             precision    recall  f1-score   support

          0       0.99      0.92      0.95       772
          1       0.10      0.44      0.16        16

avg / total       0.97      0.91      0.94       788


EVALUATION FOR LABEL: SORPRENDENTE
             precision    recall  f1-score   support

          0       0.97      0.85      0.90       757
          1       0.07      0.26      0.11        31

avg / total       0.93      0.83      0.87       788


EVALUATION FOR LABEL: SPIAZZANTE
             precision    recall  f1-score   support

          0       0.95      0.85      0.89       734
          1       0.14      0.33      0.20        54

avg / total       0.89      0.81      0.85       788


EVALUATION FOR LABEL: ADULTI (40-OLTRE)
             precision    recall  f1-score   support

          0       0.56      0.65      0.60       275
          1       0.80      0.72      0.76       513

avg / total       0.71      0.70      0.70       788


EVALUATION FOR LABEL: BAMBINI ANCHE SOLI (ET� PRE-SCOLARE)
             precision    recall  f1-score   support

          0       0.99      0.95      0.97       775
          1       0.18      0.62      0.28        13

avg / total       0.98      0.95      0.96       788


EVALUATION FOR LABEL: BAMBINI ANCHE SOLI (ET� SCOLARE)
             precision    recall  f1-score   support

          0       0.99      0.93      0.96       755
          1       0.31      0.73      0.43        33

avg / total       0.96      0.92      0.94       788


EVALUATION FOR LABEL: FAMILY (BAMBINI + ADULTI)
             precision    recall  f1-score   support

          0       0.96      0.90      0.93       696
          1       0.49      0.72      0.58        92

avg / total       0.90      0.88      0.89       788


EVALUATION FOR LABEL: FEMMINILE
             precision    recall  f1-score   support

          0       0.96      0.86      0.91       681
          1       0.47      0.77      0.58       107

avg / total       0.89      0.85      0.86       788


EVALUATION FOR LABEL: GIOVANI ADULTI (20-40)
             precision    recall  f1-score   support

          0       0.35      0.62      0.44       149
          1       0.89      0.73      0.80       639

avg / total       0.79      0.71      0.73       788


EVALUATION FOR LABEL: MASCHILE
             precision    recall  f1-score   support

          0       0.86      0.80      0.83       581
          1       0.53      0.63      0.58       207

avg / total       0.77      0.76      0.76       788


EVALUATION FOR LABEL: TEENS (13-19)
             precision    recall  f1-score   support

          0       0.93      0.83      0.88       679
          1       0.37      0.61      0.46       109

avg / total       0.85      0.80      0.82       788


