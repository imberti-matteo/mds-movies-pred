EVALUATION FOR LABEL: ADRENALINICO/MOZZAFIATO
             precision    recall  f1-score   support

          0       0.94      0.51      0.66       607
          1       0.35      0.90      0.51       181

avg / total       0.81      0.60      0.63       788


EVALUATION FOR LABEL: AGGHIACCIANTE-TERRIFICANTE
             precision    recall  f1-score   support

          0       0.98      0.88      0.92       747
          1       0.22      0.61      0.32        41

avg / total       0.94      0.86      0.89       788


EVALUATION FOR LABEL: AGRODOLCE
             precision    recall  f1-score   support

          0       0.88      0.86      0.87       686
          1       0.20      0.24      0.21       102

avg / total       0.79      0.78      0.78       788


EVALUATION FOR LABEL: ARRAPANTE
             precision    recall  f1-score   support

          0       0.99      0.91      0.95       774
          1       0.05      0.29      0.09        14

avg / total       0.97      0.90      0.93       788


EVALUATION FOR LABEL: CEREBRALE
             precision    recall  f1-score   support

          0       0.98      0.82      0.89       771
          1       0.03      0.24      0.05        17

avg / total       0.96      0.81      0.88       788


EVALUATION FOR LABEL: CHE FA PENSARE/INSPIRING
             precision    recall  f1-score   support

          0       0.85      0.89      0.87       657
          1       0.26      0.18      0.21       131

avg / total       0.75      0.78      0.76       788


EVALUATION FOR LABEL: CHE FA SOGNARE
             precision    recall  f1-score   support

          0       0.98      0.88      0.92       757
          1       0.15      0.52      0.23        31

avg / total       0.95      0.86      0.90       788


EVALUATION FOR LABEL: COMMOVENTE/STRAPPALACRIME
             precision    recall  f1-score   support

          0       0.92      0.92      0.92       727
          1       0.09      0.10      0.10        61

avg / total       0.86      0.86      0.86       788


EVALUATION FOR LABEL: ESALTANTE/TRASCINANTE/EPICO
             precision    recall  f1-score   support

          0       0.99      0.39      0.56       724
          1       0.12      0.95      0.21        64

avg / total       0.92      0.43      0.53       788


EVALUATION FOR LABEL: FA MORIRE DAL RIDERE
             precision    recall  f1-score   support

          0       0.91      0.87      0.89       698
          1       0.24      0.33      0.28        90

avg / total       0.83      0.81      0.82       788


EVALUATION FOR LABEL: FEEL-GOOD
             precision    recall  f1-score   support

          0       0.90      0.73      0.81       695
          1       0.17      0.42      0.24        93

avg / total       0.82      0.69      0.74       788


EVALUATION FOR LABEL: INQUIETANTE/ANGOSCIANTE
             precision    recall  f1-score   support

          0       0.94      0.68      0.79       676
          1       0.28      0.76      0.41       112

avg / total       0.85      0.69      0.74       788


EVALUATION FOR LABEL: NOSTALGICO/MELANCONICO
             precision    recall  f1-score   support

          0       0.95      0.84      0.89       746
          1       0.07      0.21      0.11        42

avg / total       0.90      0.81      0.85       788


EVALUATION FOR LABEL: PROVOCA RABBIA/INDIGNAZIONE
             precision    recall  f1-score   support

          0       1.00      1.00      1.00       788

avg / total       1.00      1.00      1.00       788


EVALUATION FOR LABEL: RASSICURANTE
             precision    recall  f1-score   support

          0       0.99      0.51      0.67       776
          1       0.02      0.67      0.04        12

avg / total       0.98      0.51      0.66       788


EVALUATION FOR LABEL: ROMANTICO
             precision    recall  f1-score   support

          0       0.96      0.68      0.80       729
          1       0.14      0.66      0.24        59

avg / total       0.90      0.68      0.75       788


EVALUATION FOR LABEL: SCACCIAPENSIERI - EVASIVO
             precision    recall  f1-score   support

          0       0.81      0.81      0.81       608
          1       0.37      0.38      0.37       180

avg / total       0.71      0.71      0.71       788


EVALUATION FOR LABEL: SFAVILLANTE/STYLISH
             precision    recall  f1-score   support

          0       0.98      0.74      0.84       772
          1       0.03      0.44      0.06        16

avg / total       0.97      0.73      0.83       788


EVALUATION FOR LABEL: SORPRENDENTE
             precision    recall  f1-score   support

          0       0.96      0.89      0.92       757
          1       0.03      0.10      0.05        31

avg / total       0.92      0.86      0.89       788


EVALUATION FOR LABEL: SPIAZZANTE
             precision    recall  f1-score   support

          0       0.97      0.46      0.63       734
          1       0.10      0.78      0.17        54

avg / total       0.91      0.48      0.59       788


EVALUATION FOR LABEL: ADULTI (40-OLTRE)
             precision    recall  f1-score   support

          0       0.37      0.88      0.52       275
          1       0.76      0.20      0.32       513

avg / total       0.62      0.44      0.39       788


EVALUATION FOR LABEL: BAMBINI ANCHE SOLI (ET� PRE-SCOLARE)
             precision    recall  f1-score   support

          0       0.99      0.97      0.98       775
          1       0.18      0.46      0.26        13

avg / total       0.98      0.96      0.97       788


EVALUATION FOR LABEL: BAMBINI ANCHE SOLI (ET� SCOLARE)
             precision    recall  f1-score   support

          0       0.98      0.83      0.90       755
          1       0.14      0.61      0.22        33

avg / total       0.94      0.82      0.87       788


EVALUATION FOR LABEL: FAMILY (BAMBINI + ADULTI)
             precision    recall  f1-score   support

          0       0.94      0.73      0.82       696
          1       0.24      0.63      0.35        92

avg / total       0.86      0.72      0.77       788


EVALUATION FOR LABEL: FEMMINILE
             precision    recall  f1-score   support

          0       0.96      0.40      0.56       681
          1       0.19      0.90      0.31       107

avg / total       0.86      0.46      0.53       788


EVALUATION FOR LABEL: GIOVANI ADULTI (20-40)
             precision    recall  f1-score   support

          0       0.20      0.74      0.32       149
          1       0.84      0.31      0.45       639

avg / total       0.72      0.39      0.43       788


EVALUATION FOR LABEL: MASCHILE
             precision    recall  f1-score   support

          0       0.84      0.62      0.71       581
          1       0.38      0.66      0.49       207

avg / total       0.72      0.63      0.65       788


EVALUATION FOR LABEL: TEENS (13-19)
             precision    recall  f1-score   support

          0       0.88      0.96      0.92       679
          1       0.43      0.17      0.25       109

avg / total       0.82      0.85      0.83       788


