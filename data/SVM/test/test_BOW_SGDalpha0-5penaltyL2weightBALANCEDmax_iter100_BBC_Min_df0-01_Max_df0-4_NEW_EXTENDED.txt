EVALUATION FOR LABEL: ADRENALINICO/MOZZAFIATO
             precision    recall  f1-score   support

          0       0.92      0.87      0.90       607
          1       0.63      0.76      0.69       181

avg / total       0.86      0.84      0.85       788


EVALUATION FOR LABEL: AGGHIACCIANTE-TERRIFICANTE
             precision    recall  f1-score   support

          0       0.98      0.92      0.95       747
          1       0.30      0.66      0.42        41

avg / total       0.94      0.90      0.92       788


EVALUATION FOR LABEL: AGRODOLCE
             precision    recall  f1-score   support

          0       0.94      0.64      0.76       686
          1       0.23      0.73      0.35       102

avg / total       0.85      0.65      0.71       788


EVALUATION FOR LABEL: ARRAPANTE
             precision    recall  f1-score   support

          0       0.99      0.86      0.92       774
          1       0.07      0.57      0.12        14

avg / total       0.97      0.85      0.90       788


EVALUATION FOR LABEL: CEREBRALE
             precision    recall  f1-score   support

          0       0.98      0.82      0.90       771
          1       0.04      0.35      0.08        17

avg / total       0.96      0.81      0.88       788


EVALUATION FOR LABEL: CHE FA PENSARE/INSPIRING
             precision    recall  f1-score   support

          0       0.90      0.67      0.77       657
          1       0.28      0.64      0.39       131

avg / total       0.80      0.67      0.71       788


EVALUATION FOR LABEL: CHE FA SOGNARE
             precision    recall  f1-score   support

          0       0.98      0.94      0.96       757
          1       0.25      0.52      0.33        31

avg / total       0.95      0.92      0.93       788


EVALUATION FOR LABEL: COMMOVENTE/STRAPPALACRIME
             precision    recall  f1-score   support

          0       0.95      0.74      0.83       727
          1       0.15      0.57      0.24        61

avg / total       0.89      0.72      0.79       788


EVALUATION FOR LABEL: ESALTANTE/TRASCINANTE/EPICO
             precision    recall  f1-score   support

          0       0.96      0.88      0.92       724
          1       0.32      0.62      0.42        64

avg / total       0.91      0.86      0.88       788


EVALUATION FOR LABEL: FA MORIRE DAL RIDERE
             precision    recall  f1-score   support

          0       0.94      0.78      0.85       698
          1       0.27      0.64      0.38        90

avg / total       0.87      0.76      0.80       788


EVALUATION FOR LABEL: FEEL-GOOD
             precision    recall  f1-score   support

          0       0.94      0.75      0.83       695
          1       0.25      0.63      0.36        93

avg / total       0.86      0.73      0.78       788


EVALUATION FOR LABEL: INQUIETANTE/ANGOSCIANTE
             precision    recall  f1-score   support

          0       0.93      0.86      0.89       676
          1       0.40      0.58      0.48       112

avg / total       0.85      0.82      0.83       788


EVALUATION FOR LABEL: NOSTALGICO/MELANCONICO
             precision    recall  f1-score   support

          0       0.97      0.70      0.81       746
          1       0.10      0.62      0.18        42

avg / total       0.92      0.70      0.78       788


EVALUATION FOR LABEL: PROVOCA RABBIA/INDIGNAZIONE
             precision    recall  f1-score   support

          0       1.00      1.00      1.00       788

avg / total       1.00      1.00      1.00       788


EVALUATION FOR LABEL: RASSICURANTE
             precision    recall  f1-score   support

          0       0.98      0.85      0.91       776
          1       0.01      0.08      0.02        12

avg / total       0.97      0.84      0.90       788


EVALUATION FOR LABEL: ROMANTICO
             precision    recall  f1-score   support

          0       0.97      0.84      0.90       729
          1       0.26      0.69      0.37        59

avg / total       0.92      0.83      0.86       788


EVALUATION FOR LABEL: SCACCIAPENSIERI - EVASIVO
             precision    recall  f1-score   support

          0       0.90      0.70      0.79       608
          1       0.42      0.74      0.54       180

avg / total       0.79      0.71      0.73       788


EVALUATION FOR LABEL: SFAVILLANTE/STYLISH
             precision    recall  f1-score   support

          0       0.99      0.87      0.93       772
          1       0.09      0.62      0.16        16

avg / total       0.97      0.87      0.91       788


EVALUATION FOR LABEL: SORPRENDENTE
             precision    recall  f1-score   support

          0       0.97      0.78      0.86       757
          1       0.06      0.35      0.10        31

avg / total       0.93      0.76      0.83       788


EVALUATION FOR LABEL: SPIAZZANTE
             precision    recall  f1-score   support

          0       0.95      0.83      0.89       734
          1       0.15      0.41      0.22        54

avg / total       0.90      0.80      0.84       788


EVALUATION FOR LABEL: ADULTI (40-OLTRE)
             precision    recall  f1-score   support

          0       0.59      0.61      0.60       275
          1       0.79      0.78      0.78       513

avg / total       0.72      0.72      0.72       788


EVALUATION FOR LABEL: BAMBINI ANCHE SOLI (ET� PRE-SCOLARE)
             precision    recall  f1-score   support

          0       0.99      0.93      0.96       775
          1       0.15      0.69      0.24        13

avg / total       0.98      0.93      0.95       788


EVALUATION FOR LABEL: BAMBINI ANCHE SOLI (ET� SCOLARE)
             precision    recall  f1-score   support

          0       0.99      0.91      0.95       755
          1       0.30      0.85      0.44        33

avg / total       0.96      0.91      0.93       788


EVALUATION FOR LABEL: FAMILY (BAMBINI + ADULTI)
             precision    recall  f1-score   support

          0       0.96      0.90      0.93       696
          1       0.49      0.73      0.58        92

avg / total       0.91      0.88      0.89       788


EVALUATION FOR LABEL: FEMMINILE
             precision    recall  f1-score   support

          0       0.97      0.80      0.88       681
          1       0.40      0.85      0.54       107

avg / total       0.89      0.80      0.83       788


EVALUATION FOR LABEL: GIOVANI ADULTI (20-40)
             precision    recall  f1-score   support

          0       0.37      0.68      0.48       149
          1       0.91      0.73      0.81       639

avg / total       0.80      0.72      0.74       788


EVALUATION FOR LABEL: MASCHILE
             precision    recall  f1-score   support

          0       0.87      0.82      0.84       581
          1       0.57      0.66      0.61       207

avg / total       0.79      0.78      0.78       788


EVALUATION FOR LABEL: TEENS (13-19)
             precision    recall  f1-score   support

          0       0.93      0.86      0.89       679
          1       0.41      0.62      0.49       109

avg / total       0.86      0.82      0.84       788


