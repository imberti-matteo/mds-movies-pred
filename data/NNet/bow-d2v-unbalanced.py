import numpy as np
import nltk
import pandas as pd
from imblearn.ensemble import BalancedBaggingClassifier
from keras.wrappers.scikit_learn import KerasClassifier
from nltk.stem.porter import PorterStemmer
from sklearn import metrics
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.model_selection import train_test_split
import tensorflow as tf
import keras.backend as K
import pickle as pk
from pathlib import Path

from data.NNet.nnmodel import nn_model
from nltk import word_tokenize
from gensim.models.doc2vec import TaggedDocument, Doc2Vec

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)


def tokenize(text):
    tokens = nltk.word_tokenize(text)
    stems = []
    for item in tokens:
        stems.append(PorterStemmer().stem(item))
    return stems


print("load data")
df_name = "new_extended_cleaned_dataset_clusters"
model_name = df_name
df_base = df = pd.read_csv("./../" + df_name + ".csv")
labels = df_base.keys()[2:]
df_train, df_test = train_test_split(df_base, test_size=0.2, random_state=7)

df['TEXT'] = df['TEXT'].str.lower()

#
train_text = df_train.loc[:, 'TEXT'].str.lower().tolist()
test_text = df_test.loc[:, 'TEXT'].str.lower().tolist()

print("doc2vec")
v_size = 100
path = "./d2v" + model_name + str(v_size) + ".mod"
if Path(path).exists():
    f = open(path, "rb")
    model = pk.load(f)
    f.close()
else:
    print("tokenize")
    df['TEXT'] = df['TEXT'].apply(word_tokenize)

    doc = []

    for sequence, movie_id in zip(df['TEXT'], df['ID']):
        s = TaggedDocument(words=sequence, tags=[movie_id])
        doc.append(s)

    model = Doc2Vec(doc, vector_size=v_size, window=4, min_count=2, workers=8, epochs=15, dbow_words=1, dm=0)
    model.train(doc, total_examples=model.corpus_count, epochs=model.epochs)
    with open(path, "wb") as f:
        pk.dump(model, f)

epochs = 100
batch_size = 512

train_text = []
for id in df_train.loc[:, 'ID'].tolist():
    train_text.append(model.docvecs[id])
test_text = []
for id in df_test.loc[:, 'ID'].tolist():
    test_text.append(model.docvecs[id])

train_text = np.array(train_text)
test_text = np.array(test_text)
print("start")
out_file = open("./test/test_" + "d2v" + "_256_32_NNET_BBC" + df_name + "_epochs" + str(epochs) + "_v_" + str(v_size),
                "a+")
for label in labels:
    print(label, "\n--------------------------")

    if label == 'PROVOCA RABBIA/INDIGNAZIONE':
        continue
    train_label = df_train.loc[:, label].tolist()
    test_label = df_test.loc[:, label].tolist()
    nnm = nn_model(input_dim=train_text.shape[1])
    base_estimator = KerasClassifier(build_fn=nnm.build_model, epochs=epochs, batch_size=batch_size,
                                     verbose=2)
    bbc = BalancedBaggingClassifier(base_estimator=base_estimator, ratio='auto',
                                    replacement=False,
                                    random_state=0)

    bbc.fit(train_text, train_label)
    predictions = bbc.predict(test_text)
    names = ['0', '1']
    print("EVALUATION FOR LABEL: \n", label, "\n")
    print(metrics.classification_report(test_label, predictions, target_names=names))
    out_file.write("EVALUATION FOR LABEL: ")
    out_file.write(label)
    out_file.write("\n")
    out_file.write(metrics.classification_report(test_label, predictions, target_names=names))
    out_file.write("\n\n")
    K.clear_session()
out_file.close()
