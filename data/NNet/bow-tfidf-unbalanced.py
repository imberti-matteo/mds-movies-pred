import nltk
import pandas as pd
from imblearn.ensemble import BalancedBaggingClassifier
from keras.wrappers.scikit_learn import KerasClassifier
from nltk.stem.porter import PorterStemmer
from sklearn import metrics
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.model_selection import train_test_split
import tensorflow as tf
import keras.backend as K

from data.NNet.nnmodel import nn_model

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)


def tokenize(text):
    tokens = nltk.word_tokenize(text)
    stems = []
    for item in tokens:
        stems.append(PorterStemmer().stem(item))
    return stems


df_name = "new_cleaned_dataset"
df_base = pd.read_csv("./../" + df_name + ".csv")
targets = df_base.keys()[2:]
df_train, df_test = train_test_split(df_base, test_size=0.2, random_state=7)

train_text = df_train.loc[:, 'TEXT'].str.lower().tolist()
test_text = df_test.loc[:, 'TEXT'].str.lower().tolist()

models = []
print("tfidf")

tfidf = TfidfVectorizer(tokenizer=tokenize, stop_words='english')
X_train_tfidf = tfidf.fit_transform(train_text).todense()
models.append({'name': 'tfidf', 'x_train': X_train_tfidf, 'vectorizer': tfidf})
print("bow")
# count_vect = CountVectorizer(min_df=2)
# X_train_count = count_vect.fit_transform(train_text).todense()
# models.append({'name': 'bow', 'x_train': X_train_count, 'vectorizer': count_vect})

epochs = 10
batch_size = 512

for i, m in enumerate(models):
    name = m['name']
    out_file = open("./test/test_" + name + "_256_32_NNET_BBC" + df_name + "_epochs" + str(epochs), "a+")
    for target in targets:
        print(target, "\n--------------------------")

        if target == 'PROVOCA RABBIA/INDIGNAZIONE':
            continue
        train_label = df_train.loc[:, target].values
        test_label = df_test.loc[:, target].values
        nnm = nn_model(input_dim=m['x_train'].shape[1])
        base_estimator = KerasClassifier(build_fn=nnm.build_model, epochs=epochs, batch_size=batch_size,
                                         verbose=2)
        bbc = BalancedBaggingClassifier(base_estimator=base_estimator, ratio='auto',
                                        replacement=False,
                                        random_state=0)

        bbc.fit(m['x_train'], train_label)
        test_input = m['vectorizer'].transform(test_text).todense()
        predictions = bbc.predict(test_input)
        names = ['0', '1']
        print("EVALUATION FOR LABEL: \n", target, "\n")
        print(metrics.classification_report(test_label, predictions, target_names=names))
        out_file.write("EVALUATION FOR LABEL: ")
        out_file.write(target)
        out_file.write("\n")
        out_file.write(metrics.classification_report(test_label, predictions, target_names=names))
        out_file.write("\n\n")
        K.clear_session()
    out_file.close()
