import numpy as np
import pickle as pk
import pandas as pd
from sklearn.preprocessing import LabelBinarizer

path = "./new_extended_cleaned_dataset"
out_path = path + "_clusters"

df = pd.read_csv(path + ".csv")
mood_labels = df.keys()[2:-8]
mood_labels = mood_labels.drop("PROVOCA RABBIA/INDIGNAZIONE")

with open("./mood2vec/mod2vec_cluster_tuple.dat", "rb") as f:
    m2v, m2c = pk.load(f)

clusters = []
for c in np.unique(list(m2c.values())):
    cluster = str(c)
    df[c] = 0
    clusters.append(c)

for index, row in df.loc[:, mood_labels].iterrows():
    for label in mood_labels:
        if row[label] == 1:
            cluster = m2c[label]
            df.loc[index, cluster] = 1

final_keys = list(df.keys()[:2]) + clusters
new_df = df.loc[:, final_keys]

new_df.to_csv(out_path + ".csv", index=False)
