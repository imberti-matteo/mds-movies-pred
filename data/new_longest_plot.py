import pandas as pd

df = pd.read_csv("summary_sinossi-imdb_trama-wikien_trama-wikiit.csv")
df.fillna("NA",inplace = True)
df.replace(to_replace="True", value="NA",inplace = True)
df.drop([df.keys()[0]], axis = 1,inplace = True)
df.loc[df['IMDB'].str.len() > df['WIKI'].str.len() , "TEXT"] = df['IMDB']
df.loc[df['IMDB'].str.len() < df['WIKI'].str.len() , "TEXT"] = df['WIKI']
df.fillna("NA", inplace = True)
out = df[df['TEXT'] != "NA"][['ID', 'TEXT']]
out.to_csv("new_extended_dataset_più_lunghi.csv")