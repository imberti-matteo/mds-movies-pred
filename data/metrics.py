import numpy as np

def at_least_one_metric(y_pred, y_true): # Quanto spesso azzecco almeno uno dei mood giusti?
    y_pred = y_pred.astype(int)
    r = np.bitwise_and(y_pred, y_true).sum(axis=1)
    r[r >= 1] = 1
    return np.mean(r)

def exact_match_metric(y_pred, y_true): # Quanto spesso azzecco alla perfezione i mood giusti?
    y_pred = y_pred.astype(int)
    res = np.bitwise_and(y_pred, y_true).sum(axis=1) / y_true.sum(axis=1) + np.bitwise_and(y_pred, y_true).sum(axis=1) / y_pred.sum(axis=1)
    res = res / 2
    for number in range(len(res)):
        if np.isnan(res[number]):
            res[number] = 0
    res[res < 1] = 0
    return np.mean(res)

def diminuited_f1_metric(y_pred, y_true): # (TP) / (TP + FP + FN) ---> Di tutti gli '1' che hai predetto e che dovevi predire, quanti ne hai azzeccati? (0 ; 0.33 ; 0.5 ; 1)
    y_pred = y_pred.astype(int)
    res = np.bitwise_and(y_pred, y_true).sum(axis=1) / np.bitwise_or(y_pred, y_true).sum(axis=1)
    return np.mean(res)

def precision_metric(y_pred, y_true): # (TP) / (TP + FP) ---> Di tutti gli '1' che hai predetto, quanti ne hai azzeccati? (0 ; 0.5 ; 1)
    y_pred = y_pred.astype(int)
    res = np.bitwise_and(y_pred, y_true).sum(axis=1) / y_pred.sum(axis=1)
    for number in range(len(res)):
        if np.isnan(res[number]):
            res[number] = 0
    return np.mean(res)

def recall_metric(y_pred, y_true): # (TP) / (TP + FN) ---> Di tutti gli '1' che dovevi predire, quanti ne hai azzeccati? (0 ; 0.5 ; 1)
    y_pred = y_pred.astype(int)
    res = np.bitwise_and(y_pred, y_true).sum(axis=1) / y_true.sum(axis=1)
    return np.mean(res)