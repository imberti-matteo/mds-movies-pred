import pandas as pd
import numpy as np
import operator

moods = ['SCACCIAPENSIERI - EVASIVO', 'CHE FA PENSARE/INSPIRING', 'ADRENALINICO/MOZZAFIATO', 'FEEL-GOOD', 'INQUIETANTE/ANGOSCIANTE', 'AGRODOLCE', 'FA MORIRE DAL RIDERE', 'COMMOVENTE/STRAPPALACRIME', 'ROMANTICO', 'SPIAZZANTE', 'ESALTANTE/TRASCINANTE/EPICO', 'NOSTALGICO/MELANCONICO', 'RASSICURANTE', 'SORPRENDENTE', 'AGGHIACCIANTE-TERRIFICANTE', 'CHE FA SOGNARE', 'CEREBRALE', 'SFAVILLANTE/STYLISH', 'PROVOCA RABBIA/INDIGNAZIONE', 'ARRAPANTE']
targets = ['GIOVANI ADULTI (20-40)', 'ADULTI (40-OLTRE)', 'FAMILY (BAMBINI + ADULTI)', 'MASCHILE', 'TEENS (13-19)', 'FEMMINILE', 'BAMBINI ANCHE SOLI (ETÀ SCOLARE)', 'BAMBINI ANCHE SOLI (ETÀ PRE-SCOLARE)']
moods_number = 20
targets_number = 8

def get_binary_labels(moods_to_convert):
    splitted_moods = moods_to_convert.upper().split(",")
    binary_labels = {}
    for m in moods:
        if m in splitted_moods:
            binary_labels[m] = 1
        else:
            binary_labels[m] = 0
    binary_labels = sorted(binary_labels.items(), key=operator.itemgetter(0))
    result_labels = []
    for i in range(moods_number):
        result_labels.append(binary_labels[i][1])
    return result_labels

def get_binary_targets(targets_to_convert):
    splitted_targets =targets_to_convert.upper().split(",")
    binary_labels = {}
    for m in targets:
        if m in splitted_targets:
            binary_labels[m] = 1
        else:
            binary_labels[m] = 0
    binary_labels = sorted(binary_labels.items(), key=operator.itemgetter(0))
    result_labels = []
    for i in range(targets_number):
        result_labels.append(binary_labels[i][1])
    return result_labels

df = pd.read_csv("../dataset.csv")
df_trame = pd.read_csv("dataset_più lunghi.csv")
#df_trame.set_index('Unnamed: 0', inplace=True)
#df_trame.reset_index(drop=True, inplace=True)

df = df.loc[df['TIPO'] == 'FILM']
df.reset_index(drop=True, inplace=True)
df_label = pd.DataFrame([],columns=['ID', 'ADRENALINICO/MOZZAFIATO', 'AGGHIACCIANTE-TERRIFICANTE', 'AGRODOLCE', 'ARRAPANTE', 'CEREBRALE', 'CHE FA PENSARE/INSPIRING', 'CHE FA SOGNARE', 'COMMOVENTE/STRAPPALACRIME', 'ESALTANTE/TRASCINANTE/EPICO', 'FA MORIRE DAL RIDERE', 'FEEL-GOOD', 'INQUIETANTE/ANGOSCIANTE', 'NOSTALGICO/MELANCONICO', 'PROVOCA RABBIA/INDIGNAZIONE', 'RASSICURANTE', 'ROMANTICO', 'SCACCIAPENSIERI - EVASIVO', 'SFAVILLANTE/STYLISH', 'SORPRENDENTE', 'SPIAZZANTE', 'ADULTI (40-OLTRE)', 'BAMBINI ANCHE SOLI (ETÀ PRE-SCOLARE)', 'BAMBINI ANCHE SOLI (ETÀ SCOLARE)', 'FAMILY (BAMBINI + ADULTI)', 'FEMMINILE', 'GIOVANI ADULTI (20-40)', 'MASCHILE', 'TEENS (13-19)'])


for m in range(df.shape[0]):
    moovie_id = df.loc[m,'ID']
    moovie_moods = get_binary_labels(df.loc[m,'MOOD'])
    moovie_targets = get_binary_targets(df.loc[m,'TARGET'])
    df_label = pd.concat([df_label, pd.DataFrame([(moovie_id, moovie_moods[0], moovie_moods[1],
                                                     moovie_moods[2], moovie_moods[3], moovie_moods[4],
                                                     moovie_moods[5], moovie_moods[6], moovie_moods[7],
                                                     moovie_moods[8], moovie_moods[9], moovie_moods[10],
                                                     moovie_moods[11], moovie_moods[12], moovie_moods[13],
                                                     moovie_moods[14], moovie_moods[15], moovie_moods[16],
                                                     moovie_moods[17], moovie_moods[18], moovie_moods[19],
                                                     moovie_targets[0], moovie_targets[1],
                                                     moovie_targets[2], moovie_targets[3], moovie_targets[4],
                                                     moovie_targets[5], moovie_targets[6], moovie_targets[7])],
                                                     columns=['ID', 'ADRENALINICO/MOZZAFIATO', 'AGGHIACCIANTE-TERRIFICANTE', 'AGRODOLCE', 'ARRAPANTE', 'CEREBRALE', 'CHE FA PENSARE/INSPIRING', 'CHE FA SOGNARE', 'COMMOVENTE/STRAPPALACRIME', 'ESALTANTE/TRASCINANTE/EPICO', 'FA MORIRE DAL RIDERE', 'FEEL-GOOD', 'INQUIETANTE/ANGOSCIANTE', 'NOSTALGICO/MELANCONICO', 'PROVOCA RABBIA/INDIGNAZIONE', 'RASSICURANTE', 'ROMANTICO', 'SCACCIAPENSIERI - EVASIVO', 'SFAVILLANTE/STYLISH', 'SORPRENDENTE', 'SPIAZZANTE', 'ADULTI (40-OLTRE)', 'BAMBINI ANCHE SOLI (ETÀ PRE-SCOLARE)', 'BAMBINI ANCHE SOLI (ETÀ SCOLARE)', 'FAMILY (BAMBINI + ADULTI)', 'FEMMINILE', 'GIOVANI ADULTI (20-40)', 'MASCHILE', 'TEENS (13-19)'])], ignore_index=True)

df_label.to_csv('label_binarizzate.csv', index=False)



