import pandas as pd
import sinossi as sin

df_path = "../updated_dataset_2018.csv"
out_path = "summary_sinossi-imdb_trama-wikien_trama-wikiit.csv"

df = pd.read_csv(df_path,sep="|")
df = df[df['TIPO'] == 'MOVIE']
df.reset_index(inplace=True)

with open("index.txt", "r") as f:
    index = int(f.read())
if index == 0:
    ds = pd.DataFrame(columns=['ID', 'IMDB', 'WIKI'])
    ds.to_csv(out_path)

df = df.loc[index:]

for movie_id, imdb_link, wiki_link in zip(df['ID'], df['LINK_IMDB'], df['LINK_WIKI']):
    print(wiki_link)
    sinossi = sin.get_sinossi_imdb(imdb_link)
    wiki_link_en = sin.get_wiki_link_en(wiki_link)
    trama_en = sin.get_wiki_trama(wiki_link_en)
    trama_it_translated = sin.get_wiki_trama_translated(wiki_link)
    try:
        trama_it_translated_len = len(trama_it_translated)
    except TypeError:
        trama_it_translated_len = 0
    try:
        trama_en_len = len(trama_en)
    except TypeError:
        trama_en_len = 0

    if trama_it_translated_len > 1.75 * trama_en_len:
        trama = trama_it_translated
        print("Prendo la Trama tradotta", "\n\n")
    else:
        trama = trama_en
        print("Prendo la Trama in Inglese", "\n\n")


    out = pd.DataFrame([[movie_id, sinossi, trama]], columns=['ID', 'IMDB', 'WIKI'])
    out.to_csv(out_path, header=False, mode='a')
    index += 1
    with open("index.txt", "w") as f:
        f.write(str(index))
