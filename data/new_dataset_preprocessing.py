import pandas as pd
import numpy as np
import re
from googletrans import Translator


df_trame = pd.read_csv("new_extended_dataset_più_lunghi.csv")
df_trame.set_index('Unnamed: 0', inplace=True)
df_trame.reset_index(drop=True, inplace=True)

df_label = pd.read_csv("new_label_binarizzate.csv")

df = pd.merge(df_trame,df_label, on="ID")

translator = Translator()
#pulizia dei caratteri anomali nel testo
for i in range(len(df.loc[:,'TEXT'])):
    df.loc[i,'TEXT'] = df.loc[i,'TEXT'].replace('\r','').replace('\n','')
    if('&#' in df.loc[i,'TEXT']):
        df.loc[i, 'TEXT'] = re.sub('[0-9]?&#[0-9]{5};','',df.loc[i,'TEXT'])
    if len(df.loc[i,'TEXT']) < 75:
        print("REMOVING ",df.loc[i,'ID']," ",i," ",df.loc[i,'TEXT'])
        df.drop(i,inplace=True)
    elif (translator.detect(df.loc[i,'TEXT']).lang == 'it'):
        print("TRADUCING ",df.loc[i,'ID'])
        df.loc[i,'TEXT'] = translator.translate(df.loc[i,'TEXT'], src='it',dest='en').text
df.reset_index(drop=True, inplace=True)

df.to_csv('new_extended_cleaned_dataset.csv',index=False)



