import numpy as np
import re
import gensim
import itertools
from collections import Counter
import pandas as pd


def clean_str(string):
    """
    Tokenization/string cleaning for datasets.
    Original taken from https://github.com/yoonkim/CNN_sentence/blob/master/process_data.py
    """
    string = re.sub(r"\'", "\'", string)
    string = re.sub(r",", " ", string)
    string = re.sub(r"\.", " ", string)
    string = re.sub(r"!", " ", string)
    string = re.sub(r";", " ", string)
    string = re.sub(r"\(", "", string)
    string = re.sub(r"\)", "", string)
    string = re.sub(r"\?", " ", string)
    string = re.sub(r":", " ", string)
    string = re.sub(r"\"", " ", string)
    return string.strip().lower()


def load_data_and_labels():
    """
    Loads plots data from files, splits the data into words and generates labels.
    Returns split sentences and labels.
    """
    df = pd.read_csv("./../new_extended_cleaned_dataset.csv")
    x_text = list(df.loc[:,'TEXT'])
    x_text = [clean_str(plot) for plot in x_text]
    x_text = [s.split(" ") for s in x_text]

    label_keys = df.keys()[2:-8].drop("PROVOCA RABBIA/INDIGNAZIONE")
    labels = df.loc[:,label_keys]
    y = np.array(labels.as_matrix())

    return [x_text, y]


def pad_sentences(sentences, padding_word="<PAD/>"):
    """
    Pads all sentences to the same length. The length is defined by the longest sentence.
    Returns padded sentences.
    """
    sequence_length = 8124 #max(len(x) for x in sentences)
    padded_sentences = []
    removed_indexes = []
    for i in range(len(sentences)):
        sentence = sentences[i]
        if len(sentence) > sequence_length:
            removed_indexes.append(i)
            continue
        num_padding = sequence_length - len(sentence)
        new_sentence = sentence + [padding_word] * num_padding
        padded_sentences.append(new_sentence)
    return padded_sentences, removed_indexes


def build_vocab(sentences):
    """
    Builds a vocabulary mapping from word to index based on the sentences.
    Returns vocabulary mapping and inverse vocabulary mapping.
    """
    # Build vocabulary
    word_counts = Counter(itertools.chain(*sentences))
    # Mapping from index to word
    vocabulary_inv = [x[0] for x in word_counts.most_common(18420)]
    vocabulary_inv = list(sorted(vocabulary_inv))
    vocabulary_inv.pop(0)
    vocabulary_inv.append("UNKNOWN")
    # Mapping from word to index
    vocabulary = {x: i for i, x in enumerate(vocabulary_inv)}
    return [vocabulary, vocabulary_inv]


def build_input_data(sentences, removed_ind, labels, vocabulary):
    """
    Maps sentences and labels to vectors based on a vocabulary.
    """
    #x = np.array([[vocabulary[word] for word in sentence] for sentence in sentences])
    sentences_vector = []
    for index,sentence in enumerate(sentences):
        vocabulary_vector = []
        for word in sentence:
            try:
                vocabulary_vector.append(vocabulary[word])
            except KeyError:
                vocabulary_vector.append(vocabulary["UNKNOWN"])
        sentences_vector.append(vocabulary_vector)
    x = np.array(sentences_vector)
    temp = np.array(labels)
    for i in removed_ind:
        temp = np.delete(temp,i,0)
    y = np.array(temp)
    return [x, y]


def load_data():
    """
    Loads and preprocessed data for the dataset.
    Returns input vectors, labels, vocabulary, and inverse vocabulary.
    """
    # Load and preprocess data
    sentences, labels = load_data_and_labels()
    sentences_padded, removed_ind = pad_sentences(sentences)
    vocabulary, vocabulary_inv = build_vocab(sentences_padded)
    x, y = build_input_data(sentences_padded, removed_ind, labels, vocabulary)
    return [x, y, vocabulary, vocabulary_inv]


def build_embedding_matrix(vocabulary_inv):
    embedding_dim = 300
    embedding_matrix = np.random.uniform(-0.5,0.5,(len(vocabulary_inv), embedding_dim))
    model = gensim.models.KeyedVectors.load_word2vec_format('./../movie2vec/google-w2v.bin', binary=True)
    for id,word in enumerate(vocabulary_inv):
        try:
            embedding_matrix[id] = model.get_vector(word)
        except KeyError:
            continue
    return embedding_matrix
