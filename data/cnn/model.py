import numpy as np

from keras.layers import Input, Dense, Embedding, Conv2D, MaxPool2D
from keras.layers import Reshape, Flatten, Dropout, Concatenate
from keras.callbacks import ModelCheckpoint
from keras.optimizers import Adam
from keras.models import Model
from sklearn.model_selection import train_test_split
from data_helpers import load_data, build_embedding_matrix
import keras.backend as K
import tensorflow as tf
import sys

sys.path.append("./../")

import metrics as met


def recall(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
    return true_positives / (possible_positives + K.epsilon())


def precision(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
    return true_positives / (predicted_positives + K.epsilon())


def precision_threshold(threshold=0.5):
    def precision(y_true, y_pred):
        """Precision metric.
        Computes the precision over the whole batch using threshold_value.
        """
        threshold_value = threshold
        # Adaptation of the "round()" used before to get the predictions. Clipping to make sure that the predicted raw values are between 0 and 1.
        y_pred = K.cast(K.greater(K.clip(y_pred, 0, 1), threshold_value), K.floatx())
        # Compute the number of true positives. Rounding in prevention to make sure we have an integer.
        true_positives = K.round(K.sum(K.clip(y_true * y_pred, 0, 1), axis=1))
        # count the predicted positives
        predicted_positives = K.sum(y_pred, axis=1)
        # Get the precision ratio
        precision_ratio = true_positives / (predicted_positives + K.epsilon())
        return K.mean(precision_ratio)

    return precision


def precision_k_best(top_k=2):
    def precision(y_true, y_pred):
        """Precision metric.
        Computes the precision over the whole batch using threshold_value.
        """
        threshold_value = K.mean(tf.nn.top_k(y_pred, top_k, sorted=True).values[top_k - 1])
        # Adaptation of the "round()" used before to get the predictions. Clipping to make sure that the predicted raw values are between 0 and 1.
        y_pred = K.cast(K.greater(K.clip(y_pred, 0, 1), threshold_value), K.floatx())
        # Compute the number of true positives. Rounding in prevention to make sure we have an integer.
        true_positives = K.round(K.sum(K.clip(y_true * y_pred, 0, 1), axis=1))
        # count the predicted positives
        predicted_positives = K.sum(y_pred, axis=1)
        # Get the precision ratio
        precision_ratio = true_positives / (predicted_positives + K.epsilon())
        return K.mean(precision_ratio)

    return precision


def my_threshold(top_k):
    def threshold(y_true, y_pred):
        return K.mean(tf.nn.top_k(y_pred, top_k, sorted=True).values[top_k - 1])

    return threshold


def recall_k_best(top_k=2):
    def recall(y_true, y_pred):
        """Recall metric.
        Computes the recall over the whole batch using threshold_value.
        """
        threshold_value = K.mean(tf.nn.top_k(y_pred, top_k, sorted=True).values[top_k - 1])
        # Adaptation of the "round()" used before to get the predictions. Clipping to make sure that the predicted raw values are between 0 and 1.
        y_pred = K.cast(K.greater(K.clip(y_pred, 0, 1), threshold_value), K.floatx())
        # Compute the number of true positives. Rounding in prevention to make sure we have an integer.
        true_positives = K.round(K.sum(K.clip(y_true * y_pred, 0, 1), axis=1))
        # Compute the number of positive targets.
        possible_positives = K.sum(K.clip(y_true, 0, 1), axis=1)
        recall_ratio = true_positives / (possible_positives + K.epsilon())
        return K.mean(recall_ratio)

    return recall


def recall_threshold(threshold=0.5):
    def recall(y_true, y_pred):
        """Recall metric.
        Computes the recall over the whole batch using threshold_value.
        """
        threshold_value = threshold
        # Adaptation of the "round()" used before to get the predictions. Clipping to make sure that the predicted raw values are between 0 and 1.
        y_pred = K.cast(K.greater(K.clip(y_pred, 0, 1), threshold_value), K.floatx())
        # Compute the number of true positives. Rounding in prevention to make sure we have an integer.
        true_positives = K.round(K.sum(K.clip(y_true * y_pred, 0, 1)))
        # Compute the number of positive targets.
        possible_positives = K.sum(K.clip(y_true, 0, 1))
        recall_ratio = true_positives / (possible_positives + K.epsilon())
        return recall_ratio

    return recall


print('Loading data')
x, y, vocabulary, vocabulary_inv = load_data()

print(x.shape, y.shape, len(vocabulary), len(vocabulary_inv))

# x.shape -> (3924, 8124)
# y.shape -> (3924, 19)
# len(vocabulary) -> 18420
# len(vocabulary_inv) -> 18420

X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=42)

print(X_train.shape, y_train.shape, X_test.shape, y_test.shape)

# X_train.shape -> (3139, 8124)
# y_train.shape -> (3139, 19)
# X_test.shape -> (785, 8124)
# y_test.shape -> (785, 19)


sequence_length = x.shape[1]  # 8124
vocabulary_size = len(vocabulary_inv)  # 18420
embedding_dim = 300
filter_sizes = [3, 4, 5]
num_filters = 100
drop = 0.5

epochs = 100
batch_size = 50
# this returns a tensor
print("Creating Model...")
embedding_matrix = build_embedding_matrix(vocabulary_inv)
inputs = Input(shape=(sequence_length,), dtype='int32')
embedding = Embedding(input_dim=vocabulary_size, output_dim=embedding_dim, input_length=sequence_length,
                      weights=[embedding_matrix], trainable=True)(inputs)
# embedding.shape ---> (?, 8124, 300)
reshape = Reshape((sequence_length, embedding_dim, 1))(embedding)  # reshape.shape ---> (8124, 300, 1)

conv_0 = Conv2D(num_filters, kernel_size=(filter_sizes[0], embedding_dim), padding='valid', kernel_initializer='normal',
                activation='relu')(reshape)
conv_1 = Conv2D(num_filters, kernel_size=(filter_sizes[1], embedding_dim), padding='valid', kernel_initializer='normal',
                activation='relu')(reshape)
conv_2 = Conv2D(num_filters, kernel_size=(filter_sizes[2], embedding_dim), padding='valid', kernel_initializer='normal',
                activation='relu')(reshape)

maxpool_0 = MaxPool2D(pool_size=(sequence_length - filter_sizes[0] + 1, 1), strides=(1, 1), padding='valid')(conv_0)
maxpool_1 = MaxPool2D(pool_size=(sequence_length - filter_sizes[1] + 1, 1), strides=(1, 1), padding='valid')(conv_1)
maxpool_2 = MaxPool2D(pool_size=(sequence_length - filter_sizes[2] + 1, 1), strides=(1, 1), padding='valid')(conv_2)

concatenated_tensor = Concatenate(axis=1)([maxpool_0, maxpool_1, maxpool_2])
flatten = Flatten()(concatenated_tensor)
dropout = Dropout(drop)(flatten)
output = Dense(units=19, activation='softmax')(dropout)

# this creates a model that includes
model = Model(inputs=inputs, outputs=output)

checkpoint = ModelCheckpoint('weights.{epoch:03d}-{val_acc:.4f}-{val_loss:.4f}.hdf5', monitor='val_loss', verbose=1,
                             save_best_only=False, mode='auto')
adam = Adam(lr=1e-4, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
model.load_weights('weights.best.hdf5')

precisiontop2 = precision_k_best(2)
model.compile(optimizer=adam, loss='binary_crossentropy',
              metrics=['accuracy', my_threshold(2), my_threshold(3), my_threshold(4), precisiontop2,
                       precision_k_best(3), precision_k_best(4), recall_k_best(2), recall_k_best(3),
                       recall_k_best(4)])
print("Traning Model...")
model.fit(X_train, y_train, batch_size=batch_size, epochs=epochs, verbose=1, validation_data=(X_test, y_test),
          callbacks=[checkpoint])  # starts training #callbacks = [checkpoint]

prediction = model.predict(X_test)
idx = np.random.randint(0, len(X_test) - 10)
print(prediction[idx:idx + 10])
print(y_test[idx:idx + 10])


def stampa(p, t, threshold=0.22):
    p[p >= threshold] = 1
    p[p < threshold] = 0

    print("\n\n\n\n")
    print("at least one", met.at_least_one_metric(p, t))
    print("exact match", met.exact_match_metric(p, t))
    print("diminuited_f1", met.diminuited_f1_metric(p, t))
    print("precision", met.precision_metric(p, t))
    print("recall", met.recall_metric(p, t))
