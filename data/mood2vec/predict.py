import tensorflow as tf
from imblearn.ensemble import BalancedBaggingClassifier
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import train_test_split
import numpy as np
from sklearn import metrics
from data.NNet.nnmodel import nn_model
import keras.backend as K


def predict(df, model, epochs, batch_size, out_file_name):
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    sess = tf.Session(config=config)

    labels = df.keys()[2:]
    df_train, df_test = train_test_split(df, test_size=0.2, random_state=7)

    df['TEXT'] = df['TEXT'].str.lower()

    train_text = []
    for id in df_train.loc[:, 'ID'].tolist():
        train_text.append(model.docvecs[id])
    test_text = []
    for id in df_test.loc[:, 'ID'].tolist():
        test_text.append(model.docvecs[id])

    train_text = np.array(train_text)
    test_text = np.array(test_text)
    print("start")
    out_file = open(out_file_name)
    for label in labels:
        print(label, "\n--------------------------")

        if label == 'PROVOCA RABBIA/INDIGNAZIONE':
            continue
        train_label = df_train.loc[:, label].tolist()
        test_label = df_test.loc[:, label].tolist()
        nnm = nn_model(input_dim=train_text.shape[1])
        base_estimator = KerasClassifier(build_fn=nnm.build_model, epochs=epochs, batch_size=batch_size,
                                         verbose=2)
        bbc = BalancedBaggingClassifier(base_estimator=base_estimator, ratio='auto',
                                        replacement=False,
                                        random_state=0)

        bbc.fit(train_text, train_label)
        predictions = bbc.predict(test_text)
        names = ['0', '1']
        print("EVALUATION FOR LABEL: \n", label, "\n")
        print(metrics.classification_report(test_label, predictions, target_names=names))
        out_file.write("EVALUATION FOR LABEL: ")
        out_file.write(label)
        out_file.write("\n")
        out_file.write(metrics.classification_report(test_label, predictions, target_names=names))
        out_file.write("\n\n")
        K.clear_session()
    out_file.close()
