import pandas as pd
# noinspection PyUnresolvedReferences
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import pickle as pk
import tensorflow as tf
import sys

sys.path.append("./../")
from imblearn.ensemble import BalancedBaggingClassifier
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.decomposition import PCA
from sklearn.model_selection import train_test_split
import numpy as np
from sklearn import metrics
from nnmodel import nn_model
from cluster_classifier import CClassifier, ClusterDataBuilder
import keras.backend as K
import metrics as met

from estimator import ClusterClassifier

from sklearn.cluster import AgglomerativeClustering
from sklearn.linear_model import LogisticRegression

np.seterr(divide='ignore', invalid='ignore')


def mmetric(y_pred, y_true):
    y_pred = y_pred.astype(int)
    r = np.bitwise_and(y_pred, y_true).sum(axis=1)
    r[r >= 1] = 1
    return np.mean(r)


def build_input(df, doc2vec):
    df['TEXT'] = df['TEXT'].str.lower()

    train_text = []
    for id_ in df.loc[:, 'ID'].tolist():
        train_text.append(doc2vec.docvecs[id_])
    train_text = np.array(train_text)
    return train_text


def run_model(df, model, out_file_name, epochs=20, batch_size=256):
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    sess = tf.Session(config=config)

    labels = df.keys()[2:]
    df_train, df_test = train_test_split(df, test_size=0.2, random_state=7)
    train_text = build_input(df_train, model)
    test_text = build_input(df_test, model)

    print("start")
    out_file = open(out_file_name, "a+")
    for label in labels:
        print(label, "\n--------------------------")

        if label == 'PROVOCA RABBIA/INDIGNAZIONE':
            continue
        train_label = df_train.loc[:, label].tolist()
        test_label = df_test.loc[:, label].tolist()
        nnm = nn_model(input_dim=train_text.shape[1])
        base_estimator = KerasClassifier(build_fn=nnm.build_model, epochs=epochs, batch_size=batch_size,
                                         verbose=2)
        bbc = BalancedBaggingClassifier(base_estimator=base_estimator, ratio='auto',
                                        replacement=False,
                                        random_state=0)
        schifo = bbc
        bbc.fit(train_text, train_label)
        predictions = bbc.predict(test_text)
        names = ['0', '1']
        print("EVALUATION FOR LABEL: \n", label, "\n")
        print(metrics.classification_report(test_label, predictions, target_names=names))
        out_file.write("EVALUATION FOR LABEL: ")
        out_file.write(str(label))
        out_file.write("\n")
        out_file.write(metrics.classification_report(test_label, predictions, target_names=names))
        out_file.write("\n\n")
        K.clear_session()
    out_file.close()


def cluster(x, df, labels, cluster_method, penalty='l2'):
    moodvect = []
    # moodmodels = []
    for label in labels:
        y = df.loc[:, label].tolist()
        model = LogisticRegression(penalty=penalty)
        model.fit(x, y)
        moodvect.append(model.coef_[0])
        # moodmodels.append(model)
    c = cluster_method
    c.fit(moodvect)

    return c, moodvect


def mood_to_cluster(df, doc2vec, clustering_method):
    # import d2v model
    train_text = []
    for id in df.loc[:, 'ID'].tolist():
        train_text.append(doc2vec.docvecs[id])
    labels = df.keys()[2:-8]
    labels = labels.drop("PROVOCA RABBIA/INDIGNAZIONE")
    x = np.array(train_text)

    c, moodvect = cluster(x, df, labels, clustering_method)
    # clusterize
    cluster_labels = c.labels_

    label2cluster = {}
    for idx, label in enumerate(labels):
        label2cluster[label] = cluster_labels[idx]
    return label2cluster, moodvect


def clustered_dataset(df, mood2cluster):
    mood_labels = df.keys()[2:-8]
    mood_labels = mood_labels.drop("PROVOCA RABBIA/INDIGNAZIONE")
    df = df.copy()
    clusters = []
    for c in np.unique(list(mood2cluster.values())):
        cluster = str(c)
        df[c] = 0
        clusters.append(c)

    for index, row in df.loc[:, mood_labels].iterrows():
        for label in mood_labels:
            if row[label] == 1:
                cluster = mood2cluster[label]
                df.loc[index, cluster] = 1

    final_keys = list(df.keys()[:2]) + clusters
    new_df = df.loc[:, final_keys]
    return new_df


def visualize(moodvect, cluster_model, labels):
    pca = PCA(n_components=3)
    x_ = pca.fit_transform(moodvect)

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.view_init(7, -80)
    tag = cluster_model.labels_

    for l in np.unique(tag):
        ax.scatter(x_[tag == l, 0], x_[tag == l, 1], x_[tag == l, 2],
                   color=plt.cm.jet(np.float(l) / np.max(tag + 1)),
                   s=20, edgecolor='k')

    for i in range(len(x_)):  # plot each point + it's index as text above
        ax.text(x_[i, 0], x_[i, 1], x_[i, 2], str(tag[i]) + "_" + labels[i][:5], size=8, zorder=1,
                color='k')
    plt.show()


def metric_vect(y_pred, y):
    return np.array([
        met.at_least_one_metric(y_pred, y),
        met.exact_match_metric(y_pred, y),
        met.diminuited_f1_metric(y_pred, y),
        met.precision_metric(y_pred, y),
        met.recall_metric(y_pred, y)
    ])


# if __name__ == '__main__':
#
#     scores = np.zeros((10, 5))
#     config = tf.ConfigProto()
#     config.gpu_options.allow_growth = True
#     sess = tf.Session(config=config)
#     classifiers = []
#
#     for i in range(0, 10):
#         print("iteration", i + 1)
#
#         path = "./../new_extended_cleaned_dataset"
#         df_base = pd.read_csv(path + ".csv")
#
#         d2v_path = "./../NNet/d2vnew_extended_cleaned_dataset_clusters100.mod"
#         with open(d2v_path, "rb") as f:
#             doc2vec = pk.load(f)
#
#         labels = df_base.keys()[2:-8]
#         labels = labels.drop("PROVOCA RABBIA/INDIGNAZIONE")
#
#         n_clusters = 2
#         clustering_method = AgglomerativeClustering(n_clusters=n_clusters)
#         mood2cluster, moodvect = mood_to_cluster(df_base, doc2vec, clustering_method)
#         # visualize(moodvect, clustering_method, labels)
#         # print("continue?")
#         # if input() == "0":
#         #     exit()
#
#         print("set classifier")
#         clustered_data = clustered_dataset(df_base, mood2cluster)
#
#         random_state = np.random.randint(0, 100000)
#         # random_state = 12
#
#         df_train, df_test = train_test_split(df_base, test_size=0.2, random_state=random_state)
#         cdf_train, cdf_test = train_test_split(clustered_data, test_size=0.2, random_state=random_state)
#         test_labels = df_test.loc[:, df_base.keys()[2:-8]].values
#         print("start")
#         classifier = ClusterClassifier(df_train, cdf_train, doc2vec, batch_size=256, epochs=200, threshold=0.4)
#         classifier.fit(0, 1)
#
#         x = build_input(df_test, doc2vec)
#         y = test_labels
#         pred = classifier.predict(x)
#
#         print("\n\n\n\n")
#         print("at least one", met.at_least_one_metric(pred, y))
#         print("exact match", met.exact_match_metric(pred, y))
#         print("diminuited_f1", met.diminuited_f1_metric(pred, y))
#         print("precision", met.precision_metric(pred, y))
#         print("recall", met.recall_metric(pred, y))
#
#         scores[i] = np.array([
#             met.at_least_one_metric(pred, y),
#             met.exact_match_metric(pred, y),
#             met.diminuited_f1_metric(pred, y),
#             met.precision_metric(pred, y),
#             met.recall_metric(pred, y)
#         ])
#
#         print("partial avg", np.mean(scores, axis=0))
#         classifiers.append(classifier)
#
#         # proba = classifier.predict_proba(x)
#         # pred_proba = proba
#         # threshold = 0.4
#         # pred_proba[pred_proba >= threshold] = 1
#         # pred_proba[pred_proba < threshold] = 0
#         # print("PROB")
#         # print("at least one", met.at_least_one_metric(pred_proba, y))
#         # print("exact match", met.exact_match_metric(pred_proba, y))
#         # print("diminuited_f1", met.diminuited_f1_metric(pred_proba, y))
#         # print("precision", met.precision_metric(pred_proba, y))
#         # print("recall", met.recall_metric(pred_proba, y))
#
#     print(np.mean(scores, axis=0))


def train_classifiers(df_base):
    step = 50
    scores = np.zeros((step, 5))
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    sess = tf.Session(config=config)
    classifiers = []

    for i in range(0, step):
        print("iteration", i + 1)
        d2v_path = "./../NNet/d2vnew_extended_cleaned_dataset_clusters100.mod"
        with open(d2v_path, "rb") as f:
            doc2vec = pk.load(f)

        labels = df_base.keys()[2:-8]
        labels = labels.drop("PROVOCA RABBIA/INDIGNAZIONE")

        n_clusters = 2
        clustering_method = AgglomerativeClustering(n_clusters=n_clusters)
        mood2cluster, moodvect = mood_to_cluster(df_base, doc2vec, clustering_method)
        # visualize(moodvect, clustering_method, labels)
        # print("continue?")
        # if input() == "0":
        #     exit()

        print("set classifier")
        clustered_data = clustered_dataset(df_base, mood2cluster)

        random_state = np.random.randint(0, 100000)
        # random_state = 12

        df_train, df_test = train_test_split(df_base, test_size=0.2, random_state=random_state)
        cdf_train, cdf_test = train_test_split(clustered_data, test_size=0.2, random_state=random_state)
        test_labels = df_test.loc[:, df_base.keys()[2:-8]].values
        print("start")
        classifier = ClusterClassifier(df_train, cdf_train, doc2vec, batch_size=256, epochs=20, threshold=0.4)
        classifier.fit(0, 1)

        x = build_input(df_test, doc2vec)
        y = test_labels
        pred = classifier.predict(x)

        print("\n\n\n\n")
        print("at least one", met.at_least_one_metric(pred, y))
        print("exact match", met.exact_match_metric(pred, y))
        print("diminuited_f1", met.diminuited_f1_metric(pred, y))
        print("precision", met.precision_metric(pred, y))
        print("recall", met.recall_metric(pred, y))

        scores[i] = metric_vect(pred, y)

        print("partial avg", np.mean(scores[:i+1], axis=0))
        classifiers.append(classifier)

        # proba = classifier.predict_proba(x)
        # pred_proba = proba
        # threshold = 0.4
        # pred_proba[pred_proba >= threshold] = 1
        # pred_proba[pred_proba < threshold] = 0
        # print("PROB")
        # print("at least one", met.at_least_one_metric(pred_proba, y))
        # print("exact match", met.exact_match_metric(pred_proba, y))
        # print("diminuited_f1", met.diminuited_f1_metric(pred_proba, y))
        # print("precision", met.precision_metric(pred_proba, y))
        # print("recall", met.recall_metric(pred_proba, y))

    print(np.mean(scores, axis=0))

    return classifiers, scores
