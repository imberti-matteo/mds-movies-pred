import numpy as np
from sklearn.base import BaseEstimator, ClassifierMixin, TransformerMixin
from sklearn.utils.validation import check_X_y, check_array, check_is_fitted
from sklearn.utils.multiclass import unique_labels
from sklearn.metrics import euclidean_distances
from cluster_classifier import CClassifier


class ClusterClassifier(BaseEstimator, ClassifierMixin):
    def __init__(self, df_base, df_clustered, doc2vec, batch_size=256, epochs=200, threshold=0.3):
        self.epochs = epochs
        self.batch_size = batch_size
        self.df_base = df_base
        self.df_clustered = df_clustered
        self.doc2vec = doc2vec
        self.threshold = threshold
        self.step1_classifier = CClassifier(threshold=threshold)
        self.cluster_data = []
        self.data = []
        self.cluster_classifiers = []

        self.cluster_data_build()

    def build_input(self, df):
        df['TEXT'] = df['TEXT'].str.lower()

        train_text = []
        for id_ in df.loc[:, 'ID'].values:
            train_text.append(self.doc2vec.docvecs[id_])
        train_text = np.array(train_text)
        return train_text

    def cluster_data_build(self):
        clusters = self.df_clustered.keys()[2:]
        for label in clusters:
            train_data = self.df_base[self.df_clustered[label] == 1]
            # train_data = train_data.loc[:, (train_data != 0).any(axis=0)]
            self.cluster_data.append(train_data)

    def fit_clusters(self):
        for idx, data in enumerate(self.cluster_data):
            print("fitting cluster", idx)
            x = self.build_input(data)
            y_keys = data.keys()[2:-8]  # TODO -8 rimuove i target
            y = data.loc[:, y_keys].values
            c = CClassifier()
            # c.compile()
            c.fit(x, y, self.batch_size, self.epochs)
            self.cluster_classifiers.append(c)

    def fit(self, X, y):
        print("fitting step 1")
        x = self.build_input(self.df_base)
        y_keys = self.df_clustered.keys()[2:]
        y = self.df_clustered.loc[:, y_keys].values

        # c = CClassifier(threshold=self.threshold)
        self.step1_classifier.fit(x, y, self.batch_size, self.epochs)
        print("fitting step 2")
        self.fit_clusters()
        # # Check that X and y have correct shape
        # X, y = check_X_y(X, y)
        # # Store the classes seen during fit
        # self.classes_ = unique_labels(y)
        #
        # self.X_ = X
        # self.y_ = y
        # # Return the classifier
        return self

    def predict_cluster(self, x):
        idxs = []
        for a in x:
            nx = a.reshape((1, 100))
            c_indexes = []
            cluster_prediction_vector = self.step1_classifier.predict(nx).astype(int)
            probability_vector = self.step1_classifier.predict_proba(nx)
            if cluster_prediction_vector.sum() == 0:
                c_indexes.append(probability_vector.argmax())
            elif cluster_prediction_vector.sum() == 1:
                c_indexes.append(cluster_prediction_vector.argmax())
            elif cluster_prediction_vector.sum() >= 2:
                c_indexes.extend(probability_vector.argsort()[0][:2])
            idxs.append(c_indexes)

        return idxs

    def predict(self, x):
        # Check is fit had been called
        # check_is_fitted(self, ['X_', 'y_'])
        #
        # # Input validation
        # X = check_array(X)
        #
        # closest = np.argmin(euclidean_distances(X, self.X_), axis=1)

        indexes = self.predict_cluster(x)
        r = np.zeros((x.shape[0], 20))
        for i, idx_list in enumerate(indexes):
            new_x = np.array(x[i]).reshape((1, 100))
            if len(idx_list) < 2:
                r[i] = self.cluster_classifiers[idx_list[0]].predict(new_x)[0]
            else:
                pred1 = self.cluster_classifiers[idx_list[0]].predict(new_x)
                pred2 = self.cluster_classifiers[idx_list[1]].predict(new_x)
                pred1_proba = self.cluster_classifiers[idx_list[0]].predict_proba(new_x)[0]
                pred2_proba = self.cluster_classifiers[idx_list[1]].predict_proba(new_x)[0]

                if pred2.sum() == 0:
                    if pred1.sum() == 0:
                        w = np.zeros(20).astype(int)
                        w[pred1_proba.argmax()] = 1
                        w[pred2_proba.argmax()] = 1
                        # return self.y_[closest]
                        r[i] = w
                    else:
                        # migliori dal primo
                        w = np.zeros(20).astype(int)
                        position1 = pred1_proba.argsort()[0]
                        position2 = pred1_proba.argsort()[1]
                        w[position1] = 1
                        w[position2] = 1
                        # return self.y_[closest]
                        r[i] = w
                else:

                    w = np.zeros(20).astype(int)
                    w[pred1_proba.argmax()] = 1
                    w[pred2_proba.argmax()] = 1
                    # return self.y_[closest]
                    r[i] = w
        return r.astype(int)

    def predict_proba(self, x):
        # TODO fix
        indexes = self.predict_cluster(x)
        r = np.zeros((x.shape[0], 20))
        for i, idx_list in enumerate(indexes):
            nx = np.array(x[i]).reshape((1, 100))
            if len(idx_list) < 2:
                r[i] = self.cluster_classifiers[idx_list[0]].predict_proba(nx)[0]
            else:
                pred1_proba = self.cluster_classifiers[idx_list[0]].predict_proba(nx)[0]
                pred2_proba = self.cluster_classifiers[idx_list[1]].predict_proba(nx)[0]
                r[i] = pred1_proba + pred2_proba
        return r
