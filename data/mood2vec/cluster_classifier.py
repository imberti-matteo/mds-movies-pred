from keras import Sequential
from keras.layers import Dense, Dropout


class CClassifier:
    def __init__(self, threshold=0.4):
        self.threshold = threshold
        self.x = object
        self.y = object
        self.model = Sequential()
        self.is_compiled = False

    def fit(self, x, y, batch_size=128, epochs=20):
        self.x = x
        self.y = y
        if not self.is_compiled:
            self.compile()
        self.model.fit(self.x, self.y, batch_size=batch_size, epochs=epochs)

    def compile(self, optimizer='adam', loss='binary_crossentropy', metrics=['binary_accuracy']):
        self.is_compiled = True
        self.model.add(Dense(100, activation='relu', input_dim=self.x.shape[1]))
        self.model.add(Dropout(0.5))
        self.model.add(Dense(70, activation='relu'))
        self.model.add(Dropout(0.35))
        self.model.add(Dense(self.y.shape[1], activation='sigmoid'))

        self.model.compile(optimizer=optimizer, loss=loss, metrics=metrics)

    def predict(self, x):
        predictions = self.model.predict_proba(x)
        predictions[predictions >= self.threshold] = 1
        predictions[predictions < self.threshold] = 0
        return predictions.astype(int)

    def predict_proba(self, x):
        return self.model.predict_proba(x)

    def predict_classes(self, x):
        return self.model.predict_classes(x)

    def build_model(self, x, y):
        self.x = x
        self.y = y


class ClusterDataBuilder:
    def __init__(self, data_frame, clustered_data_frame):
        self.df = data_frame
        self.cl_df = clustered_data_frame
        self.clusters = self.cl_df.keys()[2:]

    def get_cluster_data(self, cluster):
        return self.cl_df[self.cl_df[cluster] == 1]
