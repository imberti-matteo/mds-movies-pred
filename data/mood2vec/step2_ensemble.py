import pickle as pk
import numpy as np
from sklearn.model_selection import train_test_split
from step2_pipeline import train_classifiers, build_input
from step2_pipeline import metric_vect
import pandas as pd
import tensorflow as tf

if __name__ == '__main__':
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    sess = tf.Session(config=config)

    path = "./../new_extended_cleaned_dataset"
    df_base = pd.read_csv(path + ".csv")

    d2v_path = "./../NNet/d2vnew_extended_cleaned_dataset_clusters100.mod"
    with open(d2v_path, "rb") as f:
        doc2vec = pk.load(f)

    random_state = np.random.randint(0, 100000)
    df_train, df_test = train_test_split(df_base, test_size=0.2, random_state=random_state)
    x = build_input(df_test, doc2vec)

    classifiers, avg_scores = train_classifiers(df_train)

    test_labels = df_test.loc[:, df_base.keys()[2:-8]].values

    probabilities = np.zeros((50, x.shape[0], 20))
    for i, c in enumerate(classifiers):
        probabilities[i] = c.predict_proba(x)

    pred_proba = np.mean(probabilities, axis=0)
    pred = pred_proba
    threshold = 0.4
    pred[pred >= threshold] = 1
    pred[pred < threshold] = 0

    ensemble_scores = metric_vect(pred, test_labels)

    print("avg \n", np.mean(avg_scores))
    print("ensemble \n", ensemble_scores)
