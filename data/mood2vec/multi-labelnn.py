import pickle as pk
from pathlib import Path

import nltk
import numpy as np
import pandas as pd
import tensorflow as tf
from keras import Sequential
from keras.layers import Dense, Dropout
from nltk.stem.porter import PorterStemmer
from sklearn.cluster import AgglomerativeClustering
from sklearn.model_selection import train_test_split
import keras.backend as K
import pipeline

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)


def my_metric(y_pred, y_true, threshold):
    y_pred[y_pred > threshold] = 1
    y_pred[y_pred <= threshold] = 0
    y_pred = y_pred.astype(int)
    res = np.bitwise_and(y_pred, y_true).sum() / np.bitwise_or(y_pred, y_true).sum()
    return np.mean(res)


def mmetric(y_pred, y_true):
    y_pred = y_pred.astype(int)
    r = np.bitwise_and(y_pred,y_true).sum(axis=1)
    return np.mean(r)


def metric2(y_pred, y_true, threshold=0.5):
    y_pred[y_pred > threshold] = 1
    y_pred[y_pred <= threshold] = 0
    y_pred = y_pred.astype(int)
    res = np.bitwise_and(y_pred, y_true).sum() / y_true.sum()
    return np.mean(res)


def tokenize(text):
    tokens = nltk.word_tokenize(text)
    stems = []
    for item in tokens:
        stems.append(PorterStemmer().stem(item))
    return stems


print("load data")
df_name = "new_extended_cleaned_dataset"
model_name = df_name
df_base = df = pd.read_csv("./../" + df_name + ".csv")

# TODO check
path = "./../NNet/d2vnew_extended_cleaned_dataset_clusters100.mod"
print("doc2vec")
v_size = 100
if Path(path).exists():
    f = open(path, "rb")
    model = pk.load(f)
    f.close()
mood2cluster, moodvect = pipeline.mood_to_cluster(df_base, model,
                                                  clustering_method=AgglomerativeClustering(n_clusters=4))
df_base = df = pipeline.clustered_dataset(df_base, mood2cluster)
# -------------------------------------

labels = df_base.keys()[2:]
df_train, df_test = train_test_split(df_base, test_size=0.2, random_state=7)

df['TEXT'] = df['TEXT'].str.lower()

train_text = df_train.loc[:, 'TEXT'].str.lower().tolist()
test_text = df_test.loc[:, 'TEXT'].str.lower().tolist()

# else:
#     print("tokenize")
#     df['TEXT'] = df['TEXT'].apply(word_tokenize)
#
#     doc = []
#
#     for sequence, movie_id in zip(df['TEXT'], df['ID']):
#         s = TaggedDocument(words=sequence, tags=[movie_id])
#         doc.append(s)
#
#
#
#
#     model = Doc2Vec(doc, vector_size=v_size, window=4, min_count=2, workers=8, epochs=15, dbow_words=1, dm=0)
#     model.train(doc, total_examples=model.corpus_count, epochs=model.epochs
#
#     with open(path, "wb") as f:
#         pk.dump(model, f)



epochs = 100
batch_size = 256

train_text = []
for id in df_train.loc[:, 'ID'].tolist():
    train_text.append(model.docvecs[id])

test_text = []
for id in df_test.loc[:, 'ID'].tolist():
    test_text.append(model.docvecs[id])

train_text = np.array(train_text)
test_text = np.array(test_text)
print("start")
out_file = open("blabal", "a+")

train_label = df_train[labels].values
test_label = df_test[labels].values
m = Sequential()
m.add(Dense(256, activation='relu', input_dim=train_text.shape[1]))
m.add(Dropout(0.5))
m.add(Dense(32, activation='relu'))
m.add(Dropout(0.35))
m.add(Dense(train_label.shape[1], activation='sigmoid'))
m.compile(optimizer='adam',
          loss='binary_crossentropy',
          metrics=['binary_accuracy'])

m.fit(train_text, train_label, batch_size=batch_size, epochs=epochs, validation_data=[train_text, train_label])

predictions = m.predict(test_text)
print("metric", metric2(predictions, test_label, threshold=0.3))

# names = ['0', '1']
# print("EVALUATION FOR LABEL: \n", label, "\n")
# print(metrics.classification_report(test_label, predictions, target_names=names))
# out_file.write("EVALUATION FOR LABEL: ")
# out_file.write(label)
# out_file.write("\n")
# out_file.write(metrics.classification_report(test_label, predictions, target_names=names))
# out_file.write("\n\n")
# K.clear_session()
out_file.close()
