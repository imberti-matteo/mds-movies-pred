import pickle as pk
# noinspection PyUnresolvedReferences
from mpl_toolkits.mplot3d import Axes3D
from sklearn.cluster import AgglomerativeClustering, AffinityPropagation, DBSCAN, KMeans
from sklearn.decomposition import PCA
import numpy as np
import pandas as pd
from sklearn.linear_model import LogisticRegression
import matplotlib.pyplot as plt

path = "./../new_extended_cleaned_dataset"
save = True
df = pd.read_csv(path + ".csv")

with open("./../NNet/d2vnew_extended_cleaned_dataset_clusters100.mod", "rb") as f:
    d2v = pk.load(f)

train_text = []
for id in df.loc[:, 'ID'].tolist():
    train_text.append(d2v.docvecs[id])

labels = df.keys()[2:-8]
# labels = df.keys()[-8:]
x = train_text = np.array(train_text)

moodvect = []
moodmodels = []
labels = labels.drop("PROVOCA RABBIA/INDIGNAZIONE")
for label in labels:
    y = train_label = df.loc[:, label].tolist()

    model = LogisticRegression(penalty='l2')
    model.fit(x, y)
    moodvect.append(model.coef_[0])
    moodmodels.append(model)

fig = plt.figure()

c = AffinityPropagation(damping=.7)
c.fit(moodvect)

pca = PCA(n_components=3)
x_ = pca.fit_transform(moodvect)

ax = fig.add_subplot(111, projection='3d')
ax.view_init(7, -80)
tag = c.labels_

for l in np.unique(tag):
    ax.scatter(x_[tag == l, 0], x_[tag == l, 1], x_[tag == l, 2],
               color=plt.cm.jet(np.float(l) / np.max(tag + 1)),
               s=20, edgecolor='k')

for i in range(len(x_)):  # plot each point + it's index as text above
    ax.text(x_[i, 0], x_[i, 1], x_[i, 2], str(tag[i]) + "_" + labels[i][:5], size=8, zorder=1,
            color='k')

mood2vec = {}

for i, label in enumerate(labels):
    mood2vec[label] = moodvect[i]

mood2cluster = {}
for i, label in enumerate(labels):
    mood2cluster[label] = tag[i]

if save is True:
    with open("mod2vec_cluster_tuple.dat", "wb") as f:
        pk.dump((mood2vec, mood2cluster), f)

ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')

plt.show()
