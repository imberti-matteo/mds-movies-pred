import pandas as pd
import data.sinossi as sin

df_path = "../dataset.csv"
out_path = "sinossi-imdb_trama-wiki.csv"

df = pd.read_csv(df_path)
df = df[df['TIPO'] == 'FILM']
df.reset_index(inplace=True)

with open("index.txt", "r") as f:
    index = int(f.read())
if index == 0:
    ds = pd.DataFrame(columns=['ID', 'IMDB', 'WIKI'])
    ds.to_csv(out_path)

df = df.loc[index:]

for movie_id, imdb_link, wiki_link in zip(df['ID'], df['LINK_IMDB'], df['LINK_WIKI']):
    wiki_link_en = sin.get_wiki_link_en(wiki_link)
    sinossi = sin.get_sinossi_imdb(imdb_link)
    trama = sin.get_wiki_trama(wiki_link_en)

    out = pd.DataFrame([[movie_id, sinossi, trama]], columns=['ID', 'IMDB', 'WIKI'])
    out.to_csv(out_path, header=False, mode='a')
    index += 1
    with open("index.txt", "w") as f:
        f.write(str(index))
