import urllib

import requests as req
from bs4 import BeautifulSoup
import wikipedia
import re
from googletrans import Translator

tail = "/plotsummary?ref_=tt_stry_pl#synopsis"
tail_sum = "/plotsummary?ref_=tt_stry_pl#summaries"

def get_sinossi_imdb(link):
    url = str(link) + tail
    print("Estraggo la Synopsis...")
    try:
        r = req.get(url)
    except req.exceptions.ConnectionError:
        print("error parsing url", url)
        exit(-1)
    except req.exceptions.MissingSchema:
        print("not valid url", url)
        return "NA"
    if not r.ok:
        return "NA"
    soup = BeautifulSoup(r.content, "html.parser")
    synopsis_html = soup.find(id="plot-synopsis-content")

    synopsis = synopsis_html.text
    synopsis_len = len(synopsis)
    if "It looks like we don't have a Synopsis for this title yet." in synopsis:
        print("Estraggo il Summary perchè la Synopsis non c'è...")
        synopsis = get_summary_imdb(link)
    elif synopsis_len < 300:
        print("Estraggo il Summary perchè la Synopsis è troppo corta...")
        candidate_syn = get_summary_imdb(link)
        if len(candidate_syn) > synopsis_len:
            print("Uso il Summary")
            synopsis = candidate_syn

    return synopsis


def get_wiki_link_en(link_ita):
    print("Accedo alla Trama in Inglese...")
    if not isinstance(link_ita, str):
        return "NA"
    try:
        r = req.get(link_ita)
    except req.exceptions.ConnectionError:
        print("error parsing url", link_ita)
        return "NA"
    except req.exceptions.MissingSchema:
        print("not valid url", link_ita)
        return "NA"
    if not r.ok:
        return "NA"

    soup = BeautifulSoup(r.content, "html.parser")
    try:
        link_en = soup.select_one("a[href*=en.wikipedia.org/wiki]").get("href")
    except AttributeError:
        return "NA"
    # link_html = soup.find(class_="interlanguage-link interwiki-en")
    # link_en = link_html.findAll('a')[0].get("a")
    return link_en


def get_wiki_trama(link_en):
    if link_en == "NA" or link_en is None:
        print("Trama in Inglese non disponibile")
        return True
    print("Estraggo la Trama in Inglese...")
    try:
        r = req.get(link_en)
    except req.exceptions.ConnectionError:
        print("error parsing url", link_en)
        return "NA"
    if not r.ok:
        return "NA"

    page = link_en.split("/")[-1]
    page = urllib.parse.unquote(page)
    wikipedia.set_lang("en")
    try:
        text = wikipedia.page(page, auto_suggest=False).section("Plot")
    except wikipedia.exceptions.DisambiguationError:
        print("disambiguation error", link_en)
        return "NA"
    except wikipedia.exceptions.PageError:
        try:
            text = wikipedia.page(page).section("Plot")
        except wikipedia.PageError:
            print("page error", page)
            return "NA"
    if text is None:
        return "NA"

    return text

def get_summary_imdb(link):
    url = str(link) + tail_sum
    try:
        r = req.get(url)
    except req.exceptions.ConnectionError:
        print("error parsing url", url)
        exit(-1)
    except req.exceptions.MissingSchema:
        print("not valid url", url)
        return "NA"
    if not r.ok:
        return "NA"
    soup = BeautifulSoup(r.content, "html.parser")
    summaries_html = soup.find(id="plot-summaries-content")
    longest_summary = ""
    for i in range(int(len(summaries_html.contents)/2)):
        summary = summaries_html.contents[2*i+1].text
        if "It looks like we don't have any Plot Summaries for this title yet." in summary:
            print("Summary non disponibile")
            return "NA"
        summary = re.sub('—.*','',summary)
        if len(summary) > len(longest_summary):
            longest_summary = summary
    return longest_summary

def get_wiki_trama_translated(link_ita):
    print("Accedo alla Trama in italiano...")
    if not isinstance(link_ita, str):
        return "NA"
    try:
        r = req.get(link_ita)
    except req.exceptions.ConnectionError:
        print("error parsing url", link_ita)
        return "NA"
    except req.exceptions.MissingSchema:
        print("not valid url", link_ita)
        return "NA"
    if not r.ok:
        return "NA"

    page = link_ita.split("/")[-1]
    page = urllib.parse.unquote(page)
    wikipedia.set_lang("it")
    try:
        text = wikipedia.page(page, auto_suggest=False).section("Trama")
    except wikipedia.exceptions.DisambiguationError:
        print("disambiguation error", link_ita)
        return "NA"
    except wikipedia.exceptions.PageError:
        try:
            text = wikipedia.page(page).section("Trama")
        except wikipedia.PageError:
            print("page error", page)
            return "NA"
        except wikipedia.exceptions.DisambiguationError:
            print("disambiguation error", link_ita)
            return "NA"
    if text is None:
        return "NA"

    print("Traduco la Trama...")
    translator = Translator()
    text_translated = translator.translate(text, src='it',dest='en')
    if (text_translated.text == text):
        print("TRADUCO IN 4 ROUND...")
        text_part1 = text[:int(len(text)/2)]
        text_part2 = text[int(len(text) / 2):]
        text_translated_part1 = translator.translate(text_part1[:int(len(text_part1)/2)], src='it', dest='en')
        text_translated_part2 = translator.translate(text_part1[int(len(text_part1)/2):], src='it', dest='en')
        text_translated_part3 = translator.translate(text_part2[:int(len(text_part2)/2)], src='it', dest='en')
        text_translated_part4 = translator.translate(text_part2[int(len(text_part2)/2):], src='it', dest='en')
        text_translated = text_translated_part1.text + text_translated_part2.text + text_translated_part3.text + text_translated_part4.text
        return text_translated

    return text_translated.text
