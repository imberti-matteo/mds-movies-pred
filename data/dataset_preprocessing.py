import pandas as pd
import numpy as np
import re

df_trame = pd.read_csv("dataset_più lunghi.csv")
df_trame.set_index('Unnamed: 0', inplace=True)
df_trame.reset_index(drop=True, inplace=True)

df_label = pd.read_csv("label_binarizzate.csv")

df = pd.merge(df_trame,df_label, on="ID")

#pulizia dei caratteri anomali nel testo
for i in range(len(df.loc[:,'TEXT'])):
    df.loc[i,'TEXT'] = df.loc[i,'TEXT'].replace('\r','').replace('\n','')
    if('&#' in df.loc[i,'TEXT']):
        df.loc[i, 'TEXT'] = re.sub('[0-9]?&#[0-9]{5};','',df.loc[i,'TEXT'])

df.to_csv('cleaned_dataset.csv',index=False)



