from keras import Sequential
from keras.layers import Dense, Dropout
import numpy as np
import sys
from sklearn.base import BaseEstimator, ClassifierMixin, TransformerMixin
# from sklearn.utils.validation import check_X_y, check_array, check_is_fitted
# from sklearn.utils.multiclass import unique_labels
# from sklearn.metrics import euclidean_distances
sys.path.append("./../")
import metrics as met
import pickle as pk
import tensorflow as tf
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.cluster import AgglomerativeClustering
from sklearn.linear_model import LogisticRegression


class CClassifier:
    def __init__(self, threshold=0.4, hidden_layers=2, hidden_neurons=[200,100]):
        self.threshold = threshold
        self.hidden_layers = hidden_layers
        self.hidden_neurons = hidden_neurons
        self.x = object
        self.y = object
        self.model = Sequential()
        self.is_compiled = False

    def fit(self, x, y, batch_size=128, epochs=20):
        self.x = x
        self.y = y
        if not self.is_compiled:
            self.compile()
        self.model.fit(self.x, self.y, batch_size=batch_size, epochs=epochs,verbose=0)

    def compile(self, optimizer='adam', loss='binary_crossentropy', metrics=['binary_accuracy']):
        self.is_compiled = True
        print("Neural Network - Neurons: ",self.hidden_neurons)
        self.model.add(Dense(self.hidden_neurons[0], activation='relu', input_dim=self.x.shape[1]))
        self.model.add(Dropout(0.5))
        for i in range(self.hidden_layers-1):
            self.model.add(Dense(self.hidden_neurons[i+1], activation='relu'))
            self.model.add(Dropout(0.5))
        self.model.add(Dense(self.y.shape[1], activation='sigmoid'))

        self.model.compile(optimizer=optimizer, loss=loss, metrics=metrics)

    def predict(self, x):
        predictions = self.model.predict_proba(x)
        predictions[predictions >= self.threshold] = 1
        predictions[predictions < self.threshold] = 0
        return predictions.astype(int)

    def predict_proba(self, x):
        return self.model.predict_proba(x)

    def predict_classes(self, x):
        return self.model.predict_classes(x)

    def build_model(self, x, y):
        self.x = x
        self.y = y


class ClusterClassifier(BaseEstimator, ClassifierMixin):
    def __init__(self, df_base, df_clustered, doc2vec, batch_size=256, epochs=300, threshold=0.4, hidden_layers=2, hidden_neurons=[200,100]):
        self.epochs = epochs
        self.batch_size = batch_size
        self.df_base = df_base
        self.df_clustered = df_clustered
        self.m2v = doc2vec
        self.threshold = threshold
        self.hidden_layers = hidden_layers
        self.hidden_neurons = hidden_neurons
        self.step1_classifier = CClassifier(threshold=threshold,hidden_layers=hidden_layers,hidden_neurons=hidden_neurons)
        self.cluster_data = []
        self.data = []
        self.cluster_classifiers = []

        self.cluster_data_build()

    def build_input(self, df):
        train_text = []
        for id_ in df.loc[:, 'ID'].values:
            found_df = self.m2v[self.m2v['ID'] == id_]
            train_text.append(found_df.iloc[0, 1])
        train_text = np.array(train_text)
        return train_text

    def cluster_data_build(self):
        clusters = self.df_clustered.keys()[2:]
        for label in clusters:
            train_data = self.df_base[self.df_clustered[label] == 1]
            # train_data = train_data.loc[:, (train_data != 0).any(axis=0)]
            self.cluster_data.append(train_data)

    def fit_clusters(self):
        for idx, data in enumerate(self.cluster_data):
            print("fitting cluster", idx)
            x = self.build_input(data)
            y_keys = data.keys()[2:-8]  # TODO -8 rimuove i target
            y = data.loc[:, y_keys].values
            c = CClassifier(threshold=self.threshold,hidden_layers=self.hidden_layers,hidden_neurons=self.hidden_neurons)
            c.fit(x, y, self.batch_size, self.epochs)
            self.cluster_classifiers.append(c)

    def fit(self, X, Y):
        print("fitting step 1")
        x = self.build_input(self.df_base)
        y_keys = self.df_clustered.keys()[2:]
        y = self.df_clustered.loc[:, y_keys].values

        # c = CClassifier(threshold=self.threshold)
        self.step1_classifier.fit(x, y, self.batch_size, self.epochs)
        print("fitting step 2")
        self.fit_clusters()
        # # Check that X and y have correct shape
        # X, y = check_X_y(X, y)
        # # Store the classes seen during fit
        # self.classes_ = unique_labels(y)
        #
        # self.X_ = X
        # self.y_ = y
        # # Return the classifier
        return self

    def predict_cluster(self, x):
        idxs = []
        for a in x:
            nx = a.reshape((1, 300))
            c_indexes = []
            cluster_prediction_vector = self.step1_classifier.predict(nx).astype(int)
            probability_vector = self.step1_classifier.predict_proba(nx)
            if cluster_prediction_vector.sum() == 0:
                c_indexes.append(probability_vector.argmax())
            elif cluster_prediction_vector.sum() == 1:
                c_indexes.append(cluster_prediction_vector.argmax())
            elif cluster_prediction_vector.sum() >= 2:
                c_indexes.extend(probability_vector.argsort()[0][:2])
            idxs.append(c_indexes)

        return idxs

    def predict(self, x):

        indexes = self.predict_cluster(x)
        r = np.zeros((x.shape[0], 20))
        for i, idx_list in enumerate(indexes):
            new_x = np.array(x[i]).reshape((1, 300))
            if len(idx_list) < 2:
                r[i] = self.cluster_classifiers[idx_list[0]].predict(new_x)[0]
            else:
                pred1 = self.cluster_classifiers[idx_list[0]].predict(new_x)
                pred2 = self.cluster_classifiers[idx_list[1]].predict(new_x)
                pred1_proba = self.cluster_classifiers[idx_list[0]].predict_proba(new_x)[0]
                pred2_proba = self.cluster_classifiers[idx_list[1]].predict_proba(new_x)[0]

                if pred2.sum() == 0:
                    if pred1.sum() == 0:
                        w = np.zeros(20).astype(int)
                        w[pred1_proba.argmax()] = 1
                        w[pred2_proba.argmax()] = 1
                        # return self.y_[closest]
                        r[i] = w
                    else:
                        # migliori dal primo
                        w = np.zeros(20).astype(int)
                        position1 = pred1_proba.argsort()[0]
                        position2 = pred1_proba.argsort()[1]
                        w[position1] = 1
                        w[position2] = 1
                        # return self.y_[closest]
                        r[i] = w
                else:

                    w = np.zeros(20).astype(int)
                    w[pred1_proba.argmax()] = 1
                    w[pred2_proba.argmax()] = 1
                    # return self.y_[closest]
                    r[i] = w
        return r.astype(int)

    def predict_proba(self, x):
        # TODO fix
        indexes = self.predict_cluster(x)
        r = np.zeros((x.shape[0], 20))
        for i, idx_list in enumerate(indexes):
            nx = np.array(x[i]).reshape((1, 300))
            if len(idx_list) < 2:
                r[i] = self.cluster_classifiers[idx_list[0]].predict_proba(nx)[0]
            else:
                pred1_proba = self.cluster_classifiers[idx_list[0]].predict_proba(nx)[0]
                pred2_proba = self.cluster_classifiers[idx_list[1]].predict_proba(nx)[0]
                r[i] = pred1_proba + pred2_proba
        return r

def build_input(df,m2v):
    train_text = []
    for id_ in df.loc[:, 'ID'].values:
        found_df = m2v[m2v['ID'] == id_]
        train_text.append(found_df.iloc[0,1])
    train_text = np.array(train_text)
    return train_text

def cluster(x, df, labels, cluster_method, penalty='l2'):
    moodvect = []
    # moodmodels = []
    for label in labels:
        y = df.loc[:, label].tolist()
        model = LogisticRegression(penalty=penalty)
        model.fit(x, y)
        moodvect.append(model.coef_[0])
        # moodmodels.append(model)
    c = cluster_method
    c.fit(moodvect)

    return c, moodvect

def mood_to_cluster(df, doc2vec, clustering_method):
    # import d2v model
    train_text = build_input(df,doc2vec)
    labels = df.keys()[2:-8]
    labels = labels.drop("PROVOCA RABBIA/INDIGNAZIONE")

    c, moodvect = cluster(train_text, df, labels, clustering_method)
    # clusterize
    cluster_labels = c.labels_

    label2cluster = {}
    for idx, label in enumerate(labels):
        label2cluster[label] = cluster_labels[idx]
    return label2cluster, moodvect


def clustered_dataset(df, mood2cluster):
    mood_labels = df.keys()[2:-8]
    mood_labels = mood_labels.drop("PROVOCA RABBIA/INDIGNAZIONE")
    df = df.copy()
    clusters = []
    for c in np.unique(list(mood2cluster.values())):
        cluster = str(c)
        df[c] = 0
        clusters.append(c)

    for index, row in df.loc[:, mood_labels].iterrows():
        for label in mood_labels:
            if row[label] == 1:
                cluster = mood2cluster[label]
                df.loc[index, cluster] = 1

    final_keys = list(df.keys()[:2]) + clusters
    new_df = df.loc[:, final_keys]
    return new_df

def metric_vect(y_pred, y):
    return np.array([
        met.at_least_one_metric(y_pred, y),
        met.exact_match_metric(y_pred, y),
        met.diminuited_f1_metric(y_pred, y),
        met.precision_metric(y_pred, y),
        met.recall_metric(y_pred, y)
    ])

def train_classifiers(df_base, layers, neurons):
    scores = np.zeros((3, 5))
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    sess = tf.Session(config=config)
    classifiers = []

    for i in range(0, 3):
        print("iteration", i + 1)
        model_name = "new_extended_cleaned_dataset"
        v_size = 300
        path = "./m2v_" + model_name + "_" + str(v_size) + ".mod"
        with open(path, "rb") as f:
            df_m2v = pk.load(f)

        labels = df_base.keys()[2:-8]
        labels = labels.drop("PROVOCA RABBIA/INDIGNAZIONE")

        n_clusters = 2
        clustering_method = AgglomerativeClustering(n_clusters=n_clusters)
        mood2cluster, moodvect = mood_to_cluster(df_base, df_m2v, clustering_method)

        print("set classifier")
        clustered_data = clustered_dataset(df_base, mood2cluster)

        random_state = np.random.randint(0, 100000)
        # random_state = 12

        df_train, df_test = train_test_split(df_base, test_size=0.2, random_state=random_state)
        cdf_train, cdf_test = train_test_split(clustered_data, test_size=0.2, random_state=random_state)
        test_labels = df_test.loc[:, df_base.keys()[2:-8]].values
        print("start")
        classifier = ClusterClassifier(df_train, cdf_train, df_m2v, batch_size=256, epochs=300, threshold=0.4, hidden_layers=layers, hidden_neurons=neurons)
        classifier.fit(0, 1)

        x = build_input(df_test, df_m2v)
        y = test_labels
        pred = classifier.predict(x)

        scores[i] = metric_vect(pred, y)

        classifiers.append(classifier)

    return classifiers, scores



if __name__ == '__main__':
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    sess = tf.Session(config=config)

    path = "./../new_extended_cleaned_dataset"
    df_base = pd.read_csv(path + ".csv")

    m2v_path = "./m2v_new_extended_cleaned_dataset_300.mod"
    with open(m2v_path, "rb") as f:
        movie2vec = pk.load(f)

    random_state = np.random.randint(0, 100000)
    df_train, df_test = train_test_split(df_base, test_size=0.2, random_state=random_state)
    x = build_input(df_test,movie2vec)
    test_labels = df_test.loc[:, df_base.keys()[2:-8]].values

    start_number = 20
    end_number = 260
    delta = 10
    best_score = 0.7999154
    best_ensemble_score = []
    winning_architecture = [220]
    for hidden_layers in range(1,3,1):
        if hidden_layers+1 == 1:
            for z in range(start_number, end_number, delta):
                classifiers, avg_scores = train_classifiers(df_train,layers=hidden_layers+1,neurons=[z])
                probabilities = np.zeros((3, x.shape[0], 20))
                for i, c in enumerate(classifiers):
                    probabilities[i] = c.predict_proba(x)
                pred_proba = np.mean(probabilities, axis=0)
                pred = pred_proba
                threshold = 0.4
                pred[pred >= threshold] = 1
                pred[pred < threshold] = 0
                ensemble_scores = metric_vect(pred, test_labels)
                score = ensemble_scores[0]+ensemble_scores[2]
                if score > best_score:
                    best_score = score
                    best_ensemble_score = ensemble_scores
                    winning_architecture = [z]
                    print("NEW WINNING ARCHITECTURE: ",winning_architecture)
                    print(best_ensemble_score)
                else:
                    print("BEST: ",winning_architecture)
                    print(best_ensemble_score)

        elif hidden_layers+1 == 2:
            for y in range(start_number, end_number, delta):
                for z in range(start_number, end_number, delta):
                    classifiers, avg_scores = train_classifiers(df_train,layers=hidden_layers+1,neurons=[y,z])
                    probabilities = np.zeros((3, x.shape[0], 20))
                    for i, c in enumerate(classifiers):
                        probabilities[i] = c.predict_proba(x)
                    pred_proba = np.mean(probabilities, axis=0)
                    pred = pred_proba
                    threshold = 0.4
                    pred[pred >= threshold] = 1
                    pred[pred < threshold] = 0
                    ensemble_scores = metric_vect(pred, test_labels)
                    score = ensemble_scores[0] + ensemble_scores[2]
                    if score > best_score:
                        best_score = score
                        best_ensemble_score = ensemble_scores
                        winning_architecture = [y,z]
                        print("NEW WINNING ARCHITECTURE: ", winning_architecture)
                        print(best_ensemble_score)
                    else:
                        print("BEST: ", winning_architecture)
                        print(best_ensemble_score)

        else:
            for x in range(start_number,end_number,delta):
                for y in range(start_number, end_number, delta):
                    for z in range(start_number, end_number, delta):
                        classifiers, avg_scores = train_classifiers(df_train,layers=hidden_layers+1,neurons=[x,y,z])
                        probabilities = np.zeros((3, x.shape[0], 20))
                        for i, c in enumerate(classifiers):
                            probabilities[i] = c.predict_proba(x)
                        pred_proba = np.mean(probabilities, axis=0)
                        pred = pred_proba
                        threshold = 0.4
                        pred[pred >= threshold] = 1
                        pred[pred < threshold] = 0
                        ensemble_scores = metric_vect(pred, test_labels)
                        score = ensemble_scores[0] + ensemble_scores[2]
                        if score > best_score:
                            best_score = score
                            best_ensemble_score = ensemble_scores
                            winning_architecture = [x,y,z]
                            print("NEW WINNING ARCHITECTURE: ", winning_architecture)
                            print(best_ensemble_score)
                        else:
                            print("BEST: ", winning_architecture)
                            print(best_ensemble_score)

