from keras import Sequential
from keras.layers import Dense, Dropout
import keras.backend as K


def recall(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
    return true_positives / (possible_positives + K.epsilon())


def precision(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
    return true_positives / (predicted_positives + K.epsilon())


def fbeta_score(y_true, y_pred, beta=1):
    if beta < 0:
        raise ValueError('The lowest choosable beta is zero (only precision).')
    if K.sum(K.round(K.clip(y_true, 0, 1))) == 0:
        return 0
    p = precision(y_true, y_pred)
    r = recall(y_true, y_pred)
    bb = beta ** 2
    return (1 + bb) * (p * r) / (bb * p + r + K.epsilon())


def fmeasure(y_true, y_pred):
    return fbeta_score(y_true, y_pred, beta=1)


class nn_model:
    def __init__(self, input_dim):
        self.input_dim = input_dim

    def build_model(self):

        m = Sequential()
        m.add(Dense(256, activation='relu', input_dim=self.input_dim))
        m.add(Dropout(0.5))
        m.add(Dense(32, activation='relu'))
        m.add(Dropout(0.35))
        m.add(Dense(1, activation='sigmoid'))
        m.compile(optimizer='adam',
                  loss='binary_crossentropy',
                  metrics=['binary_accuracy', recall, precision, fmeasure])
        return m
