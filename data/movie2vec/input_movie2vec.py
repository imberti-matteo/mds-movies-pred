import pickle as pk
import pandas as pd
import numpy as np

def build_movie2vec_input(df,m2v):

    train_text = []
    for id_ in df.loc[:, 'ID'].values:
        found_df = m2v[m2v['ID'] == id_]
        train_text.append(found_df.iloc[0,1])
    train_text = np.array(train_text)
    return train_text

model_name = "new_extended_cleaned_dataset"
v_size = 300
path = "./m2v_" + model_name + "_" + str(v_size) + ".mod"
with open(path, "rb") as f:
    df_m2v = pk.load(f)

df_base = pd.read_csv("./../" + model_name + ".csv")
first = build_movie2vec_input(df_base,df_m2v)


