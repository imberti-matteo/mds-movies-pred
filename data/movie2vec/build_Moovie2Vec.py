import gensim
import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from nltk import ne_chunk, pos_tag, word_tokenize
from nltk.tree import Tree
import pickle as pk

def get_continuous_chunks(text):
    chunked = ne_chunk(pos_tag(word_tokenize(text)))
    prev = None
    continuous_chunk = []
    current_chunk = []
    for i in chunked:
            if type(i) == Tree:
                    current_chunk.append(" ".join([token for token, pos in i.leaves()]))
            elif current_chunk:
                    named_entity = " ".join(current_chunk)
                    if named_entity not in continuous_chunk:
                            continuous_chunk.append(named_entity)
                            current_chunk = []
            else:
                    continue
    return continuous_chunk

def build_movie2vec(df_base):

    m2v_df = pd.DataFrame([], columns=['ID', 'MOVIE2VEC'])
    model = gensim.models.KeyedVectors.load_word2vec_format('./google-w2v.bin', binary=True)
    base_text = df_base.loc[:, 'TEXT'].str.lower().tolist()
    df_base_id_list = df_base.loc[:, 'ID'].tolist()
    base_text_original = df_base.loc[:, 'TEXT'].tolist()

    tfidf = TfidfVectorizer(min_df=0.01, stop_words='english')
    tfidf_input = tfidf.fit_transform(base_text)
    feature_names = tfidf.get_feature_names()

    tfidf_input = tfidf_input.toarray()

    movie2vec = []
    m2v_account = 15
    range_len = len(tfidf_input)
    for movie in range(range_len):
        w2v_keywords = []
        slave_input = tfidf_input[movie]
        slave_feature_names = feature_names[:]
        ne_set = get_continuous_chunks(base_text_original[movie])
        #ne_set = []
        i = 0
        while i < m2v_account:
            max_score = max(slave_input)
            if max_score == 0:
                break
            try:
                ind = int(np.where(slave_input == max_score)[0])
            except TypeError:
                ind = int(np.where(slave_input == max_score)[0][0])
            if (slave_feature_names[ind] not in [x.lower() for x in ne_set]):
                try:
                    w2v_keywords.append(model.get_vector(slave_feature_names[ind]))
                    i += 1
                except KeyError:
                    pass
            slave_input = np.delete(slave_input, ind)
            slave_feature_names.pop(ind)
        plot2vec = np.average(w2v_keywords, axis=0)
        plot2vec = np.array(plot2vec)
        m2v_df = pd.concat([m2v_df,pd.DataFrame([(df_base_id_list[movie], plot2vec)], columns=['ID', 'MOVIE2VEC'])],ignore_index=True)
        print("Movie2Vec -", df_base_id_list[movie], "||", int((movie * 100) / range_len), "%")
    return m2v_df


df = pd.read_csv("./../new_extended_cleaned_dataset.csv")
model_name = "new_extended_cleaned_dataset"
v_size = 300
path = "./m2v_" + model_name + "_" + str(v_size) + ".mod"
m2v = build_movie2vec(df)
with open(path, "wb") as f:
    pk.dump(m2v, f)
