import pandas as pd
import numpy as np
import string as str
import operator
import networkx as nx
import matplotlib.pyplot as plt


data = pd.read_csv("2017_07_14_v01dataset.csv",sep=",")

moods = data.loc[:]['MOOD'] #ottengo tutti i valori della colonna MOOD

# QUANTE VOLTE APPARE UNA LABEL?
moods_dictionary = {}
unique_moods = []
for current_mood in moods:
    current_mood_lower = current_mood.lower()
    splitted_mood = current_mood_lower.split(',')
    for m in splitted_mood:
        if m in moods_dictionary:
            moods_dictionary[m] += 1
        else:
            moods_dictionary[m] = 1
            unique_moods.append(m)
print("\n|| NUMERO DI VOLTE IN CUI UNA LABEL APPARE ||\n")
print(sorted(moods_dictionary.items(), key=operator.itemgetter(1), reverse=True))
print("\n")

y=np.arange(len(unique_moods))
plt.plot(1)
plt.subplot(221)
plt.barh(y,list(moods_dictionary.values()))
plt.yticks(y, list(moods_dictionary.keys()),  fontsize=5)
plt.title("NUMERO DI VOLTE IN CUI UNA LABEL APPARE", fontsize=7)

# QUANTE VOLTE APPARE UNA LABEL IN PERCENTUALE?
total_count = len(moods)
moods_percentual_dictionary = {key:round((value/total_count)*100,2) for key,value in moods_dictionary.items()}
print("\n|| PERCENTUALE DI VOLTE IN CUI UNA LABEL APPARE ||\n")
print(sorted(moods_percentual_dictionary.items(), key=operator.itemgetter(1), reverse=True))
print("\n")

y=np.arange(len(unique_moods))
plt.subplot(222)
plt.barh(y,list(moods_percentual_dictionary.values()))
plt.yticks(y, list(moods_percentual_dictionary.keys()),  fontsize=5)
plt.title("PERCENTUALE DI VOLTE IN CUI UNA LABEL APPARE", fontsize=7)

# QUANTE VOLTE DUE LABEL APPAIONO INSIEME?
cross_relation = {}
for x in unique_moods:
    for y in unique_moods:
        union_string = x + ' UNION ' + y
        reversed_union_string = y + ' UNION ' + x
        if (reversed_union_string not in cross_relation) and (x != y):
            cross_relation[union_string] = 0
for current_mood in moods:
    current_mood_lower = current_mood.lower()
    splitted_mood = current_mood_lower.split(',')
    for x in splitted_mood:
        for y in splitted_mood:
            union_string = x + ' UNION ' + y
            reversed_union_string = y + ' UNION ' + x
            if (union_string in cross_relation):
                cross_relation[union_string] += 1
            elif (reversed_union_string in cross_relation):
                cross_relation[reversed_union_string] += 1
cross_relation = {key:int(value/2) for key,value in cross_relation.items()}
print("\n|| NUMERO DI VOLTE IN CUI DUE LABEL APPAIONO INSIEME ||\n")
print(sorted(cross_relation.items(), key=operator.itemgetter(1), reverse=True))
print("\n")

# QUANTE VOLTE APPARE SOLA UNA LABEL?
lonely_dictionary = moods_dictionary.copy()
for current_mood in lonely_dictionary.keys():
    for other_mood in unique_moods:
        union_string = current_mood + ' UNION ' + other_mood
        reversed_union_string = other_mood + ' UNION ' + current_mood
        if (union_string in cross_relation) and (current_mood != other_mood):
            lonely_dictionary[current_mood] -= cross_relation[union_string]
        elif (reversed_union_string in cross_relation) and (current_mood != other_mood):
            lonely_dictionary[current_mood] -= cross_relation[reversed_union_string]
print("\n|| NUMERO DI VOLTE IN CUI UNA LABEL APPARE DA SOLA ||\n")
print(sorted(lonely_dictionary.items(), key=operator.itemgetter(1), reverse=True))
print("\n")

y=np.arange(len(lonely_dictionary))
plt.subplot(223)
plt.barh(y,list(lonely_dictionary.values()))
plt.yticks(y, list(lonely_dictionary.keys()), fontsize=5)
plt.title("NUMERO DI VOLTE IN CUI UNA LABEL APPARE DA SOLA", fontsize=7)

# QUANTE VOLTE IN PERCENTUALE UNA LABEL APPARE DA SOLA?
lonely_percentual_dictionary = lonely_dictionary.copy()
for current_mood in lonely_percentual_dictionary.keys():
    lonely_percentual_dictionary[current_mood] = round((lonely_percentual_dictionary[current_mood] / moods_dictionary[current_mood])*100,2)
print("\n|| PERCENTUALE DI VOLTE IN CUI UNA LABEL APPARE DA SOLA ||\n")
print(sorted(lonely_percentual_dictionary.items(), key=operator.itemgetter(1), reverse=True))
print("\n")

y=np.arange(len(lonely_percentual_dictionary))
plt.subplot(224)
plt.barh(y,list(lonely_percentual_dictionary.values()))
plt.yticks(y, list(lonely_percentual_dictionary.keys()), fontsize=5)
plt.title("PERCENTUALE DI VOLTE IN CUI UNA LABEL APPARE DA SOLA", fontsize=7)
plt.show()

# QUANTE VOLTE IN PERCENTUALE UNA LABEL APPARE IN PRESENZA DI UN'ALTRA?
print("\n|| PERCENTUALE DI VOLTE IN CUI UNA LABEL APPARE IN PRESENZA DI UN'ALTRA ||\n[Da leggere come 'X' ---> 'Y', Z: Se è presente la label 'X', allora nel Z% delle volte è presente anche la label 'Y']\n")
composition = {}
ratio_dictionary = {}
for m in unique_moods:
    for n in unique_moods:
        if m != n:
            union_string = m + ' UNION ' + n
            reversed_union_string = n + ' UNION ' + m
            if (union_string in cross_relation):
                ratio = cross_relation[union_string] / moods_dictionary[n]
                composition_string = "'" + n + "' ---> '" + m + "'"
                ratio_dictionary[composition_string] = round(ratio*100,2)
                composition[composition_string] = round(ratio*100,2)
            elif (reversed_union_string in cross_relation):
                ratio = cross_relation[reversed_union_string] / moods_dictionary[n]
                composition_string = "'" + n + "' ---> '" + m + "'"
                ratio_dictionary[composition_string] = round(ratio * 100, 2)
                composition[composition_string] = round(ratio * 100,2)
print(sorted(ratio_dictionary.items(), key=operator.itemgetter(1), reverse=True))
print("\n")

G = nx.Graph()
G.add_nodes_from(unique_moods)
graph_threshold = 0
# QUALI SONO LE LABEL IN PERCENTUALE CHE COMPAIONO CON UN'ALTRA LABEL?
print("\n|| COMPOSIZIONE PERCENTUALE LABEL ||\n")
plt.plot(2)
i = 0
for m in unique_moods:
    current_composition = {}
    print("°°°",m.upper(),"°°°")
    for n in unique_moods:
        composition_string = "'" + m + "' ---> '" + n + "'"
        if (composition_string in composition) and (composition[composition_string]>0):
            current_composition[n]= composition[composition_string]
            if(composition[composition_string]>graph_threshold):
                G.add_edge(m,n)
    current_composition['niente']=lonely_percentual_dictionary[m]
    if i>3:
        plt.show()
        i=0
    plt.subplot(2,2,(i%4)+1)
    i += 1
    plt.title(m.upper(),fontsize=8)
    plt.pie(list(current_composition.values()),labels=list(current_composition.keys()),textprops={'fontsize':"x-small"})
    print(sorted(current_composition.items(), key=operator.itemgetter(1), reverse=True))
    print()
print()
plt.show()
plt.plot(3)
plt.title("GRAFO DELLE RELAZIONI TRA LABEL", fontsize=15)
nx.draw_spring(G, with_labels=True, font_size=6, font_weight='bold')
plt.show()

# QUANTE VOLTE IN PERCENTUALE DUE LABEL APPAIONO INSIEME?
print("\n|| PERCENTUALE DI VOLTE IN CUI DUE LABEL APPAIONO INSIEME ||\n[Da leggere come 'X' UNION 'Y', Z: La label 'X' e la label 'Y' sono presenti insieme il Z% delle volte]\n")
done_list = []
bidirectional_ratio_dictionary = {}
for m in unique_moods:
    for n in unique_moods:
        if m != n:
            union_string = m + ' UNION ' + n
            reversed_union_string = n + ' UNION ' + m
            if (union_string in cross_relation) and (union_string not in done_list):
                bidirectional_ratio = cross_relation[union_string] / (moods_dictionary[n] + moods_dictionary[m] - cross_relation[union_string])
                bidirectional_composition_string = "'" + n + "'" + " UNION " + "'" + m + "'"
                bidirectional_ratio_dictionary[bidirectional_composition_string] = round(bidirectional_ratio*100,2)
                done_list.append(union_string)
            elif (reversed_union_string in cross_relation) and (reversed_union_string not in done_list):
                bidirectional_ratio = cross_relation[reversed_union_string] / (moods_dictionary[n] + moods_dictionary[m] - cross_relation[reversed_union_string])
                bidirectional_composition_string = "'" + n + "'" + " UNION " + "'" + m + "'"
                bidirectional_ratio_dictionary[bidirectional_composition_string] = round(bidirectional_ratio * 100, 2)
                done_list.append(reversed_union_string)
print(sorted(bidirectional_ratio_dictionary.items(), key=operator.itemgetter(1), reverse=True))
print("\n")