import pickle
import scraping
import pandas as pd

key_imdb = "LINK_IMDB"
dataset_name = "dataset.csv"

def build_dataset1():
    ds = pd.read_csv(dataset_name)

    imdb_links = list(ds[key_imdb])
    imdb_links = list(filter(lambda a: a != "\\N", imdb_links))
    resume = 0
    step_number = 50
    step = int(len(imdb_links) / step_number)
    head = step * resume
    tail = head + step

    for j in range(resume, step_number):
        movies_id_list = []
        if j == 49:
            tail = -1
        for link in imdb_links[head:tail]:
            # Extract id from link in the dataset
            movie_id = link.split("/")[-1]
            if movie_id == "tt1124017":
                continue
            movies_id_list.append(movie_id)

        print("fetched")

        synopsis = scraping.get_synopsis_from_list(movies_id_list=movies_id_list, verbose=True)
        with open("synopsis.pk", "ab") as file:
            pickle.dump(synopsis, file)

        print("block", j, "done")
        head = tail
        tail += step

def build_dataset():
    with open("index", "r")as f:
        last_index = int(f.read())

    data = pd.read_csv(dataset_name)
    ds = data.loc[last_index:]
    print("start",last_index)
    for index, link in zip(ds.index, ds[key_imdb]):
        imdb_id = link.split("/")[-1]
        synopsis = scraping.get_synopsis(imdb_id,verbose=True)
        row_dataset = ds.loc[index]
        row_dataset['imdb_id'] = imdb_id
        row_dataset['synopsis'] = synopsis
        row_dataset['index'] = index
        with(open('index',"w")) as f:
            f.write(str(index+1))
        with open('dataset_con_sinossi.csv', 'a') as f:
            pd.DataFrame.from_records([row_dataset]).to_csv(f, header=False, index=False)

    print("end")



build_dataset()
#pd.read_csv("foo.csv",encoding = "ISO-8859-1")