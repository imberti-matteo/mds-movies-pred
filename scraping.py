import requests as req
from bs4 import BeautifulSoup

base_url_head = "http://www.imdb.com/title/"
base_url_tail = "/plotsummary?ref_=tt_stry_pl#synopsis"


def get_synopsis(movie_id, verbose=False):
    base_url = base_url_head + movie_id + base_url_tail
    r = req.get(base_url)
    soup = BeautifulSoup(r.content, "html.parser")
    synopsis = soup.find(id="plot-synopsis-content")
    # print(type(synopsis))
    if synopsis is None:
        #TODO check with other sources?
        print("None", movie_id)
        return "NA"
    # if synopsis == '\n\nIt looks like we don\'t have a Synopsis for this title yet. Be the first to contribute! ' \
    #                'Just click the "Edit page" button at the bottom of the page or learn more in the Synopsis submission guide.\n\n':
    #     synopsis = None
    if verbose:
        print("success", movie_id)

    summaries = soup.find(id = "plot-summaries-content").find_all(class_="ipl-zebra-list__item")
    sum_text = ""
    for sum in summaries:
        sum_text += sum.find("p").text + " "
    # print(sum_text)
    return sum_text + synopsis.text

def get_synopsis_from_list(movies_id_list, verbose=False):
    synopsis_dictionary = {}
    for i, movie_id in enumerate(movies_id_list):
        if verbose:
            print(i, movie_id)
        synopsis_dictionary[movie_id] = get_synopsis(movie_id, verbose=verbose)
        if verbose:
            print("Success")

    return synopsis_dictionary

# def get_summary(movie_id, verbose=False):
