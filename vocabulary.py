from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
import operator
import string

def create_vocabulary(plots):
    vocabulary = []
    occurrencies = []
    stop_words = set(stopwords.words("english"))
    length = len(plots)

    for i in range(length):
        plot = plots[i]
        plot_lower_cased = plot.lower()

        for pun in string.punctuation:
            if pun != "-":
                plot_lower_cased = plot_lower_cased.replace(pun, " ")
            else:
                plot_lower_cased = plot_lower_cased.replace(pun, "")

        plot_stopped_tokenized = word_tokenize(plot_lower_cased)
        plot_tokenized = [w for w in plot_stopped_tokenized if w not in stop_words]

        for w in plot_tokenized:
            if w not in vocabulary:
                vocabulary.append(w)
                occurrencies.append(1)
            else:
                word_index = vocabulary.index(w)
                occurrencies[word_index] += 1

    voc_dict_occ = dict(zip(vocabulary, occurrencies))
    sorted_dict = sorted(voc_dict_occ.items(), key=operator.itemgetter(1), reverse=True)

    return sorted_dict